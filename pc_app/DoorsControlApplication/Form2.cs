﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DoorsControlApplication
{
    public partial class Form2 : Form
    {
        public string log = "";
        public string pass = "";
        public int state = -1;

        public Form2()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            log = this.login_textBox.Text;
            pass = this.password_textBox.Text;
            this.login_textBox.Text = "";
            this.password_textBox.Text = "";
            state = 1;
            this.Hide();
        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.state = 0;
            this.Hide();
        }
        
        //ожидание нажатия кнопки "OK"
        public async System.Threading.Tasks.Task<int> wait_user_data()
        {
            while (this.state < 0) await System.Threading.Tasks.Task.Delay(100);
            return this.state;
        }

        private void Form2_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.Equals(Keys.Enter))
            {
                this.log = this.login_textBox.Text;
                this.pass = this.password_textBox.Text;
                this.state = 1;
                this.login_textBox.Text = "";
                this.password_textBox.Text = "";
                this.Hide();
            }
            else if (e.KeyCode.Equals(Keys.Escape))
            {
                this.state = 0;
                this.login_textBox.Text = "";
                this.password_textBox.Text = "";
                this.Hide();
            }
        }
    }
}
