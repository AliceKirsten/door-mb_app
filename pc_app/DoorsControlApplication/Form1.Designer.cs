﻿namespace DoorsControlApplication
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage0 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBox31 = new System.Windows.Forms.TextBox();
            this.textBox30 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.disconnect_radioButton = new System.Windows.Forms.RadioButton();
            this.connect_radioButton = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Connection_button = new System.Windows.Forms.Button();
            this.Ethernet_radioButton = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.COM_radioButton = new System.Windows.Forms.RadioButton();
            this.ports_comboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.Read_datalog_button = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.Close_button = new System.Windows.Forms.Button();
            this.Open_button = new System.Windows.Forms.Button();
            this.Calib_button = new System.Windows.Forms.Button();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.textBox26 = new System.Windows.Forms.TextBox();
            this.Write_sys_button = new System.Windows.Forms.Button();
            this.Read_sys_button = new System.Windows.Forms.Button();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.Read_ErrCnt_button = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.Write_time_button = new System.Windows.Forms.Button();
            this.label28 = new System.Windows.Forms.Label();
            this.Read_time_button = new System.Windows.Forms.Button();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.Write_mech_button = new System.Windows.Forms.Button();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.Read_mech_button = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.Write_elect_button = new System.Windows.Forms.Button();
            this.Read_elect_button = new System.Windows.Forms.Button();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.textBox27 = new System.Windows.Forms.TextBox();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.radioButton7 = new System.Windows.Forms.RadioButton();
            this.radioButton8 = new System.Windows.Forms.RadioButton();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage0.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox8.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage0);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(-2, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(907, 654);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            this.tabControl1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tabControl1_KeyUp);
            // 
            // tabPage0
            // 
            this.tabPage0.Controls.Add(this.groupBox2);
            this.tabPage0.Controls.Add(this.groupBox1);
            this.tabPage0.Location = new System.Drawing.Point(4, 22);
            this.tabPage0.Name = "tabPage0";
            this.tabPage0.Size = new System.Drawing.Size(899, 628);
            this.tabPage0.TabIndex = 2;
            this.tabPage0.Text = "Подключение";
            this.tabPage0.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBox31);
            this.groupBox2.Controls.Add(this.textBox30);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.disconnect_radioButton);
            this.groupBox2.Controls.Add(this.connect_radioButton);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(10, 120);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(455, 221);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Статус";
            // 
            // textBox31
            // 
            this.textBox31.Location = new System.Drawing.Point(168, 142);
            this.textBox31.Name = "textBox31";
            this.textBox31.ReadOnly = true;
            this.textBox31.Size = new System.Drawing.Size(129, 20);
            this.textBox31.TabIndex = 16;
            // 
            // textBox30
            // 
            this.textBox30.Location = new System.Drawing.Point(168, 94);
            this.textBox30.Name = "textBox30";
            this.textBox30.ReadOnly = true;
            this.textBox30.Size = new System.Drawing.Size(129, 20);
            this.textBox30.TabIndex = 15;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(22, 145);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Потеряно пакетов";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(22, 97);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(99, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Получено пакетов";
            // 
            // disconnect_radioButton
            // 
            this.disconnect_radioButton.AutoCheck = false;
            this.disconnect_radioButton.AutoSize = true;
            this.disconnect_radioButton.Checked = true;
            this.disconnect_radioButton.Location = new System.Drawing.Point(262, 38);
            this.disconnect_radioButton.Name = "disconnect_radioButton";
            this.disconnect_radioButton.Size = new System.Drawing.Size(81, 17);
            this.disconnect_radioButton.TabIndex = 7;
            this.disconnect_radioButton.TabStop = true;
            this.disconnect_radioButton.Text = "Отключено";
            this.disconnect_radioButton.UseVisualStyleBackColor = true;
            // 
            // connect_radioButton
            // 
            this.connect_radioButton.AutoCheck = false;
            this.connect_radioButton.AutoSize = true;
            this.connect_radioButton.Location = new System.Drawing.Point(168, 38);
            this.connect_radioButton.Name = "connect_radioButton";
            this.connect_radioButton.Size = new System.Drawing.Size(88, 17);
            this.connect_radioButton.TabIndex = 6;
            this.connect_radioButton.TabStop = true;
            this.connect_radioButton.Text = "Подключено";
            this.connect_radioButton.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Статус подключения";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Connection_button);
            this.groupBox1.Controls.Add(this.Ethernet_radioButton);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.COM_radioButton);
            this.groupBox1.Controls.Add(this.ports_comboBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(10, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(455, 102);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Настройки";
            // 
            // Connection_button
            // 
            this.Connection_button.Location = new System.Drawing.Point(350, 31);
            this.Connection_button.Name = "Connection_button";
            this.Connection_button.Size = new System.Drawing.Size(80, 46);
            this.Connection_button.TabIndex = 5;
            this.Connection_button.Text = "Подключить";
            this.Connection_button.UseVisualStyleBackColor = true;
            this.Connection_button.Click += new System.EventHandler(this.Connection_button_Click);
            // 
            // Ethernet_radioButton
            // 
            this.Ethernet_radioButton.AutoSize = true;
            this.Ethernet_radioButton.Enabled = false;
            this.Ethernet_radioButton.Location = new System.Drawing.Point(262, 29);
            this.Ethernet_radioButton.Name = "Ethernet_radioButton";
            this.Ethernet_radioButton.Size = new System.Drawing.Size(65, 17);
            this.Ethernet_radioButton.TabIndex = 4;
            this.Ethernet_radioButton.TabStop = true;
            this.Ethernet_radioButton.Text = "Ethernet";
            this.Ethernet_radioButton.UseVisualStyleBackColor = true;
            this.Ethernet_radioButton.CheckedChanged += new System.EventHandler(this.Ethernet_radioButton_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Порт";
            // 
            // COM_radioButton
            // 
            this.COM_radioButton.AutoSize = true;
            this.COM_radioButton.Checked = true;
            this.COM_radioButton.Location = new System.Drawing.Point(168, 29);
            this.COM_radioButton.Name = "COM_radioButton";
            this.COM_radioButton.Size = new System.Drawing.Size(49, 17);
            this.COM_radioButton.TabIndex = 3;
            this.COM_radioButton.TabStop = true;
            this.COM_radioButton.Text = "COM";
            this.COM_radioButton.UseVisualStyleBackColor = true;
            this.COM_radioButton.CheckedChanged += new System.EventHandler(this.COM_radioButton_CheckedChanged);
            // 
            // ports_comboBox
            // 
            this.ports_comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ports_comboBox.FormattingEnabled = true;
            this.ports_comboBox.Location = new System.Drawing.Point(168, 56);
            this.ports_comboBox.Name = "ports_comboBox";
            this.ports_comboBox.Size = new System.Drawing.Size(159, 21);
            this.ports_comboBox.TabIndex = 1;
            this.ports_comboBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ports_comboBox_MouseClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Тип подключения";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.Read_datalog_button);
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.chart1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(899, 628);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Состояние";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // Read_datalog_button
            // 
            this.Read_datalog_button.Location = new System.Drawing.Point(813, 547);
            this.Read_datalog_button.Name = "Read_datalog_button";
            this.Read_datalog_button.Size = new System.Drawing.Size(75, 23);
            this.Read_datalog_button.TabIndex = 20;
            this.Read_datalog_button.Text = "Считать";
            this.Read_datalog_button.UseVisualStyleBackColor = true;
            this.Read_datalog_button.Click += new System.EventHandler(this.Read_datalog_button_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.radioButton3);
            this.groupBox4.Controls.Add(this.radioButton1);
            this.groupBox4.Controls.Add(this.Close_button);
            this.groupBox4.Controls.Add(this.Open_button);
            this.groupBox4.Controls.Add(this.Calib_button);
            this.groupBox4.Location = new System.Drawing.Point(3, 576);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(893, 49);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Управление";
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Checked = true;
            this.radioButton3.Location = new System.Drawing.Point(7, 23);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(102, 17);
            this.radioButton3.TabIndex = 23;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "автоматически";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(115, 23);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(66, 17);
            this.radioButton1.TabIndex = 21;
            this.radioButton1.Text = "вручную";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // Close_button
            // 
            this.Close_button.Location = new System.Drawing.Point(771, 20);
            this.Close_button.Name = "Close_button";
            this.Close_button.Size = new System.Drawing.Size(108, 23);
            this.Close_button.TabIndex = 2;
            this.Close_button.Text = "Закрыть двери";
            this.Close_button.UseVisualStyleBackColor = true;
            this.Close_button.Click += new System.EventHandler(this.Close_button_Click);
            // 
            // Open_button
            // 
            this.Open_button.Location = new System.Drawing.Point(657, 20);
            this.Open_button.Name = "Open_button";
            this.Open_button.Size = new System.Drawing.Size(108, 23);
            this.Open_button.TabIndex = 1;
            this.Open_button.Text = "Открыть двери";
            this.Open_button.UseVisualStyleBackColor = true;
            this.Open_button.Click += new System.EventHandler(this.Open_button_Click);
            // 
            // Calib_button
            // 
            this.Calib_button.Location = new System.Drawing.Point(543, 20);
            this.Calib_button.Name = "Calib_button";
            this.Calib_button.Size = new System.Drawing.Size(108, 23);
            this.Calib_button.TabIndex = 0;
            this.Calib_button.Text = "Калибровка";
            this.Calib_button.UseVisualStyleBackColor = true;
            this.Calib_button.Click += new System.EventHandler(this.Calib_button_Click);
            // 
            // chart1
            // 
            chartArea1.BackColor = System.Drawing.Color.Snow;
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(6, 6);
            this.chart1.Name = "chart1";
            this.chart1.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Bright;
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series1.Color = System.Drawing.Color.Black;
            series1.Legend = "Legend1";
            series1.LegendText = "Ток";
            series1.MarkerColor = System.Drawing.Color.Black;
            series1.Name = "CURRENT";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series2.Color = System.Drawing.Color.Fuchsia;
            series2.Legend = "Legend1";
            series2.LegendText = "Уставка";
            series2.MarkerColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            series2.Name = "UPOS";
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series3.Color = System.Drawing.Color.Cyan;
            series3.Legend = "Legend1";
            series3.LegendText = "Положение";
            series3.MarkerColor = System.Drawing.Color.Aqua;
            series3.Name = "POS";
            series4.ChartArea = "ChartArea1";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series4.Color = System.Drawing.Color.Lime;
            series4.Legend = "Legend1";
            series4.LegendText = "Скорость";
            series4.MarkerColor = System.Drawing.Color.Lime;
            series4.Name = "VEL";
            series5.ChartArea = "ChartArea1";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series5.Color = System.Drawing.Color.Orange;
            series5.Legend = "Legend1";
            series5.LegendText = "Ускорение";
            series5.MarkerColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            series5.Name = "ACC";
            series6.ChartArea = "ChartArea1";
            series6.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series6.Color = System.Drawing.Color.Red;
            series6.Legend = "Legend1";
            series6.LegendText = "Ошибка";
            series6.MarkerColor = System.Drawing.Color.Red;
            series6.Name = "EPOS";
            this.chart1.Series.Add(series1);
            this.chart1.Series.Add(series2);
            this.chart1.Series.Add(series3);
            this.chart1.Series.Add(series4);
            this.chart1.Series.Add(series5);
            this.chart1.Series.Add(series6);
            this.chart1.Size = new System.Drawing.Size(890, 564);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chart1";
            this.chart1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.chart1_MouseDoubleClick);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox3);
            this.tabPage2.Controls.Add(this.groupBox10);
            this.tabPage2.Controls.Add(this.groupBox8);
            this.tabPage2.Controls.Add(this.groupBox9);
            this.tabPage2.Controls.Add(this.groupBox5);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(899, 628);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Администрирование";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.textBox4);
            this.groupBox3.Controls.Add(this.label23);
            this.groupBox3.Controls.Add(this.textBox21);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.textBox22);
            this.groupBox3.Controls.Add(this.textBox26);
            this.groupBox3.Controls.Add(this.Write_sys_button);
            this.groupBox3.Controls.Add(this.Read_sys_button);
            this.groupBox3.Controls.Add(this.label29);
            this.groupBox3.Controls.Add(this.label30);
            this.groupBox3.Location = new System.Drawing.Point(602, 213);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(290, 201);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Настройки самодиагностики";
            this.groupBox3.Visible = false;
            // 
            // textBox21
            // 
            this.textBox21.Location = new System.Drawing.Point(154, 77);
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new System.Drawing.Size(130, 20);
            this.textBox21.TabIndex = 25;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 28);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(133, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "Коэффициент ускорения";
            // 
            // textBox22
            // 
            this.textBox22.Location = new System.Drawing.Point(155, 51);
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new System.Drawing.Size(129, 20);
            this.textBox22.TabIndex = 24;
            // 
            // textBox26
            // 
            this.textBox26.Location = new System.Drawing.Point(155, 25);
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new System.Drawing.Size(129, 20);
            this.textBox26.TabIndex = 23;
            // 
            // Write_sys_button
            // 
            this.Write_sys_button.Location = new System.Drawing.Point(173, 164);
            this.Write_sys_button.Name = "Write_sys_button";
            this.Write_sys_button.Size = new System.Drawing.Size(75, 23);
            this.Write_sys_button.TabIndex = 5;
            this.Write_sys_button.Text = "Записать";
            this.Write_sys_button.UseVisualStyleBackColor = true;
            this.Write_sys_button.Click += new System.EventHandler(this.Write_sys_button_Click);
            // 
            // Read_sys_button
            // 
            this.Read_sys_button.Location = new System.Drawing.Point(27, 164);
            this.Read_sys_button.Name = "Read_sys_button";
            this.Read_sys_button.Size = new System.Drawing.Size(75, 23);
            this.Read_sys_button.TabIndex = 4;
            this.Read_sys_button.Text = "Считать";
            this.Read_sys_button.UseVisualStyleBackColor = true;
            this.Read_sys_button.Click += new System.EventHandler(this.Read_sys_button_Click);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(6, 80);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(136, 13);
            this.label29.TabIndex = 2;
            this.label29.Text = "Коэффициент положения";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(6, 54);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(127, 13);
            this.label30.TabIndex = 1;
            this.label30.Text = "Коэффициент скорости";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.Read_ErrCnt_button);
            this.groupBox10.Controls.Add(this.label9);
            this.groupBox10.Controls.Add(this.dataGridView1);
            this.groupBox10.Controls.Add(this.textBox1);
            this.groupBox10.Location = new System.Drawing.Point(10, 420);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(882, 202);
            this.groupBox10.TabIndex = 6;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Лог ошибок";
            this.groupBox10.Visible = false;
            // 
            // Read_ErrCnt_button
            // 
            this.Read_ErrCnt_button.Location = new System.Drawing.Point(810, 173);
            this.Read_ErrCnt_button.Name = "Read_ErrCnt_button";
            this.Read_ErrCnt_button.Size = new System.Drawing.Size(30, 23);
            this.Read_ErrCnt_button.TabIndex = 18;
            this.Read_ErrCnt_button.Text = "<->";
            this.Read_ErrCnt_button.UseVisualStyleBackColor = true;
            this.Read_ErrCnt_button.Click += new System.EventHandler(this.Read_ErrCnt_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(608, 179);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(97, 16);
            this.label9.TabIndex = 17;
            this.label9.Text = "Всего ошибок";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4});
            this.dataGridView1.Location = new System.Drawing.Point(6, 19);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridView1.Size = new System.Drawing.Size(580, 177);
            this.dataGridView1.TabIndex = 5;
            this.dataGridView1.DoubleClick += new System.EventHandler(this.dataGridView1_DoubleClick);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Номер";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Время";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Тип";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Статус";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(711, 175);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(93, 20);
            this.textBox1.TabIndex = 11;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.textBox16);
            this.groupBox8.Controls.Add(this.textBox17);
            this.groupBox8.Controls.Add(this.textBox18);
            this.groupBox8.Controls.Add(this.textBox19);
            this.groupBox8.Controls.Add(this.textBox20);
            this.groupBox8.Controls.Add(this.Write_time_button);
            this.groupBox8.Controls.Add(this.label28);
            this.groupBox8.Controls.Add(this.Read_time_button);
            this.groupBox8.Controls.Add(this.label27);
            this.groupBox8.Controls.Add(this.label26);
            this.groupBox8.Controls.Add(this.label25);
            this.groupBox8.Controls.Add(this.label24);
            this.groupBox8.Location = new System.Drawing.Point(603, 6);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(290, 201);
            this.groupBox8.TabIndex = 3;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Время";
            this.groupBox8.Visible = false;
            // 
            // textBox16
            // 
            this.textBox16.Location = new System.Drawing.Point(155, 132);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(129, 20);
            this.textBox16.TabIndex = 22;
            // 
            // textBox17
            // 
            this.textBox17.Location = new System.Drawing.Point(155, 106);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(129, 20);
            this.textBox17.TabIndex = 21;
            // 
            // textBox18
            // 
            this.textBox18.Location = new System.Drawing.Point(155, 80);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(129, 20);
            this.textBox18.TabIndex = 20;
            // 
            // textBox19
            // 
            this.textBox19.Location = new System.Drawing.Point(155, 54);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(129, 20);
            this.textBox19.TabIndex = 19;
            // 
            // textBox20
            // 
            this.textBox20.Location = new System.Drawing.Point(155, 28);
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(129, 20);
            this.textBox20.TabIndex = 18;
            // 
            // Write_time_button
            // 
            this.Write_time_button.Location = new System.Drawing.Point(179, 164);
            this.Write_time_button.Name = "Write_time_button";
            this.Write_time_button.Size = new System.Drawing.Size(75, 23);
            this.Write_time_button.TabIndex = 7;
            this.Write_time_button.Text = "Записать";
            this.Write_time_button.UseVisualStyleBackColor = true;
            this.Write_time_button.Click += new System.EventHandler(this.Write_time_button_Click);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(6, 132);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(25, 13);
            this.label28.TabIndex = 4;
            this.label28.Text = "Год";
            // 
            // Read_time_button
            // 
            this.Read_time_button.Location = new System.Drawing.Point(33, 164);
            this.Read_time_button.Name = "Read_time_button";
            this.Read_time_button.Size = new System.Drawing.Size(75, 23);
            this.Read_time_button.TabIndex = 6;
            this.Read_time_button.Text = "Считать";
            this.Read_time_button.UseVisualStyleBackColor = true;
            this.Read_time_button.Click += new System.EventHandler(this.Read_time_button_Click);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(6, 109);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(40, 13);
            this.label27.TabIndex = 3;
            this.label27.Text = "Месяц";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(6, 83);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(34, 13);
            this.label26.TabIndex = 2;
            this.label26.Text = "День";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 57);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(35, 13);
            this.label25.TabIndex = 1;
            this.label25.Text = "Часы";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(6, 31);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(46, 13);
            this.label24.TabIndex = 0;
            this.label24.Text = "Минуты";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.comboBox3);
            this.groupBox9.Controls.Add(this.label22);
            this.groupBox9.Controls.Add(this.groupBox7);
            this.groupBox9.Controls.Add(this.groupBox6);
            this.groupBox9.Controls.Add(this.comboBox2);
            this.groupBox9.Controls.Add(this.comboBox1);
            this.groupBox9.Controls.Add(this.label15);
            this.groupBox9.Controls.Add(this.label16);
            this.groupBox9.Controls.Add(this.label17);
            this.groupBox9.Controls.Add(this.label18);
            this.groupBox9.Controls.Add(this.textBox11);
            this.groupBox9.Controls.Add(this.label19);
            this.groupBox9.Controls.Add(this.label20);
            this.groupBox9.Controls.Add(this.textBox13);
            this.groupBox9.Controls.Add(this.label21);
            this.groupBox9.Controls.Add(this.textBox14);
            this.groupBox9.Controls.Add(this.Write_mech_button);
            this.groupBox9.Controls.Add(this.Read_mech_button);
            this.groupBox9.Location = new System.Drawing.Point(306, 6);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(290, 408);
            this.groupBox9.TabIndex = 4;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Параметры masterboard";
            this.groupBox9.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 80);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(87, 13);
            this.label10.TabIndex = 25;
            this.label10.Text = "Скорость (авто)";
            // 
            // Write_mech_button
            // 
            this.Write_mech_button.Location = new System.Drawing.Point(166, 371);
            this.Write_mech_button.Name = "Write_mech_button";
            this.Write_mech_button.Size = new System.Drawing.Size(75, 23);
            this.Write_mech_button.TabIndex = 4;
            this.Write_mech_button.Text = "Записать";
            this.Write_mech_button.UseVisualStyleBackColor = true;
            this.Write_mech_button.Click += new System.EventHandler(this.Write_mech_button_Click);
            // 
            // textBox24
            // 
            this.textBox24.Location = new System.Drawing.Point(155, 77);
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new System.Drawing.Size(129, 20);
            this.textBox24.TabIndex = 3;
            // 
            // Read_mech_button
            // 
            this.Read_mech_button.Location = new System.Drawing.Point(20, 371);
            this.Read_mech_button.Name = "Read_mech_button";
            this.Read_mech_button.Size = new System.Drawing.Size(75, 23);
            this.Read_mech_button.TabIndex = 3;
            this.Read_mech_button.Text = "Считать";
            this.Read_mech_button.UseVisualStyleBackColor = true;
            this.Read_mech_button.Click += new System.EventHandler(this.Read_mech_button_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.groupBox11);
            this.groupBox5.Controls.Add(this.label31);
            this.groupBox5.Controls.Add(this.label35);
            this.groupBox5.Controls.Add(this.textBox28);
            this.groupBox5.Controls.Add(this.textBox27);
            this.groupBox5.Controls.Add(this.textBox23);
            this.groupBox5.Controls.Add(this.label32);
            this.groupBox5.Controls.Add(this.label14);
            this.groupBox5.Controls.Add(this.textBox7);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Controls.Add(this.textBox6);
            this.groupBox5.Controls.Add(this.label4);
            this.groupBox5.Controls.Add(this.textBox2);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Controls.Add(this.Write_elect_button);
            this.groupBox5.Controls.Add(this.Read_elect_button);
            this.groupBox5.Controls.Add(this.textBox5);
            this.groupBox5.Controls.Add(this.label13);
            this.groupBox5.Controls.Add(this.label12);
            this.groupBox5.Controls.Add(this.textBox3);
            this.groupBox5.Controls.Add(this.label11);
            this.groupBox5.Controls.Add(this.textBox24);
            this.groupBox5.Location = new System.Drawing.Point(10, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(290, 408);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Параметры driverboard";
            this.groupBox5.Visible = false;
            // 
            // Write_elect_button
            // 
            this.Write_elect_button.Location = new System.Drawing.Point(175, 371);
            this.Write_elect_button.Name = "Write_elect_button";
            this.Write_elect_button.Size = new System.Drawing.Size(75, 23);
            this.Write_elect_button.TabIndex = 7;
            this.Write_elect_button.Text = "Записать";
            this.Write_elect_button.UseVisualStyleBackColor = true;
            this.Write_elect_button.Click += new System.EventHandler(this.Write_mdata_button_Click);
            // 
            // Read_elect_button
            // 
            this.Read_elect_button.Location = new System.Drawing.Point(18, 371);
            this.Read_elect_button.Name = "Read_elect_button";
            this.Read_elect_button.Size = new System.Drawing.Size(75, 23);
            this.Read_elect_button.TabIndex = 6;
            this.Read_elect_button.Text = "Считать";
            this.Read_elect_button.UseVisualStyleBackColor = true;
            this.Read_elect_button.Click += new System.EventHandler(this.Read_mdata_button_Click);
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(155, 25);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(129, 20);
            this.textBox5.TabIndex = 1;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 168);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(44, 13);
            this.label13.TabIndex = 6;
            this.label13.Text = "Реверс";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 54);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(115, 13);
            this.label12.TabIndex = 4;
            this.label12.Text = "Макс. напряжение, В";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(155, 51);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(129, 20);
            this.textBox3.TabIndex = 2;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 29);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(70, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "Макс. ток, А";
            // 
            // serialPort1
            // 
            this.serialPort1.ReadTimeout = 120000;
            // 
            // timer1
            // 
            this.timer1.Interval = 1200000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 106);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 13);
            this.label4.TabIndex = 27;
            this.label4.Text = "Скорость (зимний)";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(155, 103);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(129, 20);
            this.textBox2.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 132);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(101, 13);
            this.label5.TabIndex = 29;
            this.label5.Text = "Процент открытия";
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(155, 129);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(129, 20);
            this.textBox6.TabIndex = 5;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 199);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(47, 13);
            this.label14.TabIndex = 31;
            this.label14.Text = "Ошибка";
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(155, 196);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(129, 20);
            this.textBox7.TabIndex = 7;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(6, 278);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(116, 13);
            this.label31.TabIndex = 37;
            this.label31.Text = "Открытое положение";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(6, 252);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(116, 13);
            this.label35.TabIndex = 36;
            this.label35.Text = "Закрытое положение";
            // 
            // textBox28
            // 
            this.textBox28.Location = new System.Drawing.Point(154, 275);
            this.textBox28.Name = "textBox28";
            this.textBox28.ReadOnly = true;
            this.textBox28.Size = new System.Drawing.Size(130, 20);
            this.textBox28.TabIndex = 34;
            // 
            // textBox27
            // 
            this.textBox27.Location = new System.Drawing.Point(154, 249);
            this.textBox27.Name = "textBox27";
            this.textBox27.ReadOnly = true;
            this.textBox27.Size = new System.Drawing.Size(130, 20);
            this.textBox27.TabIndex = 32;
            // 
            // textBox23
            // 
            this.textBox23.Location = new System.Drawing.Point(154, 222);
            this.textBox23.Name = "textBox23";
            this.textBox23.ReadOnly = true;
            this.textBox23.Size = new System.Drawing.Size(130, 20);
            this.textBox23.TabIndex = 35;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(6, 225);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(72, 13);
            this.label32.TabIndex = 33;
            this.label32.Text = "Скорость ХХ";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 237);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(125, 13);
            this.label15.TabIndex = 45;
            this.label15.Text = "Удержание в закр. поз";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 145);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(119, 13);
            this.label16.TabIndex = 43;
            this.label16.Text = "Действия при пожаре";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 117);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(137, 13);
            this.label17.TabIndex = 41;
            this.label17.Text = "Запирание замка (выход)";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 81);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(169, 13);
            this.label18.TabIndex = 39;
            this.label18.Text = "Выдержка в откр. поз(пожар), с";
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(181, 26);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(103, 20);
            this.textBox11.TabIndex = 8;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 172);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(148, 13);
            this.label19.TabIndex = 37;
            this.label19.Text = "Действия с аккумулятором";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 55);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(175, 13);
            this.label20.TabIndex = 35;
            this.label20.Text = "Выдержка в откр. поз(зимний), с";
            // 
            // textBox13
            // 
            this.textBox13.Location = new System.Drawing.Point(181, 52);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(103, 20);
            this.textBox13.TabIndex = 9;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 30);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(160, 13);
            this.label21.TabIndex = 32;
            this.label21.Text = "Выдержка в откр. поз(авто), с";
            // 
            // textBox14
            // 
            this.textBox14.Location = new System.Drawing.Point(181, 78);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(103, 20);
            this.textBox14.TabIndex = 10;
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "1",
            "2",
            "3"});
            this.comboBox1.Location = new System.Drawing.Point(181, 142);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(103, 21);
            this.comboBox1.TabIndex = 13;
            // 
            // comboBox2
            // 
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4"});
            this.comboBox2.Location = new System.Drawing.Point(181, 169);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(103, 21);
            this.comboBox2.TabIndex = 14;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.radioButton4);
            this.groupBox6.Controls.Add(this.radioButton2);
            this.groupBox6.Location = new System.Drawing.Point(181, 101);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(103, 35);
            this.groupBox6.TabIndex = 48;
            this.groupBox6.TabStop = false;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(6, 12);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(37, 17);
            this.radioButton2.TabIndex = 11;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "да";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(49, 12);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(42, 17);
            this.radioButton4.TabIndex = 12;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "нет";
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.radioButton5);
            this.groupBox7.Controls.Add(this.radioButton6);
            this.groupBox7.Location = new System.Drawing.Point(181, 223);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(103, 35);
            this.groupBox7.TabIndex = 49;
            this.groupBox7.TabStop = false;
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.Location = new System.Drawing.Point(49, 12);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(42, 17);
            this.radioButton5.TabIndex = 17;
            this.radioButton5.TabStop = true;
            this.radioButton5.Text = "нет";
            this.radioButton5.UseVisualStyleBackColor = true;
            // 
            // radioButton6
            // 
            this.radioButton6.AutoSize = true;
            this.radioButton6.Location = new System.Drawing.Point(6, 12);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(37, 17);
            this.radioButton6.TabIndex = 16;
            this.radioButton6.TabStop = true;
            this.radioButton6.Text = "да";
            this.radioButton6.UseVisualStyleBackColor = true;
            // 
            // comboBox3
            // 
            this.comboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Items.AddRange(new object[] {
            "1",
            "2",
            "3"});
            this.comboBox3.Location = new System.Drawing.Point(181, 196);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(103, 21);
            this.comboBox3.TabIndex = 15;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 199);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(61, 13);
            this.label22.TabIndex = 51;
            this.label22.Text = "Тип замка";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.radioButton7);
            this.groupBox11.Controls.Add(this.radioButton8);
            this.groupBox11.Location = new System.Drawing.Point(155, 155);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(129, 35);
            this.groupBox11.TabIndex = 50;
            this.groupBox11.TabStop = false;
            // 
            // radioButton7
            // 
            this.radioButton7.AutoSize = true;
            this.radioButton7.Location = new System.Drawing.Point(81, 12);
            this.radioButton7.Name = "radioButton7";
            this.radioButton7.Size = new System.Drawing.Size(42, 17);
            this.radioButton7.TabIndex = 17;
            this.radioButton7.TabStop = true;
            this.radioButton7.Text = "нет";
            this.radioButton7.UseVisualStyleBackColor = true;
            // 
            // radioButton8
            // 
            this.radioButton8.AutoSize = true;
            this.radioButton8.Location = new System.Drawing.Point(6, 12);
            this.radioButton8.Name = "radioButton8";
            this.radioButton8.Size = new System.Drawing.Size(37, 17);
            this.radioButton8.TabIndex = 16;
            this.radioButton8.TabStop = true;
            this.radioButton8.Text = "да";
            this.radioButton8.UseVisualStyleBackColor = true;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(154, 103);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(130, 20);
            this.textBox4.TabIndex = 27;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 106);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(122, 13);
            this.label23.TabIndex = 26;
            this.label23.Text = "Нулевой коэффициент";
            // 
            // Form1
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(902, 651);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "Form1";
            this.Text = "Door Control";
            this.tabControl1.ResumeLayout(false);
            this.tabPage0.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage0;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RadioButton disconnect_radioButton;
        private System.Windows.Forms.RadioButton connect_radioButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button Connection_button;
        private System.Windows.Forms.RadioButton Ethernet_radioButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton COM_radioButton;
        private System.Windows.Forms.ComboBox ports_comboBox;
        private System.Windows.Forms.Label label2;
        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Button Close_button;
        private System.Windows.Forms.Button Open_button;
        private System.Windows.Forms.Button Calib_button;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button Write_elect_button;
        private System.Windows.Forms.Button Read_elect_button;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.Button Write_mech_button;
        private System.Windows.Forms.Button Read_mech_button;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.Button Write_time_button;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Button Read_time_button;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.TextBox textBox26;
        private System.Windows.Forms.Button Write_sys_button;
        private System.Windows.Forms.Button Read_sys_button;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox textBox30;
        private System.Windows.Forms.TextBox textBox31;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button Read_ErrCnt_button;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button Read_datalog_button;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.TextBox textBox27;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.RadioButton radioButton6;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.RadioButton radioButton7;
        private System.Windows.Forms.RadioButton radioButton8;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label23;
    }
}

