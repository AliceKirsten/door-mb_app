﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.IO.Ports;

namespace DoorsControlApplication
{
    public enum TrxPacketFields
    {
        Start = 0,
        Address = 4,
        Type = 5,
        Data = 6,
        CRC = 30
    }
    public enum AddressType
    {
        pc = 0,
        drv,
        tdrv,
        tlog,
        tpost
    }

    //тип подключения
    public enum ConnectionType  
    {
        Unused = 0,                     //не инициализировано
        COM,                            //USB - UART
        Ethernet,                       //LAN
        WiFi,                           //Web
        Bluetooth                       //беспроводной
    }

    //статус драйвера
    public enum StatusType
    {
        Unused = 0,                     //не инициализировано
        OK,                             //все штатно
        SystemError,                    //системная ошибка
        FatalError                      //критическая ошибка
    }

    //тип ошибок
    public enum ErrorType
    {
        Unused = 0,                     //не инициализировано
        NoMotorData,                    //нет данных на мотор
        InVoltageLow,                   //низкое входное напряжение
        InVoltageNegative,              //отрицатеьное входное напряжение 
        AccVoltageNegative,             //отрицательное напряжение аккумулятора
        Overcurrent,                    //превышение по току
        PositionUnreached,              //невозможно достичь заданную позицию
        CalibrationFailed,              //ошибка алгоритма калибровки
        NoDataInFlash,                  //память МК пуста
        FlashEraseFailed,               //память МК не отвечает
        ADCUnunit,                      //ошибка инициализации АЦП
        ADCConvertionFailed,            //ошибка преобразования АЦП    
        DMAUninit,                      //ошибка инициализации прямого доступа к памяти
        I2CUninit,                      //ошибка инициализации интерфейса резервного хранилища
        TimPWMUninit,                   //ошибка инициализации ШИМ
        TimEncUninit,                   //ошибка инициализации энкодера
        TimCycleUninit,                 //ошибка инициализации таймера
        UartUninit                      //ошибка инициализации консоли
    }

    //тип комманд
    public enum CommandType
    {
        Unused = 0,                     //не инициализировано
        SetElectData = 5,               //5- установка параметров электрической системы
        GetElectData,                   //6- чтение параметров электрической системы
        SetMechData,                    //7- установка параметров механической системы
        GetMechData,                    //8- чтение параметров механической системы
        SetManCtrl,                     //9- установка ручного режима управления дверью
        SetAutoCtrl,                    //10- установка автоматического режима управления дверью
        SetWintCtrl,                    //11- установка автоматического режима управления дверью
        ResetWintCtrl,                  //12- установка автоматического режима управления дверью
        GetCalibrate,                   //13- калибровка автоматики
        SetDoorOpen,                    //14- открыть дверь
        SetDoorClose,                   //15- закрыть дверь
        GetAccVolt,                     //16- чтение напряжения на аккумуляторе
        SetAccOpen,                     //17- открыть транзистор аккумулятора
        SetAccClose,                    //18- закрыть транзистор аккумулятора
        SetTime,                        //19- установка времени
        GetTime,                        //20- чтение времени
        GetErrorCount,                  //21-- запрос количества ошибок
        GetVoltageData,                 //22- чтение пула напряжений
        GetSystemData,                  //23- чтение параметров состояния системы
        GetErrorData,                   //24- чтение лога ошибок
        SetDiagData,                    //25 - запись диагностических коэффициентов
        GetDiagData,                    //26 - чтение диагностических коэффициентов
    }
    public partial class Form1 : Form
    {
        ConnectionType contype = ConnectionType.COM;
        bool connect = false;
        UInt32 driver_data_packs_ok = 0, driver_data_packs_lost = 0;

        Form2 auth_form;
        string[] allowed_login = { "admin", "a" };
        string[] allowed_pass = { "password", "a" };
        string session_login = "";
        string session_pass = "";

        const uint magic_32 = 0xf3c3f00c;
        Thread serial_port_read;
        delegate void SetFormtDataDelegate(object sender, object[] data, bool clean);

        public Form1()
        {
            InitializeComponent();
            Renew_ports_list();

            auth_form = new Form2();
            auth_form.Hide();
        }

        //обновление списка текущих COM-портов
        private void Renew_ports_list()
        {
            this.ports_comboBox.Items.Clear();
            switch (contype)
            {
                case ConnectionType.COM:
                    string[] ports = SerialPort.GetPortNames();
                    foreach (string p in ports)
                    {
                        this.ports_comboBox.Items.Add(p);
                    }
                    break;
                case ConnectionType.Ethernet:
                    //do nothing
                    break;
                default:
                    break;
            }
        }

        //обновление выпадающего списка портов по клику 
        private void ports_comboBox_MouseClick(object sender, MouseEventArgs e)
        {
            Renew_ports_list();
        }

        //статус подключения был изменен
        private void Set_Connection_Status(bool state)
        {
           this.connect_radioButton.Checked = state;
           this.disconnect_radioButton.Checked = !state;
           connect = state;
        }

        //изменить тип подключения на COM
        private void COM_radioButton_CheckedChanged(object sender, EventArgs e)
        {
            contype = ConnectionType.COM;
            Renew_ports_list();
        }

        //изменить тип подключения на Ethernet
        private void Ethernet_radioButton_CheckedChanged(object sender, EventArgs e)
        {
            contype = ConnectionType.Ethernet;
            Renew_ports_list();
        }

        //подключение к устройству
        private void Connection_button_Click(object sender, EventArgs e)
        {
            switch (contype)
            {
                case ConnectionType.COM:
                    if (!this.serialPort1.IsOpen)
                    {
                        try
                        {
                            this.serialPort1.BaudRate = 115200;
                            this.serialPort1.PortName = this.ports_comboBox.Text;
                            this.serialPort1.Open();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                            Set_Connection_Status(false);
                            return;
                        }
                        this.serialPort1.DiscardInBuffer();
                        this.Connection_button.BackColor = Color.ForestGreen;
                        Set_Connection_Status(true);

                        serial_port_read = new Thread(new ThreadStart(Serial_Port_Read_Thread));
                        serial_port_read.IsBackground = true;
                        serial_port_read.Start();
                    }
                    else
                    {
                        this.serialPort1.Close();
                        this.Connection_button.BackColor = SystemColors.ButtonFace;
                        Set_Connection_Status(false);
                        serial_port_read.Abort();
                    }
                    break;
                default:
                    break;
            }
        }

        //проверка уровня доступа к ресурсам администрирования
        private async void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.tabControl1.SelectedIndex == 2 && !this.timer1.Enabled)
            {
                auth_form.Show();
                int res = await auth_form.wait_user_data();
                if (res > 0)
                {
                    this.session_login = auth_form.log;
                    this.session_pass = auth_form.pass;
                    auth_form.log = "";
                    auth_form.pass = "";

                    foreach (string l in allowed_login)                                     //check login
                    {
                        if (this.session_login.Equals(l))                                   //login OK
                        {
                            foreach (string p in allowed_pass)                              //check password
                            {
                                if (this.session_pass.Equals(p))                            //password OK
                                {
                                    foreach (GroupBox gb in this.tabPage2.Controls)         //show tab content
                                    {
                                        SetFormData(gb, new object[] { true }, false);
                                    }
                                    this.timer1.Enabled = true;                             //start session timer
                                }
                            }
                        }
                    }
                }
                auth_form.state = -1;
                if (!this.timer1.Enabled)        //authorization failed                
                {
                    this.tabControl1.SelectedIndex = 0;
                    MessageBox.Show("Неверный логин/пароль!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        //событие таймера -> окончание сессии администрирования
        private void timer1_Tick(object sender, EventArgs e)
        {
            foreach (GroupBox gb in this.tabPage2.Controls)    //hide tab content
            {
                SetFormData(gb, new object[] { false }, false);
            }
            this.session_login = "";
            this.session_pass = "";
            this.timer1.Enabled = false;
        }

        //очистка графика
        private void chart1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            SetFormData(this.chart1, new object[] { 0 }, true);
        }

        //изменить управление на ручное
        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            bool chk = this.radioButton1.Checked;

            this.Open_button.Enabled = !chk;
            this.Close_button.Enabled = !chk;
            this.Calib_button.Enabled = !chk;

            if (chk)
            {
                if (!this.serialPort1.IsOpen)
                {
                    MessageBox.Show("COM-Порт закрыт!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                byte[] b;
                byte[] buffer = new byte[32];
                b = BitConverter.GetBytes(magic_32);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Start, b.Length);

                buffer[(int)TrxPacketFields.Address] = (byte)AddressType.tdrv << 4 | (byte)AddressType.pc;
                buffer[(int)TrxPacketFields.Type] = (byte)CommandType.SetManCtrl;

                this.serialPort1.Write(buffer, 0, buffer.Length);
            }
        }

        //изменить управление на автоматическое
        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            bool chk = this.radioButton3.Checked;
            
            this.Open_button.Enabled = chk;
            this.Close_button.Enabled = chk;
            this.Calib_button.Enabled = chk;

            if (chk)
            {
                if (!this.serialPort1.IsOpen)
                {
                    MessageBox.Show("COM-Порт закрыт!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                byte[] b;
                byte[] buffer = new byte[32];
                b = BitConverter.GetBytes(magic_32);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Start, b.Length);

                buffer[(int)TrxPacketFields.Address] = (byte)AddressType.tdrv << 4 | (byte)AddressType.pc;
                buffer[(int)TrxPacketFields.Type] = (byte)CommandType.SetAutoCtrl;

                this.serialPort1.Write(buffer, 0, buffer.Length);
            }
        }

        //метод безопасного изменения данных на форме 
        private void SetFormData(object sender, object[] data, bool clean)
        {
            Control c = (Control)sender;

            if (c.InvokeRequired)
            {
                SetFormtDataDelegate tsDelegate = new SetFormtDataDelegate(SetFormData);
                c.Invoke(tsDelegate, sender, data, clean);
            }
            else
            {
                string t = sender.GetType().ToString();
                if (t.Equals("System.Windows.Forms.TextBox"))
                {
                    TextBox tb = (TextBox)sender;
                    if (clean) tb.Text = "";
                    else
                    {
                        object d = data[0];
                        tb.Text = d.ToString();
                    }
                }
                else if (t.Equals("System.Windows.Forms.ComboBox"))
                {
                    ComboBox cb = (ComboBox)sender;
                    object d = data[0];
                    cb.SelectedItem = d;
                }
                else if (t.Equals("System.Windows.Forms.radioButton"))
                {
                    RadioButton rb = (RadioButton)sender;
                    bool b = (bool)data[0];
                    if (b) rb.Checked = true;
                    else rb.Checked = false;
                }
                else if (t.Equals("System.Windows.Forms.DataVisualization.Charting.Chart"))
                {
                    Chart ch = (Chart)sender;
                    if (clean)
                    {
                        foreach (Series s in ch.Series)
                        {
                            s.Points.Clear();
                        }
                    }
                    else
                    {
                        if (ch.Series[0].Points.Count > 1000)
                        {
                            ch.Series[0].Points.RemoveAt(0);
                            ch.Series[1].Points.RemoveAt(0);
                            ch.Series[2].Points.RemoveAt(0);
                            ch.Series[3].Points.RemoveAt(0);
                            ch.Series[4].Points.RemoveAt(0);
                        }
                        float[] d = { 0, 0, 0, 0, 0 };
                        Array.Copy(data, d, 5);
                        ch.Series[0].Points.Add(d[0]);
                        ch.Series[1].Points.Add(d[1]);
                        ch.Series[2].Points.Add(d[2]);
                        ch.Series[3].Points.Add(d[3]);
                        ch.Series[4].Points.Add(d[4]);
                    }
                }
                else if (t.Equals("System.Windows.Forms.GroupBox"))
                {
                    GroupBox gb = (GroupBox)sender;
                    gb.Visible = (bool)data[0];
                }
                else if (t.Equals("System.Windows.Forms.DataGridView"))
                {
                    DataGridView dg = (DataGridView)sender;
                    dg.Rows.Add();
                    dg[0, dg.RowCount - 1].Value = data[0].ToString();
                    dg[1, dg.RowCount - 1].Value = data[1].ToString();
                    dg[2, dg.RowCount - 1].Value = data[2].ToString();
                    dg[3, dg.RowCount - 1].Value = data[3].ToString();
                }
            }
        }

        //обработчик принятого пакета
        private void Serial_Port_Read_Thread()
        {
            uint sword = 0;
            byte[] buffer = new byte[32];       //MAX SIZE of packet - 32 bytes
            int err_cnt = 0, grid_cnt = 0;
            StatusType Status;
            ErrorType Error;

            while (this.serialPort1.IsOpen)    //close the thread when serial port is closed
            {
                uint b = 0;
                try                            //check unexpected external disconnection 
                {
                    b = (uint)this.serialPort1.ReadByte();
                }
                catch (Exception)              //catch unauthorized access exception if connection was closed
                {
                    MessageBox.Show("COM-Порт закрыт или устройство не отвечает", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;                     //close the thread          
                }

                sword >>= 8;                   //right shift sync word - LSB first
                sword |= (b << 24);            //last received byte is MSB 

                if (sword == magic_32)         //check start word
                {
                    this.serialPort1.Read(buffer, (int)TrxPacketFields.Address, 28);
                    if ((AddressType)(buffer[(int)TrxPacketFields.Address] & 0xF0) == AddressType.pc)     //rx address -> PC
                    {
                        if ((AddressType)(buffer[(int)TrxPacketFields.Address] & 0x0F) == AddressType.tdrv) //tx address -> driver board
                        {
                            driver_data_packs_ok++;
                            SetFormData(textBox30, new object[] { driver_data_packs_ok }, false);

                            CommandType Command = (CommandType)buffer[(int)TrxPacketFields.Type];         //check type
                            if (Command.Equals(CommandType.Unused)) continue;

                            switch (Command)
                            {
                                case CommandType.GetElectData:
                                    try
                                    {
                                        Int16 Max_current = BitConverter.ToInt16(buffer, (int)TrxPacketFields.Data + 0);
                                        Int16 Max_voltage = BitConverter.ToInt16(buffer, (int)TrxPacketFields.Data + 2);
                                        Int16 Vel = BitConverter.ToInt16(buffer, (int)TrxPacketFields.Data + 4);
                                        Int16 Vel_wint = BitConverter.ToInt16(buffer, (int)TrxPacketFields.Data + 6);
                                        Int16 Open_percent = BitConverter.ToInt16(buffer, (int)TrxPacketFields.Data + 8);
                                        Int16 Reverse = BitConverter.ToInt16(buffer, (int)TrxPacketFields.Data + 10);
                                        Int16 Max_error = BitConverter.ToInt16(buffer, (int)TrxPacketFields.Data + 12);
                                        Int16 Vel_xx = BitConverter.ToInt16(buffer, (int)TrxPacketFields.Data + 14);
                                        Int32 Close_pos = BitConverter.ToInt16(buffer, (int)TrxPacketFields.Data + 16);
                                        Int16 Open_pos = BitConverter.ToInt16(buffer, (int)TrxPacketFields.Data + 20);
                                        SetFormData(textBox5, new object[] { Max_current }, false);
                                        SetFormData(textBox3, new object[] { Max_voltage }, false);
                                        SetFormData(textBox24, new object[] { Vel }, false);
                                        SetFormData(textBox2, new object[] { Vel_wint }, false);
                                        SetFormData(textBox6, new object[] { Open_percent }, false);
                                        if (Reverse > 0) SetFormData(radioButton8, new object[] { true }, false);
                                        else SetFormData(radioButton7, new object[] { true }, false);
                                        SetFormData(textBox7, new object[] { Max_error }, false);
                                        SetFormData(textBox23, new object[] { Vel_xx }, false);
                                        SetFormData(textBox27, new object[] { Close_pos }, false);
                                        SetFormData(textBox28, new object[] { Open_pos }, false);

                                    }
                                    catch (Exception) { break; }
                                    break;
                                case CommandType.GetMechData:
                                    try
                                    {
                                        Int16 Max_delay = BitConverter.ToInt16(buffer, (int)TrxPacketFields.Data + 0);
                                        Int16 Max_delay_wint = BitConverter.ToInt16(buffer, (int)TrxPacketFields.Data + 2);
                                        Int16 Max_delay_fire = BitConverter.ToInt16(buffer, (int)TrxPacketFields.Data + 4);
                                        Int16 Is_lock = BitConverter.ToInt16(buffer, (int)TrxPacketFields.Data + 6);
                                        Int16 Fire_job = BitConverter.ToInt16(buffer, (int)TrxPacketFields.Data + 8);
                                        Int16 Batt_job = BitConverter.ToInt16(buffer, (int)TrxPacketFields.Data + 10);
                                        Int16 Lock_type = BitConverter.ToInt16(buffer, (int)TrxPacketFields.Data + 12);
                                        Int16 Is_hold = BitConverter.ToInt16(buffer, (int)TrxPacketFields.Data + 14);
                                        SetFormData(textBox11, new object[] { Max_delay }, false);
                                        SetFormData(textBox13, new object[] { Max_delay_wint }, false);
                                        SetFormData(textBox14, new object[] { Max_delay_fire }, false);
                                        if (Is_lock > 0) SetFormData(radioButton2, new object[] { true }, false);
                                        else SetFormData(radioButton4, new object[] { true }, false);
                                        SetFormData(comboBox1, new object[] { Fire_job }, false);
                                        SetFormData(comboBox2, new object[] { Batt_job }, false);
                                        SetFormData(comboBox3, new object[] { Lock_type }, false);
                                        if (Is_hold > 0) SetFormData(radioButton6, new object[] { true }, false);
                                        else SetFormData(radioButton5, new object[] { true }, false);
                                    }
                                    catch (Exception) { break; }
                                    break;
                                case CommandType.GetErrorCount:
                                    err_cnt = BitConverter.ToUInt16(buffer, (int)TrxPacketFields.Data + 0);
                                    SetFormData(textBox1, new object[] { err_cnt }, false);
                                    break;
                                case CommandType.GetSystemData:
                                    try
                                    {
                                        int curr = BitConverter.ToInt32(buffer, (int)TrxPacketFields.Data + 0);
                                        float Current = curr / 55.0f;
                                        int upos = BitConverter.ToInt32(buffer, (int)TrxPacketFields.Data + 4);
                                        int pos = BitConverter.ToInt32(buffer, (int)TrxPacketFields.Data + 8);
                                        int vel = BitConverter.ToInt32(buffer, (int)TrxPacketFields.Data + 12);
                                        int acc = BitConverter.ToInt32(buffer, (int)TrxPacketFields.Data + 16);
                                        float e = BitConverter.ToSingle(buffer, (int)TrxPacketFields.Data + 20);
                                        SetFormData(this.chart1, new object[] { Current, upos, pos, vel, acc, e }, false);
                                    }
                                    catch (Exception) { break; }
                                    break;
                                case CommandType.GetDiagData:
                                    try
                                    {
                                        float c1 = BitConverter.ToSingle(buffer, (int)TrxPacketFields.Data + 0);
                                        float c2 = BitConverter.ToSingle(buffer, (int)TrxPacketFields.Data + 4);
                                        float c3 = BitConverter.ToSingle(buffer, (int)TrxPacketFields.Data + 8);
                                        float c4 = BitConverter.ToSingle(buffer, (int)TrxPacketFields.Data + 12);
                                        SetFormData(textBox26, new object[] { c1 }, false);
                                        SetFormData(textBox22, new object[] { c2 }, false);
                                        SetFormData(textBox21, new object[] { c3 }, false);
                                        SetFormData(textBox4, new object[] { c4 }, false);
                                    }
                                    catch (Exception) { break; }
                                    break;
                                case CommandType.GetErrorData:
                                        grid_cnt++;

                                        string time = "";
                                    //    int sec = buffer[(int)TrxPacketFields.Data + 0] & 0xFC >> 2;
                                    //    int min = (buffer[(int)TrxPacketFields.Data + 0] & 0x03 << 4) | (buffer[(int)TrxPacketFields.Data + 1] & 0xF0 >> 4);
                                    //    int hour = (buffer[(int)TrxPacketFields.Data + 1] & 0x0F << 1) | (buffer[(int)TrxPacketFields.Data + 2] & 0x80 >> 7);
                                    //    int day = (buffer[(int)TrxPacketFields.Data + 2] & 0x7C >> 2);
                                    //    int month = (buffer[(int)TrxPacketFields.Data + 2] & 0x03 << 2) | (buffer[(int)TrxPacketFields.Data + 3] & 0xC0 >> 6);
                                    //    int year = 2018 + (buffer[(int)TrxPacketFields.Data + 3] & 0x3F);
                                    //    time = day.ToString() + "." + month.ToString() + "." +
                                    //           year.ToString() + " " + hour.ToString() + ":" +
                                    //           min.ToString() + ":" + sec.ToString();

                                        string err = "";
                                        Error = (ErrorType)buffer[(int)TrxPacketFields.Data + 1];
                                    switch (Error)
                                        {
                                            case ErrorType.NoMotorData:
                                                err = "нет данных на мотор";
                                                break;
                                            case ErrorType.InVoltageLow:
                                                err = "низкое входное напряжение";
                                            break;
                                            case ErrorType.InVoltageNegative:
                                                err = "отрицательное входное напряжение";
                                                break;
                                            case ErrorType.AccVoltageNegative:             
                                                err = "отрицательное напряжение аккумулятора";
                                                break;
                                            case ErrorType.Overcurrent:
                                                err = "превышение по току";
                                                break;
                                            case ErrorType.PositionUnreached:
                                                err = "невозможно достичь заданную позицию";
                                                break;
                                            case ErrorType.CalibrationFailed:
                                                err = "ошибка алгоритма калибровки";
                                                break;
                                            case ErrorType.NoDataInFlash:
                                                err = "память драйвера пуста";
                                                break;
                                            case ErrorType.FlashEraseFailed:
                                                err = "память драйвера не отвечает";
                                                break;
                                            case ErrorType.ADCUnunit:
                                                err = "ошибка инициализации АЦП";
                                                break;
                                            case ErrorType.ADCConvertionFailed:
                                                err = "ошибка преобразования АЦП";
                                                break;
                                            case ErrorType.DMAUninit:
                                                err = "ошибка инициализации прямого доступа к памяти";
                                                break;
                                            case ErrorType.I2CUninit:
                                                err = "ошибка инициализации интерфейса резервного хранилища";
                                                break;
                                            case ErrorType.TimPWMUninit:
                                                err = "ошибка инициализации ШИМ";
                                                break;
                                            case ErrorType.TimEncUninit:
                                                err = "ошибка инициализации энкодера";
                                                break;
                                            case ErrorType.TimCycleUninit:
                                                err = "ошибка инициализации таймера";
                                                break;
                                            case ErrorType.UartUninit:
                                                err = "ошибка инициализации консоли";
                                                break;
                                            default:
                                                break;
                                        }

                                        string st = "";
                                        Status = (StatusType)buffer[(int)TrxPacketFields.Data + 0]; 
                                    switch (Status)
                                        {
                                            case StatusType.SystemError:
                                                st = "системная ошибка";
                                                break;
                                            case StatusType.FatalError:
                                                st = "критическая ошибка";
                                                break;
                                            default:
                                                break;
                                        }
                                            
                                        SetFormData(dataGridView1, new object[] { grid_cnt, time, err, st }, false);
                                        if (grid_cnt == err_cnt) grid_cnt = 0;
                                    break;
                                default:
                                    this.serialPort1.DiscardInBuffer();
                                    driver_data_packs_lost++;
                                    SetFormData(textBox31, new object[] { driver_data_packs_lost }, false);
                                    break;
                            }    //end of case
                        }        //end of rx address -> driver board
                    }            //end of rx address -> PC
                }
            }
        }
        
        //команда на чтение данных о моторе
        private void Read_mdata_button_Click(object sender, EventArgs e)
        {
            if (!this.serialPort1.IsOpen)
            {
                MessageBox.Show("COM-Порт закрыт!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;                            
            }

            byte[] b;
            byte[] buffer = new byte[32];
            b = BitConverter.GetBytes(magic_32);
            Array.Copy(b, 0, buffer, (int)TrxPacketFields.Start, b.Length);

            buffer[(int)TrxPacketFields.Address] = (byte)AddressType.tdrv << 4 | (byte)AddressType.pc;
            buffer[(int)TrxPacketFields.Type] = (byte)CommandType.GetElectData;
            this.serialPort1.Write(buffer, 0, buffer.Length);
        }

        //команда на чтение данных о механической системе
        private void Read_mech_button_Click(object sender, EventArgs e)
        {
            if (!this.serialPort1.IsOpen)
            {
                MessageBox.Show("COM-Порт закрыт!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            byte[] b;
            byte[] buffer = new byte[32];
            b = BitConverter.GetBytes(magic_32);
            Array.Copy(b, 0, buffer, (int)TrxPacketFields.Start, b.Length);

            buffer[(int)TrxPacketFields.Address] = (byte)AddressType.tdrv << 4 | (byte)AddressType.pc;
            buffer[(int)TrxPacketFields.Type] = (byte)CommandType.GetMechData;
            this.serialPort1.Write(buffer, 0, buffer.Length);
        }
        
        //команда на чтение данных о времени
        private void Read_time_button_Click(object sender, EventArgs e)
        {
            if (!this.serialPort1.IsOpen)
            {
                MessageBox.Show("COM-Порт закрыт!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            byte[] b;
            byte[] buffer = new byte[32];
            b = BitConverter.GetBytes(magic_32);
            Array.Copy(b, 0, buffer, (int)TrxPacketFields.Start, b.Length);

            buffer[(int)TrxPacketFields.Address] = (byte)AddressType.tdrv << 4 | (byte)AddressType.pc;
            buffer[(int)TrxPacketFields.Type] = (byte)CommandType.GetTime;
            this.serialPort1.Write(buffer, 0, buffer.Length);
        }
        
        //команда на чтение данных о системе (рыба)
        private void Read_sys_button_Click(object sender, EventArgs e)
        {
            if (!this.serialPort1.IsOpen)
            {
                MessageBox.Show("COM-Порт закрыт!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            byte[] b;
            byte[] buffer = new byte[32];
            b = BitConverter.GetBytes(magic_32);
            Array.Copy(b, 0, buffer, (int)TrxPacketFields.Start, b.Length);

            buffer[(int)TrxPacketFields.Address] = (byte)AddressType.tdrv << 4 | (byte)AddressType.pc;
            buffer[(int)TrxPacketFields.Type] = (byte)CommandType.GetDiagData;
            this.serialPort1.Write(buffer, 0, buffer.Length);
        }
               
        //команда на запись данных о моторе
        private void Write_mdata_button_Click(object sender, EventArgs e)
        {
            if (!this.serialPort1.IsOpen)
            {
                MessageBox.Show("COM-Порт закрыт!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            byte[] b;
            byte[] buffer = new byte[32];
            b = BitConverter.GetBytes(magic_32);
            Array.Copy(b, 0, buffer, (int)TrxPacketFields.Start, b.Length);

            buffer[(int)TrxPacketFields.Address] = (byte)AddressType.tdrv << 4 | (byte)AddressType.pc;
            buffer[(int)TrxPacketFields.Type] = (byte)CommandType.SetElectData;

            try
            {
                UInt16 amax = Convert.ToUInt16(textBox5.Text);
                b = BitConverter.GetBytes(amax);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 0, b.Length);

                UInt16 umax = Convert.ToUInt16(textBox3.Text);
                b = BitConverter.GetBytes(amax);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 2, b.Length);

                UInt16 scf = Convert.ToUInt16(textBox24.Text);
                b = BitConverter.GetBytes(amax);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 4, b.Length);

                UInt16 scfw = Convert.ToUInt16(textBox2.Text);
                b = BitConverter.GetBytes(amax);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 6, b.Length);

                UInt16 perc = Convert.ToUInt16(textBox6.Text);
                b = BitConverter.GetBytes(amax);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 8, b.Length);

                UInt16 rev = (UInt16)(radioButton8.Checked ? 1 : radioButton7.Checked ? 0 : 0);
                b = BitConverter.GetBytes(rev);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 10, b.Length);

                UInt16 er = Convert.ToUInt16(textBox7.Text);
                b = BitConverter.GetBytes(er);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 12, b.Length);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            
           

            this.serialPort1.Write(buffer, 0, buffer.Length);
        }

        //команда на запись данных о механической системе - только крайние положения
        private void Write_mech_button_Click(object sender, EventArgs e)
        {
            if (!this.serialPort1.IsOpen)
            {
                MessageBox.Show("COM-Порт закрыт!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            byte[] b;
            byte[] buffer = new byte[32];
            b = BitConverter.GetBytes(magic_32);
            Array.Copy(b, 0, buffer, (int)TrxPacketFields.Start, b.Length);

            buffer[(int)TrxPacketFields.Address] = (byte)AddressType.tdrv << 4 | (byte)AddressType.pc;
            buffer[(int)TrxPacketFields.Type] = (byte)CommandType.SetMechData;

            try
            {
                UInt16 del = Convert.ToUInt16(textBox11.Text);
                b = BitConverter.GetBytes(del);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 0, b.Length);

                UInt16 delw = Convert.ToUInt16(textBox13.Text);
                b = BitConverter.GetBytes(delw);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 2, b.Length);

                UInt16 delf = Convert.ToUInt16(textBox14.Text);
                b = BitConverter.GetBytes(delf);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 4, b.Length);

                UInt16 lck = (UInt16)(radioButton2.Checked ? 1 : radioButton4.Checked ? 0 : 0);
                b = BitConverter.GetBytes(lck);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 6, b.Length);

                UInt16 fire = Convert.ToUInt16(comboBox1.SelectedItem);
                b = BitConverter.GetBytes(fire);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 8, b.Length);

                UInt16 batt = Convert.ToUInt16(comboBox2.SelectedItem);
                b = BitConverter.GetBytes(batt);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 10, b.Length);

                UInt16 ltype = Convert.ToUInt16(comboBox3.SelectedItem);
                b = BitConverter.GetBytes(ltype);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 12, b.Length);

                UInt16 hold = (UInt16)(radioButton6.Checked ? 1 : radioButton5.Checked ? 0 : 0);
                b = BitConverter.GetBytes(hold);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 14, b.Length);

                UInt16 er = Convert.ToUInt16(textBox7.Text);
                b = BitConverter.GetBytes(er);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 12, b.Length);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            
            this.serialPort1.Write(buffer, 0, buffer.Length);
        }
        
        //команда на запись времени
        private void Write_time_button_Click(object sender, EventArgs e)
        {
            if (!this.serialPort1.IsOpen)
            {
                MessageBox.Show("COM-Порт закрыт!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            byte[] b;
            byte[] buffer = new byte[32];
            b = BitConverter.GetBytes(magic_32);
            Array.Copy(b, 0, buffer, (int)TrxPacketFields.Start, b.Length);

            buffer[(int)TrxPacketFields.Address] = (byte)AddressType.tdrv << 4 | (byte)AddressType.pc;
            buffer[(int)TrxPacketFields.Type] = (byte)CommandType.SetTime;

            try
            {
                int min = Convert.ToInt32(textBox20.Text);
                b = BitConverter.GetBytes(min);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 0, b.Length);

                int hour = Convert.ToInt32(textBox19.Text);
                b = BitConverter.GetBytes(hour);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 4, b.Length);

                int day = Convert.ToInt32(textBox18.Text);
                b = BitConverter.GetBytes(day);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 8, b.Length);

                int mon = Convert.ToInt32(textBox17.Text);
                b = BitConverter.GetBytes(mon);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 12, b.Length);

                int year = Convert.ToInt32(textBox16.Text);
                b = BitConverter.GetBytes(year);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 18, b.Length);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            this.serialPort1.Write(buffer, 0, buffer.Length);
        }

        //команда на запись системных настроек
        private void Write_sys_button_Click(object sender, EventArgs e)
        {
            if (!this.serialPort1.IsOpen)
            {
                MessageBox.Show("COM-Порт закрыт!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            byte[] b;
            byte[] buffer = new byte[32];
            b = BitConverter.GetBytes(magic_32);
            Array.Copy(b, 0, buffer, (int)TrxPacketFields.Start, b.Length);

            buffer[(int)TrxPacketFields.Address] = (byte)AddressType.tdrv << 4 | (byte)AddressType.pc;
            buffer[(int)TrxPacketFields.Type] = (byte)CommandType.SetDiagData;

            try
            {
                float c1 = Convert.ToSingle(textBox26.Text);
                b = BitConverter.GetBytes(c1);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 0, b.Length);

                float c2 = Convert.ToSingle(textBox22.Text);
                b = BitConverter.GetBytes(c2);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 4, b.Length);

                float c3 = Convert.ToSingle(textBox21.Text);
                b = BitConverter.GetBytes(c3);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 8, b.Length);

                float c4 = Convert.ToSingle(textBox4.Text);
                b = BitConverter.GetBytes(c4);
                Array.Copy(b, 0, buffer, (int)TrxPacketFields.Data + 8, b.Length);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            this.serialPort1.Write(buffer, 0, buffer.Length);
        }
        
        //команда на чтение текущих системных показателей
        private void Read_datalog_button_Click(object sender, EventArgs e)
        {
            if (!this.serialPort1.IsOpen)
            {
                MessageBox.Show("COM-Порт закрыт!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            byte[] b;
            byte[] buffer = new byte[32];
            b = BitConverter.GetBytes(magic_32);
            Array.Copy(b, 0, buffer, (int)TrxPacketFields.Start, b.Length);

            buffer[(int)TrxPacketFields.Address] = (byte)AddressType.tdrv << 4 | (byte)AddressType.pc;
            buffer[(int)TrxPacketFields.Type] = (byte)CommandType.GetSystemData;
            this.serialPort1.Write(buffer, 0, buffer.Length);
        }
        
        //команда калибровки
        private void Calib_button_Click(object sender, EventArgs e)
        {
            if (!this.serialPort1.IsOpen)
            {
                MessageBox.Show("COM-Порт закрыт!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            byte[] b;
            byte[] buffer = new byte[32];
            b = BitConverter.GetBytes(magic_32);
            Array.Copy(b, 0, buffer, (int)TrxPacketFields.Start, b.Length);

            buffer[(int)TrxPacketFields.Address] = (byte)AddressType.tdrv << 4 | (byte)AddressType.pc;
            buffer[(int)TrxPacketFields.Type] = (byte)CommandType.GetCalibrate;
            this.serialPort1.Write(buffer, 0, buffer.Length);
        }
        
        //команда открыть двери
        private void Open_button_Click(object sender, EventArgs e)
        {
            if (!this.serialPort1.IsOpen)
            {
                MessageBox.Show("COM-Порт закрыт!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            byte[] b;
            byte[] buffer = new byte[32];
            b = BitConverter.GetBytes(magic_32);
            Array.Copy(b, 0, buffer, (int)TrxPacketFields.Start, b.Length);

            buffer[(int)TrxPacketFields.Address] = (byte)AddressType.tdrv << 4 | (byte)AddressType.pc;
            buffer[(int)TrxPacketFields.Type] = (byte)CommandType.SetDoorOpen;
            this.serialPort1.Write(buffer, 0, buffer.Length);
        }

        //команда закрыть дверь
        private void Close_button_Click(object sender, EventArgs e)
        {
            if (!this.serialPort1.IsOpen)
            {
                MessageBox.Show("COM-Порт закрыт!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            byte[] b;
            byte[] buffer = new byte[32];
            b = BitConverter.GetBytes(magic_32);
            Array.Copy(b, 0, buffer, (int)TrxPacketFields.Start, b.Length);

            buffer[(int)TrxPacketFields.Address] = (byte)AddressType.tdrv << 4 | (byte)AddressType.pc;
            buffer[(int)TrxPacketFields.Type] = (byte)CommandType.SetDoorClose;
            this.serialPort1.Write(buffer, 0, buffer.Length);
        }

        //команда на чтение лога ошибок
        private void dataGridView1_DoubleClick(object sender, EventArgs e)
        {
            if (!this.serialPort1.IsOpen)
            {
                MessageBox.Show("COM-Порт закрыт!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            byte[] b;
            byte[] buffer = new byte[32];
            b = BitConverter.GetBytes(magic_32);
            Array.Copy(b, 0, buffer, (int)TrxPacketFields.Start, b.Length);

            buffer[(int)TrxPacketFields.Address] = (byte)AddressType.tdrv << 4 | (byte)AddressType.pc;
            buffer[(int)TrxPacketFields.Type] = (byte)CommandType.GetErrorData;
            this.serialPort1.Write(buffer, 0, buffer.Length);
        }

        //команда на чтение числа ошибок
        private void Read_ErrCnt_Click(object sender, EventArgs e)
        {
            if (!this.serialPort1.IsOpen)
            {
                MessageBox.Show("COM-Порт закрыт!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            byte[] b;
            byte[] buffer = new byte[32];
            b = BitConverter.GetBytes(magic_32);
            Array.Copy(b, 0, buffer, (int)TrxPacketFields.Start, b.Length);

            buffer[(int)TrxPacketFields.Address] = (byte)AddressType.tdrv << 4 | (byte)AddressType.pc;
            buffer[(int)TrxPacketFields.Type] = (byte)CommandType.GetErrorCount;
            this.serialPort1.Write(buffer, 0, buffer.Length);
        }

        //масштабирование графика
        private void tabControl1_KeyUp(object sender, KeyEventArgs e)
        {
            if (this.tabControl1.SelectedTab == this.tabControl1.TabPages[1])
            {
                if (e.Control)
                {
                    if (e.KeyCode.Equals(Keys.Oemplus)) this.chart1.Scale(new SizeF(2, 2));    //rewrite it
                    else if (e.KeyCode.Equals(Keys.OemMinus)) this.chart1.Scale(new SizeF(0.5f, 0.5f));
                }
            }
        }
    }
}

