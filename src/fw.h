#ifndef FW_H_
#define FW_H_

//������ ������ � ������� ����������� ����� �� ������� ��� ������ � flash
#define FW_BLOCK_SIZE        (512) // ������ ����� ��� ���������, ������������� �������� �� 1 ����

void fw_app_check(void);

result_t fw_copy(uint8_t * dst, uint8_t *src, uint32_t src_size);

result_t fw_erase_boot(void);   // �������� ������� BOOT
result_t fw_erase_app(void);    // �������� ������� APP
result_t fw_erase_fwcopy(void); // �������� ������� FW_COPY

result_t fw_write_data(uint32_t adr_wr, uint8_t *src_data, uint32_t len);
result_t fw_upgrade(uint8_t *fw_bin);

#endif