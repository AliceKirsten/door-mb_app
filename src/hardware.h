#ifndef HARDWARE_H_
#define HARDWARE_H_

#include "msg_types.h"

// ��������� ������
#define KEY_GPIO_UP    (1)  // ������ �� ������
#define KEY_GPIO_DOWN  (0)  // ������ ������


void uart_drv_init(void);
void uart_pc_init(void);
void uart_console_init(void);
void buzz_init( void );
void buzz_on(void);
void buzz_off(void);
input_dat_state_e get_sensor_power( void );
void sensor_power_on( void );
void sensor_power_off( void );
pdu_state_e get_pdu_state( void );
input_dat_state_e get_radar1_state( void );
input_dat_state_e get_radar2_state( void );
input_dat_state_e get_bar1_state( void );
input_dat_state_e get_bar2_state( void );
input_dat_state_e get_key_stop_state( void );
input_dat_state_e get_key_test_state( void );
input_dat_state_e get_fire_state( void );
void driver_pcb_reset( void );
void driver_pcb_reset_init( void );
void dat_init(void);
void keyboard_init(void);
uint8_t key1_get_status(void);
uint8_t key2_get_status(void);
uint8_t key3_get_status(void);
uint8_t key4_get_status(void);
void run_reboot(void);
void lock_init(void);
result_t lock_open(void);
result_t lock_close(void);

#endif