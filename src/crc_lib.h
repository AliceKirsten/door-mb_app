#ifndef CRC_LIB_H_
#define CRC_LIB_H_

#include <stdint.h>

uint16_t calc_crc16( uint8_t *adr, uint32_t len);
uint16_t calc_ms_crc16( uint8_t *msg_p, uint32_t length);


// ETHERNET, winrar, zip & etc
uint32_t calc_crc32_eth( uint8_t *adr, uint32_t len);

// For IAR
uint32_t calc_crc32_iar( uint8_t *adr, uint32_t len);

#endif /* CRC32_H_ */
