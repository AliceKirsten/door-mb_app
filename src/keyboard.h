#ifndef _KEYBOARD_CMD_H_
#define _KEYBOARD_CMD_H_
#include <stdint.h>

// ���� ������, ����� �������� �����
#define KEY_UP   '1'
#define KEY_DOWN '2'
#define KEY_EXIT '3'
#define KEY_OK   '4'
//#define KEY_QUIT 'q'

#define KEY_TIMEOUT_MS (20)  // ����� �������� ������������ ����������� ��������� (�����������) ��

typedef enum {
    KEY_POS_NONE = 0,  // �� ������
    KEY_POS_DOWN,      // ������
//    KEY_POS_UP,        // ������ (��������� ������)
    KEY_POS_CLICK      // ������ ���� ������-������
} key_position_t;

// ��������� �������� ������� �� ������
typedef enum {
    KEY_WAIT_DOWN,  // �������� ������� �� ������
    KEY_DELAY_DOWN, // �������� ��� �������� �� ����������� (������� ������)
    KEY_TEST_DOWN,  // �������� ������ ��� ��� ������
    
    KEY_WAIT_UP,    // �������� ������� ������
    KEY_DELAY_UP,   // �������� ��� �������� �� ����������� (������� ������)
    KEY_TEST_UP     // �������� ������ ������
} keyboard_automat_st;

typedef struct {
    keyboard_automat_st key_auto; // ��������� �������� ������ ������
    uint8_t (*get_key_status)();  // ������������ ��������� ��������� ������ �� ����� GPIO
    key_position_t key;           // ��������� ������
    uint32_t t_start;             // ����� ������ ������� ������
    uint32_t t_stop;              // ����� ������� ������
    uint32_t t_down;              // �������� ����� �������� � �������� ������
} key_status_t;


void key_status( key_status_t *key_st );

#endif