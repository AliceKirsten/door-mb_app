#ifndef _FIRMWARE_HEADER_20_H_
#define _FIRMWARE_HEADER_20_H_

// ----------------------------------------------------------------------------
// for Little - endian
extern const char FIRMWARE_HEAD_NAME_C[4];
extern const uint32_t FIRMWARE_HEAD_NAME_B;

#define FW_SOFT_TYPES_MBOOT (0) // ������(������� ����������) - ���������
#define FW_SOFT_TYPES_MAPP  (1) // ������(������� ����������) - ���������� ��
#define FW_SOFT_TYPES_SBOOT (2) // �������(������ ����������) - ���������
#define FW_SOFT_TYPES_SAPP  (3) // �������(������ ����������) - ���������� ��

typedef struct { 
	unsigned char hversion; // FIX = 2 !
    unsigned char v1;
	unsigned char v2;
	unsigned char v3;
} ver3_2_1_h;          // version = v1 v2 v3 02

#define FIRMWARE_VERSION_2  (0x02)
typedef struct {
	union { 
		uint32_t bin4;
	    char char_name[4];       // 'F' 'I' 'R' 'M'
	} name;

	union { 
		uint32_t bin4;
		ver3_2_1_h;
		//unsigned char hversion; // FIX = 2 !
	    //unsigned char v1;
		//unsigned char v2;
		//unsigned char v3;
	} version;          // version = v1 v2 v3 02

	uint32_t soft_types; // Boot=0 or App=1 ....

	uint32_t len;       // len data file, bytes
	uint32_t crc32;     // crc32
}firmware_header_v20_t;

#define FIRMWARE_HEADER_V2_0_SIZE  (sizeof(firmware_header_v20_t))





#endif