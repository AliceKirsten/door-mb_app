#include <stdint.h>
#include <stddef.h>
#include "main.h"
#include "i2c_745.h"
#include "log_array.h"


// ��������� ����� ��� ������������ ������ ������ � I2C ����������, ����� + ������
static uint8_t i2c_packet_buf[ 16 ];

//-----------------------------------------------------------------------------
// ������ 1 �������� � �������
// � ������ ������������ ������������ ���������� �������(out) ��������
//-----------------------------------------------------------------------------
log_return_st log_set_rec( log_allocation_table_t * lat, log_record_t log_r)
{
	uint16_t n;
	uint16_t adr;
	uint8_t res;
	log_return_st ret;
	uint8_t *p;
	size_t i;

	// ������� ���� ���������� ����� � ������� ?
	n = LOG_RECORD_N - log_rec_quantity( lat );
	if (n == 1){ // �������� ����� �� 1 ������� = ����� ��� ! �������� �������� ��������� �� 1 ��� ����� ����������� ����� ��� ��� 1 ��������, ������������� ������� - ��� ����� ������� �������
		// �������� ������ ������� ��� � ��������� ��� ����� ���� �� ��� ����� ���������� ��������� �������
		lat->out1++;
		if (lat->out1 == LOG_RECORD_N) lat->out1 = 0;
		lat->out2 = lat->out1;

		// ��������� (��������������) LAT(RAM) � LAT(FLASH)
		ret = LOG_RET_OK; 
        ret |= log_write_lat1( lat );
        ret |= log_write_lat2( lat );
		if (ret == LOG_RET_ERR) return ret;
	}

	adr = LOG_ARRAY_ADR + lat->in1 * sizeof(log_record_t);

	// ���������� ������� � ������
	p = (uint8_t*)&log_r;
	i2c_packet_buf[ 0 ] = (adr & 0xff00) >> 8;
	i2c_packet_buf[ 1 ] = (adr & 0x00ff) >> 0;
	for (i=0; i<sizeof(log_record_t); i++){
	    i2c_packet_buf[ 2 + i ] = (uint8_t)*(p + i);
	}

	res = i2c_write(U4_I2C_ADR, i2c_packet_buf, 2 + sizeof(log_record_t));
	if (res) return LOG_RET_ERR;

	// �������� ��������� ������� �� ��������� �������
	lat->in1++;
	if (lat->in1 == LOG_RECORD_N) lat->in1 = 0;
    lat->in2 = lat->in1;

	// ��������� (��������������) LAT(RAM) � LAT(FLASH)
	ret = LOG_RET_OK;
    ret |= log_write_lat1( lat );
    ret |= log_write_lat2( lat );
	return ret;
}

//-----------------------------------------------------------------------------
// ������ �������� �� �������
//-----------------------------------------------------------------------------
log_return_st log_get_rec( log_allocation_table_t * lat, log_record_t * log_r)
{
	uint16_t n;
	uint16_t adr;
	uint8_t res;
	log_return_st ret;
	
	// ������� ���� ��������� � ������� ?
	n = log_rec_quantity( lat );
	if (n == 0) return LOG_RET_ERR; // ��� ������� - ������ (������� ���������, �� ���� ���)

	adr = LOG_ARRAY_ADR + lat->out1 * sizeof(log_record_t);

	// ������ ������
	res = i2c_read_adr_16bit(U4_I2C_ADR, adr, (uint8_t *)log_r, sizeof(log_record_t));
	if (res) return LOG_RET_ERR;

	// �������� ��������� ����������� ��������� �� 1
	lat->out1++;
	if (lat->out1 == LOG_RECORD_N) lat->out1 = 0;
	lat->out2 = lat->out1;

	// ��������� (��������������) LAT(RAM) � LAT(FLASH)
	ret = LOG_RET_OK;
    ret |= log_write_lat1( lat );
    ret |= log_write_lat2( lat );
	return ret;
}

//-----------------------------------------------------------------------------
// ���������� ��������� � �������
//-----------------------------------------------------------------------------
uint16_t log_rec_quantity(log_allocation_table_t * lat)
{
	if (lat->in1 == lat->out1){
		return 0;
	}

	if (lat->in1 > lat->out1){
		return lat->in1 - lat->out1;
	}

	if (lat->in1 < lat->out1){
		return LOG_RECORD_N - lat->out1 + lat->in1;
	}

	return 0;
}

//-----------------------------------------------------------------------------
// ������ ����������(��������� ������) �� FLASH ������
//
// � ������ ������ ������������ ����� ���������� � �������� ���������
//-----------------------------------------------------------------------------
log_return_st log_get_lat(log_allocation_table_t * lat)
{
	int res = 0;
    log_return_st ret;

	res |= i2c_read_adr_16bit( U4_I2C_ADR, LOG_LAT1_LOCK_ADR, &lat->lock1, LOG_LAT1_LOCK_SIZE);
	res |= i2c_read_adr_16bit( U4_I2C_ADR, LOG_LAT1_IN_ADR,   (uint8_t*)&lat->in1,   LOG_LAT1_IN_SIZE);
	res |= i2c_read_adr_16bit( U4_I2C_ADR, LOG_LAT1_OUT_ADR,  (uint8_t*)&lat->out1,  LOG_LAT1_OUT_SIZE);

	res |= i2c_read_adr_16bit( U4_I2C_ADR, LOG_LAT2_LOCK_ADR, &lat->lock2, LOG_LAT2_LOCK_SIZE);
	res |= i2c_read_adr_16bit( U4_I2C_ADR, LOG_LAT2_IN_ADR,   (uint8_t*)&lat->in2,   LOG_LAT2_IN_SIZE);
	res |= i2c_read_adr_16bit( U4_I2C_ADR, LOG_LAT2_OUT_ADR,  (uint8_t*)&lat->out2,  LOG_LAT2_OUT_SIZE);

	if (res) return LOG_RET_ERR;

	// ��� �������, ������ ������
	if (lat->lock1 == 0xff && 
		lat->in1   == 0xffff &&
		lat->out1  == 0xffff &&
	    lat->lock2 == 0xff && 
		lat->in2   == 0xffff &&
		lat->out2  == 0xffff
		){

		// ��������� �������� � ��������� ���������
        lat->lock1 = LOG_UNLOCK;
		lat->in1   = 0;
		lat->out1  = 0;
	    lat->lock2 = LOG_UNLOCK;
		lat->in2   = 0;
		lat->out2  = 0;

		ret = log_write_lat1(lat); // ������ � ������� ���������� ������ ���������
		ret |= log_write_lat2(lat); // ������ � ������� ���������� ������ ���������
        return ret;
	}

	// ��� ��������� LOCK1, LOCK2 (��� ��������� ���������)
	if (lat->lock1 == LOG_LOCK && 
	    lat->lock2 == LOG_LOCK
		){
		// ��������� �������� � ��������� ���������
        lat->lock1 = LOG_UNLOCK;
		lat->in1   = 0;
		lat->out1  = 0;
	    lat->lock2 = LOG_UNLOCK;
		lat->in2   = 0;
		lat->out2  = 0;

		ret = log_write_lat1(lat); // ������ � ������� ���������� ������ ���������
		ret |= log_write_lat2(lat); // ������ � ������� ���������� ������ ���������
        return ret;
	}

	// ��������� �� ����� LOCK1 != UNLOCK2 (���������� ������ ����������....)
	if (lat->lock1 == LOG_LOCK && 
	    lat->lock2 == LOG_UNLOCK
		){
		if (lat->in2 > LOG_RECORD_N || lat->out2 > LOG_RECORD_N){ // �������� ��������, ���� ��� ��������� �� �������� !
			lat->in2 = 0;
			lat->out2 = 0;
		}
		// ��������
        lat->lock1 = LOG_UNLOCK;
		lat->in1   = lat->in2;
		lat->out1  = lat->out2;
	    
		ret = log_write_lat1(lat); // ������ � ������� ���������� ������ ���������
        return ret;
	}

	// ��������� �� ����� UNLOCK1 != LOCK2 (���������� ������ ����������....)
	if (lat->lock1 == LOG_UNLOCK && 
	    lat->lock2 == LOG_LOCK
		){
		if (lat->in1 > LOG_RECORD_N || lat->out1 > LOG_RECORD_N){ // �������� ��������, ���� ��� ��������� �� �������� !
			lat->in1 = 0;
			lat->out1 = 0;
		}
		// ��������
        lat->lock2 = LOG_UNLOCK;
		lat->in2   = lat->in1;
		lat->out2  = lat->out1;
	    
		ret = log_write_lat2(lat); // ������ � ������� ���������� ������ ���������
        return ret;
	}

	// ��������� �� ����� 1 != 2
	if (lat->lock1 != lat->lock2){
		if (lat->in1 > LOG_RECORD_N || lat->out1 > LOG_RECORD_N){ // �������� ��������, ���� ��� ��������� �� �������� !
			lat->in1 = 0;
			lat->out1 = 0;
		}
		if (lat->in2 > LOG_RECORD_N || lat->out2 > LOG_RECORD_N){ // �������� ��������, ���� ��� ��������� �� �������� !
			lat->in2 = 0;
			lat->out2 = 0;
		}
		// ��������
        lat->lock1 = LOG_UNLOCK;
        lat->lock2 = LOG_UNLOCK;
	    
		ret = log_write_lat1(lat); // ������ � ������� ���������� ������ ���������
		ret |= log_write_lat2(lat); // ������ � ������� ���������� ������ ���������
        return ret;
	}

    return LOG_RET_OK;
}


//-----------------------------------------------------------------------------
// ������ ������ ��������� �� ����
//-----------------------------------------------------------------------------
log_return_st log_write_lat1(log_allocation_table_t * lat)
{
	uint8_t res;

	// �������� - lat1 ����� !
	i2c_packet_buf[ 0 ] = (LOG_LAT1_LOCK_ADR & 0xff00) >> 8;
	i2c_packet_buf[ 1 ] = (LOG_LAT1_LOCK_ADR & 0x00ff) >> 0;
	i2c_packet_buf[ 2 ] = LOG_LOCK;
	lat->lock1 = LOG_LOCK;

	res = i2c_write(U4_I2C_ADR, i2c_packet_buf, 3);
	if (res) return LOG_RET_ERR;

	// �������� - lat1.in
	i2c_packet_buf[ 0 ] = (LOG_LAT1_IN_ADR & 0xff00) >> 8;
	i2c_packet_buf[ 1 ] = (LOG_LAT1_IN_ADR & 0x00ff) >> 0;
	i2c_packet_buf[ 2 ] = (lat->in1 & 0x00ff) >> 0;
	i2c_packet_buf[ 3 ] = (lat->in1 & 0xff00) >> 8;

	res = i2c_write(U4_I2C_ADR, i2c_packet_buf, 4);
	if (res) return LOG_RET_ERR;

	// �������� - lat1.out
	i2c_packet_buf[ 0 ] = (LOG_LAT1_OUT_ADR & 0xff00) >> 8;
	i2c_packet_buf[ 1 ] = (LOG_LAT1_OUT_ADR & 0x00ff) >> 0;
	i2c_packet_buf[ 2 ] = (lat->out1 & 0x00ff) >> 0;
	i2c_packet_buf[ 3 ] = (lat->out1 & 0xff00) >> 8;

	res = i2c_write(U4_I2C_ADR, i2c_packet_buf, 4);
	if (res) return LOG_RET_ERR;

	// �������� - lat1 �������� !
	i2c_packet_buf[ 0 ] = (LOG_LAT1_LOCK_ADR & 0xff00) >> 8;
	i2c_packet_buf[ 1 ] = (LOG_LAT1_LOCK_ADR & 0x00ff) >> 0;
	i2c_packet_buf[ 2 ] = LOG_UNLOCK;
	lat->lock1 = LOG_UNLOCK;

	res = i2c_write(U4_I2C_ADR, i2c_packet_buf, 3);
	if (res) return LOG_RET_ERR;

	return LOG_RET_OK;
}

//-----------------------------------------------------------------------------
// ������ ������ ��������� �� ����
//-----------------------------------------------------------------------------
log_return_st log_write_lat2(log_allocation_table_t * lat)
{
	uint8_t res;

	// �������� - lat1 ����� !
	i2c_packet_buf[ 0 ] = (LOG_LAT2_LOCK_ADR & 0xff00) >> 8;
	i2c_packet_buf[ 1 ] = (LOG_LAT2_LOCK_ADR & 0x00ff) >> 0;
	i2c_packet_buf[ 2 ] = LOG_LOCK;
	lat->lock2 = LOG_LOCK;

	res = i2c_write(U4_I2C_ADR, i2c_packet_buf, 3);
	if (res) return LOG_RET_ERR;

	// �������� - lat1.in
	i2c_packet_buf[ 0 ] = (LOG_LAT2_IN_ADR & 0xff00) >> 8;
	i2c_packet_buf[ 1 ] = (LOG_LAT2_IN_ADR & 0x00ff) >> 0;
	i2c_packet_buf[ 2 ] = (lat->in2 & 0x00ff) >> 0;
	i2c_packet_buf[ 3 ] = (lat->in2 & 0xff00) >> 8;

	res = i2c_write(U4_I2C_ADR, i2c_packet_buf, 4);
	if (res) return LOG_RET_ERR;

	// �������� - lat1.out
	i2c_packet_buf[ 0 ] = (LOG_LAT2_OUT_ADR & 0xff00) >> 8;
	i2c_packet_buf[ 1 ] = (LOG_LAT2_OUT_ADR & 0x00ff) >> 0;
	i2c_packet_buf[ 2 ] = (lat->out2 & 0x00ff) >> 0;
	i2c_packet_buf[ 3 ] = (lat->out2 & 0xff00) >> 8;

	res = i2c_write(U4_I2C_ADR, i2c_packet_buf, 4);
	if (res) return LOG_RET_ERR;

	// �������� - lat1 �������� !
	i2c_packet_buf[ 0 ] = (LOG_LAT2_LOCK_ADR & 0xff00) >> 8;
	i2c_packet_buf[ 1 ] = (LOG_LAT2_LOCK_ADR & 0x00ff) >> 0;
	i2c_packet_buf[ 2 ] = LOG_UNLOCK;
	lat->lock2 = LOG_UNLOCK;

	res = i2c_write(U4_I2C_ADR, i2c_packet_buf, 3);
	if (res) return LOG_RET_ERR;

	return LOG_RET_OK;
}
