// ������ - ������ ������������ ���������� � I2C ������
#include <stdint.h>
#include <string.h>
#include "printf_hal.h"
#include "i2c_745.h"
#include "main.h"
#include "crc_lib.h"
#include "msg_types.h"

#ifndef DEBUG_DC
#define DEBUG_DC 0
#endif

// �������� �� ���������, ��� ������������ � ������� ���������� ������ ��� �������� ������
static device_config_t dev_cfg_default = {
// �������� �������� �� ���������, ���� ���. �������� ����� ����������� �� I2C ������ !
//    device_config.speed_open            = 10;  // �������� ��������   (��/���)
    10,
//    device_config.speed_close           = 10;  // �������� ��������  (��/���)
    10,
//    device_config.delay_open            = 5;   // �������� � �������� ��������� (���)
    5,
//    device_config.lock_pos    = LOCK_POS_CLOSE;// ��������� ����� � ������ "������ �����" (��-���)
    LOCK_POS_CLOSE,
//    device_config.reaction_fire_input   = 1;   // ������� �� �������� ���� 1,2,3 - ���� �� ���������� � ��
    1,
//    device_config.delay_open_fire       = 10;  // �������� � �������� ��������� �� ������� ��������� ����� (���)
    10,
//    device_config.batt_job_n            = 1;   // ������ � �������������  1,2,3,4 - ������ �� ��
    1,
//    device_config.lock_type_num         = 1;   // ��� ����� 1,2,3
    1,
//    device_config.motor_stop            = MOTOR_STOP_YES; // ��������� ���������� � �������� ��������� (��-���)
    MOTOR_STOP_YES,
    
//    motor_i_max             // ������������ ��� ��������� (�) - �� ��������
    3,
//    motor_v_max             // ������������ ���������� ��������� (������) - �� ��������
    42,
//    speed_open_winter       // �������� �������� ������ � ������ ������ (�.�)
    100,
//    delay_open_winter       // �������� �������� ������ � ������ ������ (���)
    5,
//    open_width_in_winter    // ������ �������� ������ � ������ ������ (%)
    80,
//    end_position_inversion  // �������� �������� ��������� (��-���) END_POS_INV_YES, END_POS_INV_NO
    END_POS_INV_NO,
//    max_position_error      // ������������ ������ ������� (�.�)
    100,

//    device_config.crc16;                // ����������� ����� - �� ��� ��������� ����������� ������
     0
};


//------------------------------------------------------------------------------
// ���������� ��������� ���������� �� ���������
//------------------------------------------------------------------------------
static void dev_cfg_set_def_val(device_config_t *d)
{
    memcpy(d, &dev_cfg_default, sizeof(device_config_t));
}

//------------------------------------------------------------------------------
// ������ ����������
//------------------------------------------------------------------------------
static void dev_cfg_print(device_config_t *d)
{
    printf_d("device_config.speed_open            = %d\n", d->speed_open);            // �������� ��������   (��/���)
    printf_d("device_config.speed_close           = %d\n", d->speed_close);           // �������� ��������  (��/���)
    printf_d("device_config.delay_open            = %d\n", d->delay_open);            // �������� � �������� ��������� (���)
    printf_d("device_config.lock_pos              = %d\n", d->lock_pos);              // ��������� ����� � ������ "������ �����" (��-���)
    printf_d("device_config.reaction_fire_input   = %d\n", d->reaction_fire_input);   // ������� �� �������� ���� 1,2,3 - ���� �� ���������� � ��
    printf_d("device_config.delay_open_fire       = %d\n", d->delay_open_fire);       // �������� � �������� ��������� �� ������� ��������� ����� (���)
    printf_d("device_config.batt_job_n            = %d\n", d->batt_job_n);            // ������ � �������������  1,2,3,4 - ������ �� ��
    printf_d("device_config.lock_type_num         = %d\n", d->lock_type_num);         // ��� ����� 1,2,3
    printf_d("device_config.motor_stop            = %d\n", d->motor_stop);            // ��������� ���������� � �������� ��������� (��-���)
    printf_d("device_config.motor_i_max           = %d\n", d->motor_i_max);           // ������������ ��� ��������� (�) - �� ��������
    printf_d("device_config.motor_v_max           = %d\n", d->motor_v_max);           // ������������ ���������� ��������� (������) - �� ��������
    printf_d("device_config.speed_open_winter     = %d\n", d->speed_open_winter);     // �������� �������� ������ � ������ ������ (�.�)
    printf_d("device_config.delay_open_winter     = %d\n", d->delay_open_winter);     // �������� �������� ������ � ������ ������ (���)
    printf_d("device_config.open_width_in_winter  = %d\n", d->open_width_in_winter);  // ������ �������� ������ � ������ ������ (%)
    printf_d("device_config.end_position_inversion= %d\n", d->end_position_inversion);// �������� �������� ��������� (��-���) END_POS_INV_YES, END_POS_INV_NO
    printf_d("device_config.max_position_error    = %d\n", d->max_position_error);    // ������������ ������ ������� (�.�)

    printf_d("device_config.crc16                 = 0x%04X\n", d->crc16); // ����������� ����� - �� ��� ��������� ����������� ������
}

//------------------------------------------------------------------------------
// ������ ���������� � ������.
//------------------------------------------------------------------------------
int dev_cfg_write(device_config_t *d)
{
    int res = 0;
    if (DEBUG_DC){
        printf_d("dev_cfg_write:\n");
        dev_cfg_print(d);
    }

    d->crc16 = calc_crc16((uint8_t *)d, sizeof(device_config_t) - 2); // -2 � ����� crc
    res = i2c_write_adr_16bit(U4_I2C_ADR, DEVICE_CONFIG_ADR, (uint8_t *)d, sizeof(device_config_t)); // ���������� �������� �� ���������
    if (DEBUG_DC) printf_d("dev_cfg_write:\n i2c_write_adr_16bit result = %d\n", res);
    return res;
}

//------------------------------------------------------------------------------
// ������ ���������� �� FLASH I2C ������
//
// ���� ��������� ������ I/O ��� ��������� crc, 
// �� ������ ��������������� � ��������� �� ���������.
// ��� ������ �� ������������ ������.
//
//------------------------------------------------------------------------------
int dev_cfg_read(device_config_t *d)
{
    int res = 0;
    uint16_t val_crc16 = 0;
    
    if (DEBUG_DC){
        printf_d("dev_cfg_read:\n");
        //dev_cfg_print(d);
    }

    res = i2c_read_adr_16bit(U4_I2C_ADR, DEVICE_CONFIG_ADR, (uint8_t *)d, sizeof(device_config_t));
    if (DEBUG_DC){
        printf_d("dev_cfg_read:\n i2c_read_adr_16bit result = %d\n", res);
        dev_cfg_print(d);
    }

    val_crc16 = calc_crc16((uint8_t *)d, sizeof(device_config_t));

    if (val_crc16 != 0 || d->crc16 == 0){
        printf_d("ERROR: DEVICE_CONFIG CRC !\nSet Default value.\n");
        dev_cfg_set_def_val(d); // ���������� �������� �� ���������
        res = dev_cfg_write(d); // �������� �������� � ������
        return res;
    }

    return res;
}


