#ifndef TIME_HAL_H_
#define TIME_HAL_H_

#include <stdint.h>

void delay_ms( const uint32_t delay );
void delay_s( const uint32_t delay );
uint32_t time_delta(uint32_t t1, uint32_t t2);

#endif /*TIME_HAL_H_*/
