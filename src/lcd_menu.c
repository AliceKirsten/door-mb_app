#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <time.h>
#include "printf_hal.h"
#include "lcd.h"
#include "lcd_menu.h"
#include "msg_types.h"
#include "os.h"
#include "keyboard.h"
#include "sys_timer.h"
#include "rtc.h"
#include "device_config.h"

extern device_post_t device_post;
extern device_config_t device_config;
extern xQueueHandle xKey;                         // ������� �� ������ Task Keyboard - ���������� ������� ������
extern portTickType xTimeMenuTimeout;

input_val_t input_val;
lcd_state_e lcd_st;
menu_position_t menu_pos;

static int flag_error = 0; // ���� ������, �������� �������� �������� ���������� (�� ����� � �������� ��������)

#define MENU_STR_VAL_SIZE  (21 + 1)
static char menu_str_val[ MENU_STR_VAL_SIZE ]; // ������ ��� ������ �� ����� (��� sprintf)

const char table_num_0_10[] = {"0123456789"}; // ������� ����������� ��������
const char table_num_1_4[]  = {"1234"};
const char table_num_1_3[]  = {"123"};
const char table_skip[]     = {".:-"}; // ������� ������� ������������


#define VAL_SIZE 16
static char val[ VAL_SIZE ]; // ����� ��������� �������� ��������-������ (���������� �����������)

#pragma diag_suppress=Pa039
static const menu_t menu_array[] = {
	// txt                     call,                types,     int_size, sprintf, ascii, min, max, val
  	{ "���. ����            ", lcd_call_input_txt,  LCD_IN_D_T_TXT, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, call_date_print, call_date_verify, table_num_0_10, table_skip },
  	{ "���. �������         ", lcd_call_input_txt,  LCD_IN_D_T_TXT, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, call_time_print, call_time_verify, table_num_0_10, table_skip },
	{ "�������� �������� ��.", lcd_call_input_int,  LCD_IN_D_T_INT, 2, "%02d cm",  table_num_0_10, 10, 50,  &device_config.speed_open, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ "�������� �������� ��.", lcd_call_input_int,  LCD_IN_D_T_INT, 2, "%02d cm",  table_num_0_10, 10, 50,  &device_config.speed_close, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ "�������� � ���� �����", lcd_call_input_int,  LCD_IN_D_T_INT, 2, "%02d ���", table_num_0_10, 0,  60,  &device_config.delay_open, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ "��������� ����� �.���", lcd_call_input_enum, LCD_IN_D_T_ENUM, 0,0,0,0,0,0, "���� ", LOCK_POS_OPEN, "�����", LOCK_POS_CLOSE, &device_config.lock_pos },
	{ "��� � ���� ����� ����", lcd_call_input_int,  LCD_IN_D_T_INT, 1, "%1d",      table_num_1_4,  1,  4,   &device_config.reaction_fire_input, 0, 0, 0, 0, 0 },
	{ "�������� ������ �����", lcd_call_input_int,  LCD_IN_D_T_INT, 2, "%02d ���", table_num_0_10, 0,  30,  &device_config.delay_open_fire, 0, 0, 0, 0, 0 },
	{ "��� ����� ������     ", lcd_call_input_int,  LCD_IN_D_T_INT, 1, "%1d",      table_num_1_4,  1,  4,   &device_config.batt_job_n, 0, 0, 0, 0, 0 },
	{ "��� �����            ", lcd_call_input_int,  LCD_IN_D_T_INT, 1, "%1d",      table_num_1_3,  1,  3,   &device_config.lock_type_num, 0, 0, 0, 0, 0 },
	{ "����. ��.� ���� �����", lcd_call_input_enum, LCD_IN_D_T_ENUM, 0,0,0,0,0,0, " ��  ", MOTOR_STOP_YES, " ��� ", MOTOR_STOP_NO, &device_config.motor_stop },

	{ "���������. ��� ������", lcd_call_input_int,  LCD_IN_D_T_INT, 2, "%02d A",   table_num_0_10, 1,  10,  &device_config.motor_i_max, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ "���������.����.������", lcd_call_input_int,  LCD_IN_D_T_INT, 2, "%02d V",   table_num_0_10, 10, 90,  &device_config.motor_v_max, 0, 0, 0, 0, 0, 0, 0, 0, 0 },

	{ "����.����.����.����  ", lcd_call_input_int,  LCD_IN_D_T_INT, 3, "%03d ���", table_num_0_10, 0, 999,  &device_config.speed_open_winter, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ "��������.����.��.����", lcd_call_input_int,  LCD_IN_D_T_INT, 2, "%02d ���", table_num_0_10, 0, 99,   &device_config.delay_open_winter, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ "���.����.��.���������", lcd_call_input_int,  LCD_IN_D_T_INT, 3, "%03d %%",  table_num_0_10, 0, 100,  &device_config.open_width_in_winter, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
	{ "�����.�����.���������", lcd_call_input_enum, LCD_IN_D_T_ENUM, 0,0,0,0,0,0, " �� ", END_POS_INV_YES, " ��� ", END_POS_INV_NO, &device_config.end_position_inversion },
    { "����.������.�������  ", lcd_call_input_int,  LCD_IN_D_T_INT, 3, "%03d ���", table_num_0_10, 0, 999,  &device_config.max_position_error, 0, 0, 0, 0, 0, 0, 0, 0, 0 },    
};    
#pragma diag_default=Pa039
static int menu_elements = sizeof(menu_array) / sizeof(menu_t); // ���������� ��������� � ����


//------------------------------------------------------------------------------
// ����� ����
//------------------------------------------------------------------------------
void lcd_print_menu(void)
{
	char key;
	int i = 0;
	int inv = 0;

	print_str(0, 0, 0, 0, "------= ���� =------ ");

	// ����� ����
	while(i < menu_elements && i != MENU_LINES_MAX){
		if (menu_pos.select == menu_pos.pos + i) inv = 1; else inv = 0;

		print_str(0, 1 + i, inv, 0, menu_array[ menu_pos.pos + i ].txt );

		i++;
	}

	if (uxQueueMessagesWaiting(xKey) == 0) // ��� ������� �� ���������� �������
		return;
	
    xQueueReceive( xKey, &key, 0);
    xTimeMenuTimeout = xTaskGetTickCount() + MENU_TIMEOUT_SEC; // ��������� ����� ���� ������ �� ����

	switch (key){
		case KEY_UP:{
			if (menu_pos.select == 0) break; // ��������� � ����� ����� - ������ ����������, �������
			menu_pos.select--; // �������� ��������� ��������� �� 1 �����
			if (menu_pos.pos > menu_pos.select) menu_pos.pos--; // ��������� ����� �� �������, �������� ���� �����
			break;
		}

		case KEY_DOWN:{
			if (menu_pos.select == menu_elements - 1) break; // ��������� � ����� ����, �������
			menu_pos.select++;  // �������� ��������� � ���
			if (menu_pos.pos + MENU_LINES_MAX - 1 < menu_pos.select) menu_pos.pos++; // ��������� ����� �� �������, �������� ���� ����
			break;
		}

		case KEY_EXIT:{
			lcd_cls();
			lcd_cursor_off(); // �������� ������
            lcd_st = LCD_ST_DAT_DOOR;
			break;
		}

		case KEY_OK:{
			if (menu_array[ menu_pos.select ].call != NULL){
				menu_array[ menu_pos.select ].call(); // �������� ������������ ������� ������� ��������� �� ����� � ���������� ���� �����
				lcd_st = LCD_ST_INPUT;
			}
			break;
		}

		//case KEY_QUIT:{ // � �������� ������� ����� �� ����� �.�. ��� ������, ����� ��� ������ �� ��� Windows
		//	exit(0);
		//}

		default: break;
	} // switch -----------------------------------------------------


}


//------------------------------------------------------------------------------
// ���� ��������
//------------------------------------------------------------------------------
void lcd_input_val(void)
{
	char key, t;
	const char *pstr;
	size_t i;
	size_t l;
	char str[6];
	uint32_t v;
	const menu_t *m;
    int res;

	m = (menu_t*)&menu_array[ menu_pos.select ];

	if (uxQueueMessagesWaiting(xKey) == 0) // ��� ������� �� ���������� �������
		return;
	
    xQueueReceive( xKey, &key, 0);
    xTimeMenuTimeout = xTaskGetTickCount() + MENU_TIMEOUT_SEC; // ��������� ����� ���� ������ �� ����

	if (m->d_type == LCD_IN_D_T_INT) goto _int_num;
	if (m->d_type == LCD_IN_D_T_ENUM) goto _enum_num;
	if (m->d_type == LCD_IN_D_T_TXT) goto _txt;

    lcd_st = LCD_ST_MENU;
	return;

//-----------------------------------------------------------------------------
// INT
_int_num:
	for (i=0; i<strlen( m->table_char ); i++){
		if (m->table_char[ i ] == val[ input_val.x ]){ 
			input_val.y = i;
			break;
		} else input_val.y = 0;
	}

	if (flag_error) { // ���� ���� ������ � ������ ����� ������ ������� � ������ ������� � ������
		print_str(0, 5, 0, 0, "                     ");
		flag_error = 0;
	}

	switch (key){
		case KEY_UP:{
			input_val.y++;                                    // ����������� ��������� � ������� ��  1 ��������
			l = strlen( m->table_char);                       // ������ ������� ��������
			if (input_val.y == l) input_val.y = 0;            // �������� ����� ������� �������� ��������� � ������ (�� ������)

   			t = m->table_char[ input_val.y ];                 // ����� ����� ������ �� �������
			sprintf(str, "%c", t);
			print_str(lcd_get_cursor_pos_x(), lcd_get_cursor_pos_y(), 0, 0, str); // ������ ����� ������ �� �����
			val[ input_val.x ] = t;                           // ��������� ������ � ���������� �� ��������������
			break;
		}

		case KEY_DOWN:{
			input_val.y--;                                    // ����������� ��������� � ������� ��  1 ��������
			l = strlen( m->table_char);                       // ������ ������� ��������
			if (input_val.y < 0) input_val.y = l - 1;         // �������� ����� ������� �������� ��������� � ������ (�� ������)

   			t = m->table_char[ input_val.y ];                 // ����� ����� ������ �� �������
			sprintf(str, "%c", t);
			print_str(lcd_get_cursor_pos_x(), lcd_get_cursor_pos_y(), 0, 0, str); // ������ ����� ������ �� �����
			val[ input_val.x ] = t;                           // ��������� ������ � ���������� �� ��������������
			break;
		}

		case KEY_EXIT:{ // ���������, ��������� �������� � ��������� ��������

			if (m->d_type == LCD_IN_D_T_INT){
				v = (uint32_t)atoi(val);
				if (v > m->int_max || v < m->int_min){
					print_str(0, 5, 1, 1, " ������ ������.����  ");
					flag_error = 1;
					break;
				}
				*m->int_val = v;
				print_str(0, 5, 1, 1, "   ������ � ������.  ");
                // write to i2c mem
                res = dev_cfg_write( &device_config );
                if (res){
                    print_str(0, 6, 1, 1, " ������ I/O          ");
                    device_post.i2c_u4 = 1; // set error
                }
				vTaskDelay(3000);
			}

			lcd_cursor_off();
			lcd_st = LCD_ST_MENU;
			break;
		}

		case KEY_OK:{
			input_val.x++;
			lcd_set_cursor_pos_x( lcd_get_cursor_pos_x() + CHAR_W);//cursor_pos.x++;
			if (input_val.x == menu_array[ menu_pos.select ].int_size_char){
				input_val.x = 0;
				lcd_set_cursor_pos_x( input_val.cx );
			}
			
			break;
		}

		default: break;
	} // switch
    
    
	if (lcd_get_cursor_state() == CURSOR_ENABLE)
        lcd_cursor_on();
    else
        lcd_cursor_off();
      
	return;

//-----------------------------------------------------------------------------
// ENUM
_enum_num:

	switch (key){
		case KEY_UP:{
			input_val.y++;    // ������� ������������ �� 1 ��������
			input_val.y &= 1; // ����������� �� ������������ �������� 0 ��� 1

			if (input_val.y == 0) pstr = m->str1_txt;
			if (input_val.y == 1) pstr = m->str2_txt;

			print_str(lcd_get_cursor_pos_x(), lcd_get_cursor_pos_y(), 0, 1, pstr); // ������ ��������� �������� �� �����
			break;
		}

		case KEY_DOWN:{
			if (input_val.y == 0) input_val.y = 1; else input_val.y--; // ������� ������������ �� 1 ��������

			if (input_val.y == 0) pstr = m->str1_txt;
			if (input_val.y == 1) pstr = m->str2_txt;

			print_str(lcd_get_cursor_pos_x(), lcd_get_cursor_pos_y(), 0, 1, pstr); // ������ ��������� �������� �� �����
			break;
		}

		case KEY_EXIT:{ // ��������� ��������

			if (input_val.y == 0) *m->enum_val = m->str1_int;
			if (input_val.y == 1) *m->enum_val = m->str2_int;

			print_str(0, 5, 1, 1, "   ������ � ������.  ");
            res = dev_cfg_write( &device_config );
            if (res){
                print_str(0, 6, 1, 1, " ������ I/O          ");
                device_post.i2c_u4 = 1; // set error
            }
			vTaskDelay(3000);

			lcd_cursor_off();// CURSOR_DISABLE;
			lcd_st = LCD_ST_MENU;
			break;
		}

		case KEY_OK:{
			
			break;
		}

		default: break;
	} // switch
	
	return;
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// TXT
_txt:
	for (i=0; i<strlen( m->table_char_str ); i++){
		if (m->table_char_str[ i ] == val[ input_val.x ]){ 
			input_val.y = i;
			break;
		} else input_val.y = 0;
	}

	if (flag_error) { // ���� ���� ������ � ������ ����� ������ ������� � ������ ������� � ������
		print_str(0, 5, 0, 0, "                     ");
		flag_error = 0;
	}

	switch (key){
		case KEY_UP:{
			input_val.y++;                                         // ����������� ��������� � ������� ��  1 ��������
			l = strlen( m->table_char_str); // ������ ������� ��������
			if (input_val.y == l) input_val.y = 0;                 // �������� ����� ������� �������� ��������� � ������ (�� ������)

   			t = m->table_char_str[ input_val.y ]; // ����� ����� ������ �� �������
			sprintf(str, "%c", t);
			print_str(lcd_get_cursor_pos_x(), lcd_get_cursor_pos_y(), 0, 0, str); // ������ ����� ������ �� �����
			val[ input_val.x ] = t;                           // ��������� ������ � ���������� �� ��������������
			break;
		}

		case KEY_DOWN:{
			input_val.y--;                                         // ����������� ��������� � ������� ��  1 ��������
			l = strlen( m->table_char_str); // ������ ������� ��������
			if (input_val.y < 0) input_val.y = l - 1;              // �������� ����� ������� �������� ��������� � ������ (�� ������)

   			t = m->table_char_str[ input_val.y ]; // ����� ����� ������ �� �������
			sprintf(str, "%c", t);
			print_str(lcd_get_cursor_pos_x(), lcd_get_cursor_pos_y(), 0, 0, str); // ������ ����� ������ �� �����
			val[ input_val.x ] = t;                           // ��������� ������ � ���������� �� ��������������
			break;
		}

		case KEY_EXIT:{ // ���������, ��������� �������� � ��������� ��������

			if (m->d_type == LCD_IN_D_T_TXT){
				if (m->call_verify_val(val) != 0){
					print_str(0, 5, 1, 1, " ������ ������.����  ");
					flag_error = 1;
					break;
				}
				print_str(0, 5, 1, 1, "   ������ � ������.  ");
                //res = dev_cfg_write( &device_config );
                //if (res){
                //    print_str(0, 6, 1, 1, " ������ I/O          ");
                //    device_post.i2c_u4 = 1; // set error
                //}
				vTaskDelay(3000);
			}

			lcd_cursor_off();
			lcd_st = LCD_ST_MENU;
			break;
		}

		case KEY_OK:{
			next_pos:
			input_val.x++;
			lcd_set_cursor_pos_x( lcd_get_cursor_pos_x() + CHAR_W);//cursor_pos.x++;
			if (input_val.x == strlen(val)){
				input_val.x = 0;
				lcd_set_cursor_pos_x( input_val.cx );
			}

			for (i=0; i<strlen(m->table_char_skip); i++){
				if (val[input_val.x] == m->table_char_skip[i]) goto next_pos; // ���������� ������ �� ������� ������������ ��������
			}
			
			
			break;
		}

		default: break;
	} // switch
	
	return;


}

//------------------------------------------------------------------------------
// ������� ��������� �������� ���������� (TXT)
//------------------------------------------------------------------------------
void lcd_call_input_txt(void)
{
	const menu_t *m;

	m = &menu_array[ menu_pos.select ];

    lcd_cls();

	print_str(0, 0, 1, 0, m->txt);
	print_str(0, 1, 0, 0, "                     ");
	print_str(0, 2, 0, 0, "                     ");
	print_str(0, 3, 0, 0, "                     ");
	print_str(0, 4, 0, 0, "                     ");
	print_str(0, 5, 0, 0, "                     ");
	print_str(0, 6, 0, 0, "---------------------");
	print_str(0, 7, 0, 0, " +   -  ����/��� ����");

	memset(val, 0, VAL_SIZE);

    m->call_print_val(val);

	print_str(0, 4, 0, 0, "��� ����.: ");
	print_str(11 * CHAR_W, 4, 0, 0, val);

	// ������������� ������ � ������� �� ������ �������� ������
	lcd_set_cursor_pos_x( 11 * CHAR_W );
	lcd_set_cursor_pos_y( 4 );

	// ������������� � �������� ��������� ��������� �����
	input_val.x = 0;
	input_val.y = 0;
	input_val.cx = lcd_get_cursor_pos_x();
	input_val.cy = lcd_get_cursor_pos_y();
	
	lcd_cursor_on();    // �������� ������

}

//------------------------------------------------------------------------------
// ������� ��������� �������� ���������� INT
//------------------------------------------------------------------------------
void lcd_call_input_int(void)
{
	const menu_t *m;

	m = &menu_array[ menu_pos.select ];

    lcd_cls();

	print_str(0, 0, 1, 0, m->txt);
	print_str(0, 1, 0, 0, "                     ");
	print_str(0, 2, 0, 0, "                     ");
	print_str(0, 3, 0, 0, "                     ");
	print_str(0, 4, 0, 0, "                     ");
	print_str(0, 5, 0, 0, "                     ");
	print_str(0, 6, 0, 0, "---------------------");
	print_str(0, 7, 0, 0, " +   -  ����/��� ����");

	print_str(0, 1, 0, 0, "��� = ");
	sprintf(menu_str_val, m->sprintf_txt, m->int_min);
	print_str(6 * CHAR_W, 1, 0, 0, menu_str_val);

	print_str(0, 2, 0, 0, "��� = ");
	sprintf(menu_str_val, m->sprintf_txt, m->int_max);
	print_str(6 * CHAR_W, 2, 0, 0, menu_str_val);
	
	memset(val, 0, VAL_SIZE);
	print_str(0, 4, 0, 0, "��� ����.: ");
	sprintf(val, m->sprintf_txt, *m->int_val);
	print_str(11 * CHAR_W, 4, 0, 0, val);

	// ������������� ������ � ������� �� ������ �������� ������
	lcd_set_cursor_pos_x( 11 * CHAR_W );
	lcd_set_cursor_pos_y( 4 );

	// ������������� � �������� ��������� ��������� �����
	input_val.x = 0;
	input_val.y = 0;
	input_val.cx = lcd_get_cursor_pos_x();
	input_val.cy = lcd_get_cursor_pos_y();
	
	lcd_cursor_on();    // �������� ������

}

//------------------------------------------------------------------------------
// ������� ��������� �������� ����������
//------------------------------------------------------------------------------
void lcd_call_input_enum(void)
{
	const menu_t *m;

	m = &menu_array[ menu_pos.select ];

    lcd_cls();
    lcd_cursor_off();    // ��������� ������

	print_str(0, 0, 1, 0, m->txt);
	print_str(0, 1, 0, 0, "                     ");
	print_str(0, 2, 0, 0, "                     ");
	print_str(0, 3, 0, 0, "                     ");
	print_str(0, 4, 0, 0, "                     ");
	print_str(0, 5, 0, 0, "                     ");
	print_str(0, 6, 0, 0, "---------------------");
	print_str(0, 7, 0, 0, " +   -  ����/��� ����");

	print_str(0, 1, 0, 0, "�� 1= ");
	print_str(6 * CHAR_W, 1, 0, 0, m->str1_txt);

	print_str(0, 2, 0, 0, "�� 2= ");
	print_str(6 * CHAR_W, 2, 0, 0, m->str2_txt);
	
	memset(val, 0, VAL_SIZE);
	print_str(0, 4, 0, 0, "��� ���� : ");
	
	if (device_config.lock_pos == m->str1_int)
		print_str(11 * CHAR_W, 4, 0, 1, m->str1_txt);
	else
		print_str(11 * CHAR_W, 4, 0, 1, m->str2_txt);

	// ������������� ������ � ������� �� ������ �������� ������
	lcd_set_cursor_pos_x( 11 * CHAR_W );
	lcd_set_cursor_pos_y( 4 );

	// ������������� � �������� ��������� ���������� �����
	input_val.x = 0;
	input_val.y = (int)*m->enum_val; // ������� ������� �������� ������������
	input_val.cx = lcd_get_cursor_pos_x();
	input_val.cy = lcd_get_cursor_pos_y();
}

//-----------------------------------------------------------------------------
// ������������ ������(����) � ���������� str
//-----------------------------------------------------------------------------
void call_date_print(char * str)
{
    uint32_t time;
    struct tm *rtc_tm;
    
    time = time_get_sec_counter();
    rtc_tm = localtime( (time_t const*)&time );
    sprintf(str, "%02d-%02d-%02d", rtc_tm->tm_mday, rtc_tm->tm_mon, rtc_tm->tm_year-100);
}

//-----------------------------------------------------------------------------
// �������� ������ str � ������ �������� �� ����������...
//-----------------------------------------------------------------------------
int call_date_verify(char * str)
{
	int day;
	int mon;
	int year;
    struct tm *tm;
    uint32_t time;


	day = atoi( str );
	mon = atoi( str + 3 );
	year = atoi( str + 6 );

	if (day == 0) return 1;
	if (day > 31) return 1;

	if (mon == 0) return 1;
	if (mon > 12) return 1;

	if (year < 19) return 1;


	if (year % 4 == 0){
	    if (mon == 2 && day > 29) return 1;
	} else {
	    if (mon == 2 && day > 28) return 1;
	}

    time = time_get_sec_counter();
    tm = localtime( (time_t const*)&time );
    
    tm->tm_sec  = 0;
    tm->tm_mday = day;
    tm->tm_mon  = mon;
    tm->tm_year = 2000 + year - 1900;
    
    time = mktime( tm );
    time_set_sec_counter( time );
    
    if (rtc_set_time32( time )) printf_d("ERROR[%s %d]: rtc_set_time32\n", __FUNCTION__, __LINE__);
    
	return 0;
}

//-----------------------------------------------------------------------------
// ������������ ������(�������) � ���������� str
//-----------------------------------------------------------------------------
void call_time_print(char * str)
{
    uint32_t time;
    struct tm *rtc_tm;
    
    time = time_get_sec_counter();
    rtc_tm = localtime( (time_t const*)&time );
    sprintf(str, "%02d:%02d", rtc_tm->tm_hour, rtc_tm->tm_min);
}

//-----------------------------------------------------------------------------
// �������� ������ str � ������ �������� �� ����������...
//-----------------------------------------------------------------------------
int call_time_verify(char * str)
{
	int min;
	int hour;
    struct tm *tm;
    uint32_t time;

	hour = atoi( str );
	min = atoi( str + 3 );

	if (hour > 23) return 1;
	if (min > 59) return 1;

    time = time_get_sec_counter();
    tm = localtime( (time_t const*)&time );
    
    tm->tm_sec  = 0;
    tm->tm_hour = hour;
    tm->tm_min  = min;
    
    time = mktime( tm );
    time_set_sec_counter( time );
    
    if (rtc_set_time32( time )) printf_d("ERROR[%s %d]: rtc_set_time32\n", __FUNCTION__, __LINE__);
    
	return 0;
}
