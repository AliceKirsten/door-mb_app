#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f7xx_hal.h"
#include "stm32f745xx.h"
    
   
#define U20_I2C_ADR   (0xa0) // 8 bit (MAC ADR) �� ������� ��.���� �� ��������  
#define U4_I2C_ADR    (0xa2) // 8 bit Flash CFG + LOG
#define U3_I2C_RTC    (0xd0) // 8 bit RTC M41T62LC6

// ����� ������ I2C (U4)---------------------------
// LOG - ���������� ������� �� ������� flash ������
//
// ���.           F L A S H - I2C
// 0000 -------------------------------------------
#define LAT1_ADR_START     (0)
//      LAT 1
// 0005 -------------------------------------------
//      LAT 2
// 0010 -------------------------------------------
#define DEVICE_CONFIG_ADR  (10)
#define DEVICE_CONFIG_SIZE (255-2) // 255 �������� �� 1 ������, -2 �.�. � ������ ���� ����� 2 �����, ��� ������ > 255 ����� ��������� �� ����� !!! � ������������ ��. ��������
//
//       Device config
//
// ��������� ������� ------------------------------
#define FREE_START         (DEVICE_CONFIG_ADR + DEVICE_CONFIG_SIZE)
#define FREE_SIZE          (0)
// ???????????????????????????
//
// 1192 -------------------------------------------
//         LOG - ������ = 1400 ��
#define LOG_ARRAY_ADR      (1192)        // ����� ������ ������� �������� LOG �������
#define LOG_RECORD_N       (1400)        // ���������� �������
#define LOG_ARRAY_SIZE     (LOG_RECORD_N * sizeof(log_record_t))    // ������ �������
//
// 8192 ����� -------------------------------------

// CPU FLASH -------------------------------------------------------------------
#define Sector0_adr  (0x08000000)
#define Sector0_size (32*1024)
    
#define Sector1_adr  (0x08008000)
#define Sector1_size (32*1024)
    
#define Sector2_adr  (0x08010000)
#define Sector2_size (32*1024)
    
#define Sector3_adr  (0x08018000)
#define Sector3_size (32*1024)
    
#define Sector4_adr  (0x08020000)
#define Sector4_size (128*1024)
    
#define Sector5_adr  (0x08040000)
#define Sector5_size (256*1024)
    
#define Sector6_adr  (0x08080000)
#define Sector6_size (256*1024)
    
#define Sector7_adr  (0x080C0000)
#define Sector7_size (256*1024)
    
    
// ����� ������ CPU (U)---------------------------------------------------------
// Boot loader
//#define FLASH_BOOT_ADR   (Sector0_adr)
//#define FLASH_BOOT_SIZE  (128 * 1024)

// ���������� ��
//#define FLASH_APP_ADR   (Sector4_adr)
//#define FLASH_APP_SIZE  ((128 + 256) * 1024)

// FW COPY - ������� �������� �� ��� ������������
//#define FLASH_FW_ADR   (Sector6_adr)
//#define FLASH_FW_SIZE  (512 * 1024)

// 1 ��� ����� CPU (U)---------------------------
    
    


/**
  * @brief  Get the FLASH Latency.
  * @retval FLASH Latency
  *          The value of this parameter depend on device used within the same series
  */
#define __HAL_FLASH_GET_LATENCY()     (READ_BIT((FLASH->ACR), FLASH_ACR_LATENCY))

    

#define QUEUE_CONSOL_RX_SIZE	(128)// ������ �������� ������ �������

#define TASK_LCDREF_PRIORITY   ( tskIDLE_PRIORITY + 1 )
#define TASK_HARDPOST_PRIORITY ( tskIDLE_PRIORITY + 2 )
#define TASK_INPSENS_PRIORITY  ( tskIDLE_PRIORITY + 1 )
#define TASK_ROUTER_PRIORITY   ( tskIDLE_PRIORITY + 2 )
#define TASK_CONSOLE_PRIORITY  ( tskIDLE_PRIORITY + 1 )
#define TASK_DRV_PRIORITY      ( tskIDLE_PRIORITY + 2 )
#define TASK_LOG_PRIORITY      ( tskIDLE_PRIORITY + 1 )
#define TASK_KEYBOARD_PRIORITY ( tskIDLE_PRIORITY + 1 )

void Error_Handler(void);

// USART1 Debug console
#define USART1_PORT (GPIOA)
#define USART1_RX (1 << 10)// bit
#define USART1_TX (1 << 9) // bit

// USART2 - PC
#define USART2_PORT_RX (GPIOA) 
#define USART2_RX      (1 << 3)
#define USART2_PORT_TX (GPIOD)
#define USART2_TX      (1 << 5)

// USART3 - DRV
#define USART3_PORT_RX (GPIOD) 
#define USART3_RX      (1 << 9)
#define USART3_PORT_TX (GPIOB)
#define USART3_TX      (1 << 10)


// SPI1 - OLED
#define OLED_CS_PORT  GPIOA
#define OLED_CLK_PORT GPIOA

#define OLED_RES_PORT  GPIOB
#define OLED_MOSI_PORT GPIOB

#define OLED_PWR_ON_PORT GPIOG
#define OLED_DH_CL_PORT  GPIOG

#define OLED_CS_L   (1 << 4)  // PA4
#define OLED_CLK    (1 << 5)  // PA5
#define OLED_RES_L  (1 << 3)  // PB3 
#define OLED_MOSI   (1 << 5)  // PB5
#define OLED_PWR_ON_L (1 << 14) // PG14
#define OLED_DH_CL  (1 << 13) // PG13

#define I2C_SDA_PORT GPIOB
#define I2C_SCL_PORT GPIOB
#define I2C_SDA     (1 << 7) // PB7 I2C_1
#define I2C_SCL     (1 << 6) // PB6




/* Private defines -----------------------------------------------------------*/
//#define SCK_RF_Pin GPIO_PIN_2
//#define SCK_RF_GPIO_Port GPIOE
//#define RF_IRQ_Pin GPIO_PIN_3
//#define RF_IRQ_GPIO_Port GPIOE
//#define CS_RF_Pin GPIO_PIN_4
//#define CS_RF_GPIO_Port GPIOE
//#define MISO_RF_Pin GPIO_PIN_5
//#define MISO_RF_GPIO_Port GPIOE
//#define MOSI_RF_Pin GPIO_PIN_6
//#define MOSI_RF_GPIO_Port GPIOE
//#define RF_TST_Pin GPIO_PIN_13
//#define RF_TST_GPIO_Port GPIOC

#define RDR2_L           (1 << 14)
#define RDR2_L_PORT      GPIOC
#define RDR1_L           (1 << 15)
#define RDR1_L_PORT      GPIOC
#define PDU_CLOSE_L      (1 << 0)
#define PDU_CLOSE_L_PORT GPIOF
#define PDU_AUTO_L       (1 << 1)
#define PDU_AUTO_L_PORT  GPIOF
#define PDU_EXIT_L       (1 << 2)
#define PDU_EXIT_L_PORT  GPIOF
#define PDU_WINTER_L     (1 << 3)
#define PDU_WINTER_L_PORT GPIOF
#define PDU_OPEN_L       (1 << 4)
#define PDU_OPEN_L_PORT  GPIOF
#define PDU_STOP_L       (1 << 5)
#define PDU_STOP_L_PORT  GPIOF
#define PDU_TEST_L       (1 << 8)
#define PDU_TEST_L_PORT  GPIOF
#define FIRE_L           (1 << 10)
#define FIRE_L_PORT      GPIOF
#define BAR1_L           (1 << 8)
#define BAR1_L_PORT      GPIOB
#define BAR2_L           (1 << 1)
#define BAR2_L_PORT      GPIOE

#define PG_VOUT_H        (1 << 1)
#define PG_VOUT_H_PORT   GPIOB
#define V20_ENA_H        (1 << 2)
#define V20_ENA_H_PORT   GPIOB


#define DR_NRST_PORT     GPIOD
#define DR_NRST          (1<<8)

/*
#define BUZZ_H_Pin GPIO_PIN_9
#define BUZZ_H_GPIO_Port GPIOF
#define DO_MEM_Pin GPIO_PIN_2
#define DO_MEM_GPIO_Port GPIOC
#define DI_MEM_Pin GPIO_PIN_3
#define DI_MEM_GPIO_Port GPIOC
#define OLED_CS_LOW_Pin GPIO_PIN_4
#define OLED_CS_LOW_GPIO_Port GPIOA
#define I_LOCK_Pin GPIO_PIN_0
#define I_LOCK_GPIO_Port GPIOB
#define ENA_DRV_H_Pin GPIO_PIN_11
#define ENA_DRV_H_GPIO_Port GPIOF
#define DR_RX_UART3_Pin GPIO_PIN_10
#define DR_RX_UART3_GPIO_Port GPIOB
#define DR_NRST_Pin GPIO_PIN_8
#define DR_NRST_GPIO_Port GPIOD
#define DR_TX_UART3_Pin GPIO_PIN_9
#define DR_TX_UART3_GPIO_Port GPIOD
#define SCK_MEM_Pin GPIO_PIN_3
#define SCK_MEM_GPIO_Port GPIOD
#define OLED_DC_Pin GPIO_PIN_13
#define OLED_DC_GPIO_Port GPIOG
#define OH_ENA_Pin GPIO_PIN_14
#define OH_ENA_GPIO_Port GPIOG
#define RES_OLED_LOW_Pin GPIO_PIN_3
#define RES_OLED_LOW_GPIO_Port GPIOB
#define CS_MEM_Pin GPIO_PIN_4
#define CS_MEM_GPIO_Port GPIOB
#define TIM_IRQ_L_Pin GPIO_PIN_0
#define TIM_IRQ_L_GPIO_Port GPIOE
*/

// ����������
#define KEY_B0_L         (1 << 9)
#define KEY_B0_L_PORT    GPIOG
#define KEY_B1_L         (1 << 10)
#define KEY_B1_L_PORT    GPIOG
#define KEY_B2_L         (1 << 11)
#define KEY_B2_L_PORT    GPIOG
#define KEY_B3_L         (1 << 12)
#define KEY_B3_L_PORT    GPIOG

// ����� -----------------------------------------------------------------------
// ������ ���
#define LOCK_INA_L       (1 << 10)
#define LOCK_INA_L_PORT  GPIOE
#define LOCK_INB_L       (1 << 11)
#define LOCK_INB_L_PORT  GPIOE

// ���������� ��������
#define ENA_DRV_H        (1<<11)
#define ENA_DRV_H_PORT   GPIOF

// ���������� ����������� �����
#define ILOCK_H       (1 << 0)
#define ILOCK_H_PORT  GPIOB
//------------------------------------------------------------------------------

// U20 �������� ������� � ���� ����������������
#define I2C_MAC_PWR_H        (1 << 9)
#define I2C_MAC_PWR_H_PORT   GPIOB

// �������
#define BUZZ_H        (1 << 9) 
#define BUZZ_H_PORT   (GPIOF)

#ifdef __cplusplus
}
#endif



#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
