#ifndef _PRINTF_HAL_H_
#define _PRINTF_HAL_H_

#include "xprintf.h"

#define printf_d   xprintf
#define printf_dos xprintf_os
#define printf     xprintf
#define sprintf    xsprintf
#define snprintf   xsnprintf

int get_os_status( void );
void ConPrintSemTake( void );
void ConPrintSemGive( void );
void PRINT_FATAL_ERROR( const char *ch );

#endif