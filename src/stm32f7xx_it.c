#include "main.h"
#include "stm32f7xx_it.h"
#include "os.h"

#include "sys_timer.h"      

#include "queue_buf.h"
#include "lcd.h"
#include "dma_745.h"
#include "gpio_745.h"
#include "sys_timer.h"
#include "msg_types.h"
#include "printf_hal.h"
      
extern void consol_tx_dma( void );
extern uint8_t flag_consol_tx;
extern queue_buf_t q_consol_tx;
extern QUEUE_VAR_TYPE consol_tx_size;
extern queue_buf_t q_consol_rx;
extern TaskHandle_t xTaskConsoleHandle;

extern queue_buf_t q_from_pc;
extern queue_buf_t q_to_pc;
extern QUEUE_VAR_TYPE uart_pc_tx_size;
extern uint8_t uart_pc_flag_tx;
extern void uart_pc_tx_dma( void );

extern queue_buf_t q_from_drv;
extern queue_buf_t q_to_drv;
extern QUEUE_VAR_TYPE uart_drv_tx_size;
extern uint8_t uart_drv_flag_tx;
extern void uart_drv_tx_dma( void );
extern lcd_j_t lcd_j;
extern uint32_t iwdg_flag;

uint8_t pc_dma_buf_rx[ PACKET_SIZE ];
uint8_t drv_dma_buf_rx[ PACKET_SIZE ];

/******************************************************************************/
/*           Cortex-M7 Processor Interruption and Exception Handlers          */ 
/******************************************************************************/
/**
  * @brief This function handles Non maskable interrupt.
  */
void NMI_Handler(void)
{
  while (1){
  }
}

/**
  * @brief This function handles Hard fault interrupt.
  */
void HardFault_Handler(void)
{
    while (1){
    }
}

/**
  * @brief This function handles Memory management fault.
  */
void MemManage_Handler(void)
{
  while (1){
  }
}

/**
  * @brief This function handles Pre-fetch fault, memory access fault.
  */
void BusFault_Handler(void)
{
  while (1){
  }
}

/**
  * @brief This function handles Undefined instruction or illegal state.
  */
void UsageFault_Handler(void)
{
  while (1){
  }
}

/**
  * @brief This function handles Debug monitor.
  */
void DebugMon_Handler(void)
{
}

/******************************************************************************/
/* STM32F7xx Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_stm32f7xx.s).                    */
/******************************************************************************/

//------------------------------------------------------------------------------
// DMA - USART3 �������� - � ������� DRV
//------------------------------------------------------------------------------
void DMA1_Stream3_IRQHandler(void)
{
    if (DMA1->LISR & DMA_LISR_TCIF3){
        DMA1->LIFCR |= DMA_LISR_TCIF3 | DMA_LISR_HTIF3 | DMA_LISR_TEIF3 | DMA_LISR_DMEIF3 | DMA_LISR_FEIF3;
        pop_data_dma_queue( &q_to_drv, uart_drv_tx_size);
        uart_drv_flag_tx = 0;
    }
}

//------------------------------------------------------------------------------
// USART3 ����� ����� �� DRV
//------------------------------------------------------------------------------
void USART3_IRQHandler(void)
{
    QUEUE_VAR_TYPE dl;
    uint32_t indx, dlen;

    if (USART3->ISR & USART_ISR_FE){
        USART3->ICR = USART_ICR_FECF;
    }
    
    if (USART3->ISR & USART_ISR_ORE){
        USART3->ICR = USART_ICR_ORECF;
    }
    
    if (USART3->ISR & USART_ISR_RTOF){
        USART3->ICR = USART_ICR_RTOCF;
        
        dma_off(DMA1_Stream1);
        indx = DMA1_Stream1->NDTR;
        
        if ((indx & (PACKET_SIZE/2-1)) == 0){
            dma_on(DMA1_Stream1);
            return;
        }
        
        if (indx > PACKET_SIZE/2){
            dlen = PACKET_SIZE - indx;
            dl = get_free_size_queue( &q_from_drv );
	        if ( dl >=  dlen + 1 ){
                q_from_drv.w_buf = drv_dma_buf_rx;
                q_from_drv.w_len = dlen;
                push_data_queue( &q_from_drv );
            }
        } else {
            dlen = PACKET_SIZE / 2 - indx;
            dl = get_free_size_queue( &q_from_drv );
	        if ( dl >=  dlen + 1 ){
                q_from_drv.w_buf = drv_dma_buf_rx + PACKET_SIZE / 2;
                q_from_drv.w_len = dlen;
                push_data_queue( &q_from_drv );
            }
        }
        dma_set_size(DMA1_Stream1, PACKET_SIZE);
        dma_on(DMA1_Stream1);
    }

}

//------------------------------------------------------------------------------
// DMA - USART3 ����� - �� DRV
//------------------------------------------------------------------------------
void DMA1_Stream1_IRQHandler(void)
{
    QUEUE_VAR_TYPE dl;

    if (DMA1->LISR & DMA_LISR_HTIF1){
        DMA1->LIFCR |= DMA_LISR_HTIF1 | DMA_LISR_TEIF1 | DMA_LISR_DMEIF1 | DMA_LISR_FEIF1;
        dl = get_free_size_queue( &q_from_drv );
	    if ( dl >=  PACKET_SIZE/2 ){
            q_from_drv.w_buf = drv_dma_buf_rx;
            q_from_drv.w_len = PACKET_SIZE / 2;
            push_data_queue( &q_from_drv );
        }
    } // if ---------------------------------------------
        
    if (DMA1->LISR & DMA_LISR_TCIF1){
        DMA1->LIFCR |= DMA_LISR_TCIF1 | DMA_LISR_TEIF1 | DMA_LISR_DMEIF1 | DMA_LISR_FEIF1;
        dl = get_free_size_queue( &q_from_drv );
	    if ( dl >=  PACKET_SIZE / 2 ){
            q_from_drv.w_buf = pc_dma_buf_rx + PACKET_SIZE / 2;
            q_from_drv.w_len = PACKET_SIZE / 2;
            push_data_queue( &q_from_drv );
        }
    } // if ---------------------------------------------
    
}


//------------------------------------------------------------------------------
// DMA - USART2 ����� - �� ��
//------------------------------------------------------------------------------
void DMA1_Stream5_IRQHandler(void)
{
    QUEUE_VAR_TYPE dl;

    if (DMA1->HISR & DMA_HISR_HTIF5){
        DMA1->HIFCR |= DMA_HISR_HTIF5 | DMA_HISR_TEIF5 | DMA_HISR_DMEIF5 | DMA_HISR_FEIF5;
        dl = get_free_size_queue( &q_from_pc );
	    if ( dl >=  PACKET_SIZE/2 ){
            q_from_pc.w_buf = pc_dma_buf_rx;
            q_from_pc.w_len = PACKET_SIZE / 2;
            push_data_queue( &q_from_pc );
        }
    } // if ---------------------------------------------
        
    if (DMA1->HISR & DMA_HISR_TCIF5){
        DMA1->HIFCR |= DMA_HISR_TCIF5 | DMA_HISR_TEIF5 | DMA_HISR_DMEIF5 | DMA_HISR_FEIF5;
        dl = get_free_size_queue( &q_from_pc );
	    if ( dl >=  PACKET_SIZE / 2 ){
            q_from_pc.w_buf = pc_dma_buf_rx + PACKET_SIZE / 2;
            q_from_pc.w_len = PACKET_SIZE / 2;
            push_data_queue( &q_from_pc );
        }
    } // if ---------------------------------------------
}

//------------------------------------------------------------------------------
// DMA - USART2 �������� - � ������� ��
//------------------------------------------------------------------------------
void DMA1_Stream6_IRQHandler(void)
{
    if (DMA1->HISR & DMA_HISR_TCIF6){
        DMA1->HIFCR |= DMA_HISR_TCIF6 | DMA_HISR_HTIF6 | DMA_HISR_TEIF6 | DMA_HISR_DMEIF6 | DMA_HISR_FEIF6;
        pop_data_dma_queue( &q_to_pc, uart_pc_tx_size);
        uart_pc_flag_tx = 0;
    }
}

//------------------------------------------------------------------------------
// USART2 ����� ����� �� ��
//------------------------------------------------------------------------------
void USART2_IRQHandler(void)
{
    QUEUE_VAR_TYPE dl;
    uint32_t indx, dlen;

    if (USART2->ISR & USART_ISR_FE){
        USART2->ICR = USART_ICR_FECF;
    }
    
    if (USART2->ISR & USART_ISR_ORE){
        USART2->ICR = USART_ICR_ORECF;
    }
    
    if (USART2->ISR & USART_ISR_RTOF){
        USART2->ICR = USART_ICR_RTOCF;
        
        dma_off(DMA1_Stream5);
        indx = DMA1_Stream5->NDTR;
        
        if ((indx & (PACKET_SIZE/2-1)) == 0){
            dma_on(DMA1_Stream5);
            return;
        }
        
        if (indx > PACKET_SIZE/2){
            dlen = PACKET_SIZE - indx;
            dl = get_free_size_queue( &q_from_pc );
	        if ( dl >=  dlen + 1 ){
                q_from_pc.w_buf = pc_dma_buf_rx;
                q_from_pc.w_len = dlen;
                push_data_queue( &q_from_pc );
            }
        } else {
            dlen = PACKET_SIZE / 2 - indx;
            dl = get_free_size_queue( &q_from_pc );
	        if ( dl >=  dlen + 1 ){
                q_from_pc.w_buf = pc_dma_buf_rx + PACKET_SIZE / 2;
                q_from_pc.w_len = dlen;
                push_data_queue( &q_from_pc );
            }
        }
        dma_set_size(DMA1_Stream5, PACKET_SIZE);
        dma_on(DMA1_Stream5);
    }
}

/**
  * @brief This function handles TIM2 global interrupt.
  */
void TIM2_IRQHandler(void)
{
}

/**
  * @brief This function handles SPI1 global interrupt.
  */
void SPI1_IRQHandler(void)
{
}

//------------------------------------------------------------------------------
// USART1 ����� ����� �� �������
//------------------------------------------------------------------------------
void USART1_IRQHandler(void)
{
    QUEUE_VAR_TYPE dl;
    uint8_t c;
    
    if (USART1->ISR & USART_ISR_RXNE){
        c = USART1->RDR;
        dl = get_free_size_queue( &q_consol_rx );
	    if ( dl >=  2 ){
            push_data_queue_b( &q_consol_rx, c);
            if (get_os_status()) xTaskResumeFromISR(xTaskConsoleHandle);
        }
    }
    
    if (USART1->ISR & USART_ISR_FE){
        USART1->ICR = USART_ICR_FECF;
    }
}


//------------------------------------------------------------------------------
// DMA - OLED ��������
//------------------------------------------------------------------------------
void DMA2_Stream3_IRQHandler(void)
{
    
    if (DMA2->LISR & DMA_LISR_TCIF3){
        DMA2->LIFCR |= DMA_LISR_TCIF3 | DMA_LISR_HTIF3 | DMA_LISR_TEIF3 | DMA_LISR_DMEIF3 | DMA_LISR_FEIF3;

        switch ( lcd_j.lcd_st ){
        case LCD_INIT:
            lcd_j.lcd_tx = LCD_READY;
            break;
        case LCD_END:
            gpio_set_1(OLED_CS_PORT, OLED_CS_L);
            lcd_j.lcd_tx = LCD_READY;
            break;
        default:
            break;
        }

    }

}

//------------------------------------------------------------------------------
// DMA - ������� ��������
//------------------------------------------------------------------------------
void DMA2_Stream7_IRQHandler(void)
{

    if (DMA2->HISR & DMA_HISR_TCIF7){
        DMA2->HIFCR |= DMA_HISR_TCIF7 | DMA_HISR_HTIF7 | DMA_HISR_TEIF7 | DMA_HISR_DMEIF7 | DMA_HISR_FEIF7;
        pop_data_dma_queue( &q_consol_tx, consol_tx_size);
        flag_consol_tx = 0;
        if ( get_data_size_queue( &q_consol_tx ) > 0 ){
            consol_tx_dma();
        }
    }

}

//------------------------------------------------------------------------------
// ��� ��������� ������
// T period = 1 ms
//------------------------------------------------------------------------------
void TIM7_IRQHandler(void)
{
    TIM7->SR = 0;
    sys_tick_handler();
}
