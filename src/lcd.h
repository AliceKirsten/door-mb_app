#ifndef LCD_H_
#define LCD_H_

#define LCD_RES_X (128)
#define LCD_RES_Y (64)
#define LCD_PAGES (8)

#define FONT_W    (5) // ������ ������� �� ������� �������
#define CHAR_W    (6) // ������ ����������

// �������� �� �������� ������
typedef enum {
	CURSOR_DISABLE = 0,
	CURSOR_ENABLE
} cursor_visible_t;

// ��������� ������� �� ������
typedef struct {
    char x;
	char y;
	cursor_visible_t cursor;
} cursor_position_t;

typedef enum { LCD_INIT = 0, /*LCD_REFRESH,*/ LCD_END } lcd_state_t;
typedef enum { LCD_READY = 0, LCD_BUSY } lcd_tx_t;

typedef struct {
    lcd_state_t lcd_st; // ������� ��������� - ���� � ��� �������� ������
    lcd_tx_t lcd_tx;    // ������� ��������� ��������.
} lcd_j_t;


void lcd_init(void);
void lcd_refresh(void);
void lcd_cls(void);
void print_str( uint8_t pos_x, uint8_t pos_y, uint8_t inv, uint8_t blink, const char *str);
void lcd_cursor_on(void);
void lcd_cursor_off(void);
void lcd_set_cursor_pos_x(uint8_t x);
void lcd_set_cursor_pos_y(uint8_t y);
uint8_t lcd_get_cursor_pos_x(void);
uint8_t lcd_get_cursor_pos_y(void);
cursor_visible_t lcd_get_cursor_state(void);

#endif