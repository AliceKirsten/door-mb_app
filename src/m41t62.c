/*
 * (C) Copyright 2008
 * Stefan Roese, DENX Software Engineering, sr@denx.de.
 *
 * based on a the Linux rtc-m41t80.c driver which is:
 *   Alexander Bigga <ab@mycable.de>, 2006 (c) mycable GmbH
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */
/*
 * Date & Time support for STMicroelectronics M41T62
 * Modify Sviridov Georgy sgot@inbox.ru
 */
/* #define	DEBUG	*/

#include "m41t62.h"
#include "main.h"
#include "printf_hal.h"
#include "i2c_745.h"
#include <time.h>

#ifndef DEBUG_M41T62
#define DEBUG_M41T62 0
#endif

unsigned char bcd2bin (unsigned char b)
{
    return ((b & 0xf0) >> 4) * 10 + (b & 0x0f);
}

unsigned char bin2bcd(unsigned char x){
    return ((x%10) | ((x/10)<<4));
}


//------------------------------------------------------------------------------
// ���������� ���� + ����� �� RTC
//------------------------------------------------------------------------------
int m41t62_get(struct tm *tm)
{
	uint8_t buf[M41T62_DATETIME_REG_SIZE];
    int res;
    
    res = i2c_read_adr_8bit(U3_I2C_RTC, M41T62_REG_SSEC, buf, M41T62_DATETIME_REG_SIZE);
    if (res) {
        printf_d("ERROR[%s %d]: i2c_read_adr_8bit(U3_I2C_RTC\n", __FUNCTION__, __LINE__);
        return res;
    }
    
	if (DEBUG_M41T62) printf_d("RTC raw read data - sec=%02x, min=%02x, hr=%02x, wday=%02x, mday=%02x, mon=%02x, year=%02x\n",
	                                buf[1], buf[2], buf[3], buf[4], buf[5], buf[6], buf[7]);

	tm->tm_sec = bcd2bin(buf[M41T62_REG_SEC] & 0x7f);
	tm->tm_min = bcd2bin(buf[M41T62_REG_MIN] & 0x7f);
	tm->tm_hour = bcd2bin(buf[M41T62_REG_HOUR] & 0x3f);
	tm->tm_mday = bcd2bin(buf[M41T62_REG_DAY] & 0x3f);
	tm->tm_wday = buf[M41T62_REG_WDAY] & 0x07;
	tm->tm_mon = bcd2bin(buf[M41T62_REG_MON] & 0x1f);
	/* assume 20YY not 19YY, and ignore the Century Bit */
	/* U-Boot needs to add 1900 here */
	tm->tm_year = bcd2bin(buf[M41T62_REG_YEAR]) + 2000;
    tm->tm_isdst = -1;
    tm->tm_yday = 0;
    
	if (DEBUG_M41T62) printf_d("%s: tm is secs=%d, mins=%d, hours=%d, mday=%d, mon=%d, year=%d, wday=%d\n",
	      __FUNCTION__,
	      tm->tm_sec, tm->tm_min, tm->tm_hour, tm->tm_mday, tm->tm_mon, tm->tm_year, tm->tm_wday);
	return 0;
}

//------------------------------------------------------------------------------
// ������ ���� � ������� � RTC
//------------------------------------------------------------------------------
int m41t62_set(struct tm *tm)
{
	uint8_t buf_w[M41T62_DATETIME_REG_SIZE + 1]; // + 1 for i2c mem-adr
	uint8_t buf_r[M41T62_DATETIME_REG_SIZE];
    int res;
    
	if (DEBUG_M41T62) printf_d("\nSet DATE: %4d-%02d-%02d (wday=%d)  TIME: %2d:%02d:%02d\n",
	      tm->tm_year, tm->tm_mon, tm->tm_mday, tm->tm_wday, tm->tm_hour, tm->tm_min, tm->tm_sec);

    res = i2c_read_adr_8bit(U3_I2C_RTC, M41T62_REG_SSEC, buf_r, M41T62_DATETIME_REG_SIZE);
    if (res) {
        printf_d("ERROR[%s %d]: i2c_read_adr_8bit(U3_I2C_RTC\n", __FUNCTION__, __LINE__);
        return res;
    }

	/* Merge time-data and register flags into buf[0..7] */
    buf_w[0] = 0;// start adr for i2c write (my realisation)
	buf_w[M41T62_REG_SSEC+1] = 0;
	buf_w[M41T62_REG_SEC+1]  = bin2bcd(tm->tm_sec) | (buf_r[M41T62_REG_SEC] & ~0x7f);
	buf_w[M41T62_REG_MIN+1]  = bin2bcd(tm->tm_min) | (buf_r[M41T62_REG_MIN] & ~0x7f);
	buf_w[M41T62_REG_HOUR+1] = bin2bcd(tm->tm_hour) | (buf_r[M41T62_REG_HOUR] & ~0x3f) ;
	buf_w[M41T62_REG_WDAY+1] = (tm->tm_wday & 0x07) | (buf_r[M41T62_REG_WDAY] & ~0x07);
	buf_w[M41T62_REG_DAY+1]  = bin2bcd(tm->tm_mday) | (buf_r[M41T62_REG_DAY] & ~0x3f);
	buf_w[M41T62_REG_MON+1]  = bin2bcd(tm->tm_mon) | (buf_r[M41T62_REG_MON] & ~0x1f);
	/* assume 20YY not 19YY */
	buf_w[M41T62_REG_YEAR+1] = bin2bcd(tm->tm_year % 100);

    
	if (DEBUG_M41T62) printf_d("RTC raw WR data - sec=%02x, min=%02x, hr=%02x, wday=%02x, mday=%02x, mon=%02x, year=%02x\n",
	                                buf_w[2], buf_w[3], buf_w[4], buf_w[5], buf_w[6], buf_w[7], buf_w[8]);
    
    
    res = i2c_write( U3_I2C_RTC, buf_w, M41T62_DATETIME_REG_SIZE + 1);
    if (res) {
        printf_d("ERROR[%s %d]: i2c_read_adr_8bit(U3_I2C_RTC\n", __FUNCTION__, __LINE__);
        return res;
    }
    
	return 0;
}

//------------------------------------------------------------------------------
// ������� ����� ��� ��������� �������
//------------------------------------------------------------------------------
int m41t62_reset(void)
{
	uint8_t buf[ 2 ];
    int res;
    
    // Set SP = 1 --------------------------------------------------------------
    buf[ 0 ] = M41T62_REG_SEC;
    buf[ 1 ] = M41T62_SEC_ST;
    
    res = i2c_write( U3_I2C_RTC, buf, 2);
    if (res) {
        printf_d("ERROR: i2c_read_adr_8bit(U3_I2C_RTC\n");
        return res;
    }

    // Set SP = 0 --------------------------------------------------------------
    buf[ 0 ] = M41T62_REG_SEC;
    buf[ 1 ] = 0;
    
    res = i2c_write( U3_I2C_RTC, buf, 2);
    if (res) {
        printf_d("ERROR: i2c_read_adr_8bit(U3_I2C_RTC\n");
        return res;
    }
    
    if (DEBUG_M41T62) printf_d("M41T62 Stop bit 1->0\n");
    return 0;
}
