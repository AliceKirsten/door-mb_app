#include <stdint.h>

// for Little - endian
const char FIRMWARE_HEAD_NAME_C[4]={'F', 'I', 'R', 'M'};
const uint32_t FIRMWARE_HEAD_NAME_B = ('F'<<0) | ('I'<<8) | ('R'<<16) | ('M'<<24);

