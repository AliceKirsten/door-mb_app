#ifndef PACKET_CHECK_OS_H_
#define PACKET_CHECK_OS_H_

#include "msg_types.h"
#include "queue_buf.h"
#include "get_packet_from_q.h"


void get_packet_from_osq(packet_build_t *p, uint32_t time);

#endif
