// ������ ��������� �������. ��� �������

#include "os.h"
#include "queue_buf.h"
#include "console.h"
#include "printf_hal.h"

extern queue_buf_t q_consol_rx;
extern void tx_byte(uint8_t ch);

console_var_type cn;

void vTaskConsole(void const * argument)
{
    uint8_t c;
    
    printf_dos("Task Console START.\n");

    cn.putc = tx_byte;
    console_init( &cn );

WHILE_BEGIN

    if ( get_data_size_queue( &q_consol_rx ) != 0 ){
        pop_data_queue_b( &q_consol_rx , &c );
        console( &cn, c);
    }

    vTaskSuspend(NULL);

WHILE_END
}
