#include "os.h"
#include "main.h"
#include "printf_hal.h"
#include "hardware.h"
#include "lcd.h"

#include "task_inputsensors.h"
#include "task_console.h"
#include "task_router.h"
#include "task_drv.h"
#include "task_log.h"
#include "task_keyboard.h"
#include <string.h>
#include "video.h"
#include "version.h"
#include "msg_types.h"
#include "fw_types.h"
#include "firmware_header_v2_0.h"
#include "fw_send_to_drv.h"

#ifndef DEBUG_TH
#define DEBUG_TH 0
#endif

extern xQueueHandle xLog;
extern uint8_t lcd_buf_p[ ];
extern uint8_t lcd_buf_n[ ];
extern uint8_t char_buf[ ];// ����� ��� sprintf - ����� ������ !!!
extern xQueueHandle xPakRouterToTPost;            // ������� ������ Router -> TASK Post
extern xQueueHandle xPakTPostToRouter;            // ������� ������ TASK Post -> Router

// �� DRV � ����������
extern const char fw_drv_app[];
extern const int fw_drv_app_length;

result_t drv_fw_check(void);// �������� ������ �� ����� DRV � ���������� �� ���� �����

TaskHandle_t xTaskConsoleHandle;

void vTaskHardPost(void const * argument)
{
    result_t res;
    portBASE_TYPE xReturn;
    msg_t msg;

    driver_pcb_reset_init(); // ������������� ����� DR_NRST (����� ����� ��������)
    
    // ������� ��������
    memcpy(lcd_buf_p, video_intro, 128*8);
    memcpy(lcd_buf_n, video_intro, 128*8);
    vTaskDelay(3000);   
    
    sprintf((char*)char_buf, "������: %d.%d.%d", SOFT_VER_H, SOFT_VER_L, SOFT_VER_Y);
    print_str( 0, 0, 0, 0, (const char*)char_buf);
    print_str( 27, 1, 0, 0, "�����������");

    
    if (DEBUG_TH) printf_dos("vTaskHardPost - Start.\n");
    buzz_off();
    
    driver_pcb_reset(); // ����� ����� ��������

//sensor_power_on(); // ��������� ������� ��������������� ��������
//goto xxx; // PG �� ��������, ���� �����������
//------------------------------------------------------------------------------    
// �������� ��������������� ��� ������� ������� � ��������
    if (DEBUG_TH) printf_dos("Enable power for - RDR + BAR.\n");
    sensor_power_on();
    vTaskDelay(500);
wait_ok:
    if (I_ON != get_sensor_power()){
      //print_str( 0, 2, 0, 0, "123456789012345678901");
        print_str( 0, 1, 0, 0, "������: ��� �������");
        print_str( 0, 2, 0, 0, "�� ������-�������");
        print_str( 0, 3, 0, 0, "---------------------");
        print_str( 0, 4, 0, 1, "�������� ��������� !");
        // �������� �������� ������
        buzz_on();
        if (DEBUG_TH) printf_dos("ERROR: PG = 0 - KZ\n");
        vTaskDelay(1000);
        goto wait_ok;
    }
    buzz_off();
//------------------------------------------------------------------------------    
//xxx:
    if (DEBUG_TH) printf_dos("Hard - Post OK.\n");

    // ������ ������ Router - ��������� �������
    if (DEBUG_TH) printf_dos("RUN TASK router.\n");
    xReturn = xTaskCreate( (TaskFunction_t)vTaskRouter, 
                          "Router",
                          configMINIMAL_STACK_SIZE,
                          NULL,
                          TASK_ROUTER_PRIORITY,
                          NULL );
    
    if( xReturn != pdPASS ){
    	PRINT_FATAL_ERROR("xTaskCreate Router.");
    }

    // ������ ������ �������
    if (DEBUG_TH) printf_dos("RUN TASK console.\n");
    xReturn = xTaskCreate( (TaskFunction_t)vTaskConsole, 
                          "Console",
                          configMINIMAL_STACK_SIZE,
                          NULL,
                          TASK_CONSOLE_PRIORITY,
                          &xTaskConsoleHandle );
    
    if( xReturn != pdPASS ){
    	PRINT_FATAL_ERROR("xTaskCreate Console.");
    }
    
    res = drv_fw_check(); // �������� ������ �� ����� DRV � ���������� �� ���� �����
    if (res != RET_OK){
        printf_dos("ERROR: drv_fw_check() - return error !\nSTOP system.\n");
        while(1) vTaskDelay(1000);
    }

    // ������ ������ �����������
    if (DEBUG_TH) printf_dos("RUN TASK LOG.\n");
    xReturn = xTaskCreate( (TaskFunction_t)vTaskLog, 
                          "Log",
                          configMINIMAL_STACK_SIZE,
                          NULL,
                          TASK_LOG_PRIORITY,
                          NULL );
    
    if( xReturn != pdPASS ){
    	PRINT_FATAL_ERROR("xTaskCreate Log.");
    }
    
    // ������ ������ ������ ��������
    if (DEBUG_TH) printf_dos("RUN TASK input sensors.\n");
    xReturn = xTaskCreate( (TaskFunction_t)vTaskInputSensors, 
                          "InpSens",
                          configMINIMAL_STACK_SIZE,
                          NULL,
                          TASK_INPSENS_PRIORITY,
                          NULL );
    
    if( xReturn != pdPASS ){
    	PRINT_FATAL_ERROR("xTaskCreate Input sensor.");
    }
    
    // ������ ������ ��������
    if (DEBUG_TH) printf_dos("RUN TASK DRV.\n");
    xReturn = xTaskCreate( (TaskFunction_t)vTaskDrv, 
                          "Drv",
                          configMINIMAL_STACK_SIZE,
                          NULL,
                          TASK_DRV_PRIORITY,
                          NULL );
    
    if( xReturn != pdPASS ){
    	PRINT_FATAL_ERROR("xTaskCreate Drv.");
    }

    // ������ ������ ������������ ����������
    if (DEBUG_TH) printf_dos("RUN TASK KEYBOARD.\n");
    xReturn = xTaskCreate( (TaskFunction_t)vTaskKeyboard, 
                          "KeyBrd",
                          configMINIMAL_STACK_SIZE,
                          NULL,
                          TASK_KEYBOARD_PRIORITY,
                          NULL );
    
    if( xReturn != pdPASS ){
    	PRINT_FATAL_ERROR("xTaskCreate Keyboard.");
    }
    
    print_str( 27, 1, 0, 0, "�����");
    
    msg = M_START;
    xReturn = xQueueSendToBack(xLog, &msg, 0);
    if (xReturn != pdPASS) printf_dos("TASK POST: error - xQueueSendToBack( xQueueSendToBack(xLog, &msg\n");
    
    printf_dos("==== DEVICE TEST POST END ====\n");
    vTaskDelete(NULL);
}

//------------------------------------------------------------------------------
// �������� ������ �� ����� DRV � ���������� �� ���� �����
//------------------------------------------------------------------------------
result_t drv_fw_check(void)
{
    result_t result;
    portBASE_TYPE res;
    packet_struct_t tx;
    packet_struct_t rx;
    packet_soft_version_t *psv = 0;
    firmware_header_v20_t *fwh = 0;
    uint8_t flag_fw_update = 0; // ���� - ��������� �������� �� DRV
    
    tx.preamble = PREAMBLE_32_BIT;
    tx.adr_dst  = ADR_DRV;
    tx.adr_src  = ADR_TPOST;
    tx.type     = CMD_VERSION;

//printf_d("=======UPGRADE FW======\n");    
//flag_fw_update = 1;

    // ������ ������ �� DRV BOOT------------------------------------------------
    res = xQueueSendToBack( xPakTPostToRouter, &tx, 0);
    if (res != pdPASS){
        printf_dos("TASK POST: error - xQueueSendToBack( xPakTPostToRouter, tx = VERSION)\n");
        return RET_ERR;
    }
    vTaskDelay(100);
    res = xQueueReceive(xPakRouterToTPost, &rx, 0);
    if (res != pdPASS){
        printf_dos("ERROR1: Not response? GET VERSION software DRV-BOOT.\n");
        print_str( 0, 4, 0, 1, "������:��������������");
        print_str( 0, 5, 0, 1, " ������ �����������  ");
        
        while(1) vTaskDelay(1000);
    }
    
    psv = (packet_soft_version_t*)rx.data;
    
    printf_dos("GET DRV Version Software:---------------------------------------\n");
    printf_dos("%d.%d.%d\n", psv->soft_ver_h, psv->soft_ver_l, psv->soft_ver_y);
    if (psv->fw_types == FW_APP) printf_dos("SoftType = Master APP\n");
	if (psv->fw_types == FW_BOOT)  printf_dos("SoftType = Master BOOT\n");
    printf_dos("----------------------------------------------------------------\n");
    
    print_str( 0, 2, 0, 0, "������ �� ��������:");
    sprintf((char*)char_buf, "���������:%02d.%02d.%02d", psv->soft_ver_h, psv->soft_ver_l, psv->soft_ver_y);
    print_str( 0, 3, 0, 0, (char const *)char_buf);

    // ��������� ������ ��
    if (psv->soft_ver_h != SOFT_VER_H){
        print_str( 0, 4, 0, 1, "������:��������������");
        print_str( 0, 5, 0, 1, " ������ �����������  ");
        while(1) vTaskDelay(1000);
    }
    
    fwh = (firmware_header_v20_t *)fw_drv_app;
    printf_dos("My(FLASH) DRV Version Software:---------------------------------\n");
    printf_dos("%d.%d.%d\n", fwh->version.v1, fwh->version.v2, fwh->version.v3);
    if (fwh->soft_types == FW_SOFT_TYPES_MBOOT) printf_dos("SoftType = Master BOOT\n");
	if (fwh->soft_types == FW_SOFT_TYPES_MAPP)  printf_dos("SoftType = Master APP\n");
	if (fwh->soft_types == FW_SOFT_TYPES_SBOOT) printf_dos("SoftType = Secondary BOOT\n");
	if (fwh->soft_types == FW_SOFT_TYPES_SAPP)  printf_dos("SoftType = Secondary APP\n");
    printf_dos("----------------------------------------------------------------\n");

    vTaskDelay(1000); // ���� ����� ����������� �� APP - DRV
    
    // ������ ������ �� DRV ----------------------------------------------------
    res = xQueueSendToBack( xPakTPostToRouter, &tx, 0);
    if (res != pdPASS){
        printf_dos("TASK POST: error - xQueueSendToBack( xPakTPostToRouter, tx = VERSION)\n");
        return RET_ERR;
    }
    vTaskDelay(100);
    res = xQueueReceive(xPakRouterToTPost, &rx, 0);
    if (res != pdPASS){
        printf_dos("ERROR: Not response? GET VERSION software DRV-APP.\n");
        flag_fw_update = 1; // ��������� �� �.�. APP - �� ��������, ������� ?!
        goto fw_update;
        //return RET_ERR;
        // ��� �������� �������� !
    }
    
    psv = (packet_soft_version_t*)rx.data;
    
    printf_dos("GET DRV Version Software:---------------------------------------\n");
    printf_dos("%d.%d.%d\n", psv->soft_ver_h, psv->soft_ver_l, psv->soft_ver_y);
    if (psv->fw_types == FW_APP) printf_dos("SoftType = Master APP\n");
	if (psv->fw_types == FW_BOOT)printf_dos("SoftType = Master BOOT\n");
    //printf_dos("GIT: %s\n", psv->git_commit_str);
    printf_dos("----------------------------------------------------------------\n");

    // ���������� ��������� � ���������� ������ ��� APP - ����� �������� ��
    if (psv->fw_types == FW_BOOT){
        printf_dos("psv->fw_types == FW_BOOT: DRV-APP - not found or Error, UPDATE fw DRV.\n");
        flag_fw_update = 1;
    }

    if (psv->soft_ver_l < fwh->version.v2 && psv->soft_ver_y <= fwh->version.v3){
        printf_dos("Version software: mb(DRV) != GET(DRV), UPDATE fw DRV.\n");
        flag_fw_update = 1;
    } else {
        printf_dos("Version software: mb(DRV), GET(DRV) - OK.\n");
    }

// Firmware UPDATE -------------------------------------------------------------
fw_update:
    if (flag_fw_update){
        print_str( 0, 4, 0, 1, "��������.�� ��������");
        driver_pcb_reset();
        vTaskDelay(200);
        result = fw_send_to_drv(ADR_DRV, ADR_TPOST, 
                              &xPakTPostToRouter, &xPakRouterToTPost,
                              (uint8_t*)fw_drv_app, fw_drv_app_length);
        printf_dos("fw_send_to_drv result=%d\n", result);
        if (result != RET_OK) return result;
        
        // �������� ��� ���� DRV-APP �������� � �������� ---------------------------
        driver_pcb_reset();
        printf_dos("RESET - DRV PCB\n");
        printf_dos("Delay...\n");
        vTaskDelay(6000); // Delay for: 1.DRV(FW_COPY->APP), 2.reboot, 3.run BOOT, 4.run APP
        
    } else {
        // �������� ��� ���� DRV-APP �������� � �������� ---------------------------
        driver_pcb_reset();
        printf_dos("RESET - DRV PCB\n");
        printf_dos("Delay...\n");
        vTaskDelay(2000); // Delay for: 2.reboot, 3.run BOOT, 4.run APP
    }
    
    printf_dos("GET VERSION ?\n");
    res = xQueueSendToBack( xPakTPostToRouter, &tx, 0);
    if (res != pdPASS){
        printf_dos("TASK POST: error - xQueueSendToBack( xPakTPostToRouter, tx = VERSION)\n");
        return RET_ERR;
    }
    vTaskDelay(100);
    res = xQueueReceive(xPakRouterToTPost, &rx, 0);
    if (res != pdPASS){
        printf_dos("ERROR2: Not response? GET VERSION software DRV-BOOT.\n");
        print_str( 0, 4, 0, 1, "������:��������������");
        print_str( 0, 5, 0, 1, " ������ �����������  ");
        while(1) vTaskDelay(1000);
        //return RET_ERR; // ��� ������ � �������
    }
    
    // ���������� ������ -------------------------------------------------------
    psv = (packet_soft_version_t*)rx.data;
    
    printf_dos("GET DRV Version Software:---------------------------------------\n");
    printf_dos("%d.%d.%d\n", psv->soft_ver_h, psv->soft_ver_l, psv->soft_ver_y);
    if (psv->fw_types == FW_APP) printf_dos("SoftType = Master APP\n");
	if (psv->fw_types == FW_BOOT)  printf_dos("SoftType = Master BOOT\n");
    printf_dos("----------------------------------------------------------------\n");
    
    if (psv->fw_types == FW_BOOT || 
        psv->soft_ver_h != fwh->version.v1 ||
        psv->soft_ver_l != fwh->version.v2 ||
        psv->soft_ver_y != fwh->version.v3 
            ){
        printf_dos("ERROR: DRV Soft Not UPDATE !\n");
        print_str( 0, 4, 0, 1, "������:�� ��������");
        print_str( 0, 5, 0, 1, "�� ����������.");
        print_str( 0, 6, 0, 1, " ������ �����������  ");
        while(1) vTaskDelay(1000);
    }
    
    return result;
}
