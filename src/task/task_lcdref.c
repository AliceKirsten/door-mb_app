// TASK LCD refresh - ������ ���������� ������ �� LCD
#include "os.h"
#include "lcd.h"

uint8_t flag_fb_refresh = 1;

void vTaskLcdRef(void const * argument)
{

WHILE_BEGIN

    switch(flag_fb_refresh){
        case 0:{
            lcd_cls();
            lcd_refresh();
            while(flag_fb_refresh == 0) vTaskDelay(500);
        }
        
        default:{
            lcd_refresh();
            vTaskDelay(500);
            lcd_refresh();
            vTaskDelay(500);
        }
    } // switch

WHILE_END
}
