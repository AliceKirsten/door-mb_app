// ������:
// ������������ ����������
//
//
//
#include "os.h"
#include "printf_hal.h"
#include "keyboard.h"
#include "hardware.h"


#ifndef DEBUG_KEYBOARD
#define DEBUG_KEYBOARD 0
#endif

extern xQueueHandle xKey;   // ������� �� ������ Task Keyboard - ���������� ������� ������

// ��������� ������ �����������
key_status_t key1_st;
key_status_t key2_st;
key_status_t key3_st;
key_status_t key4_st;


void vTaskKeyboard(void const * argument)
{
    portBASE_TYPE res;
    int c = 0;
    uint8_t k;

    if (DEBUG_ROUTER) printf_dos("Task Keyboard - START.\n");
    
    key1_st.get_key_status = key1_get_status;
    key1_st.key_auto       = KEY_WAIT_DOWN;
    key1_st.get_key_status = key1_get_status;

    key2_st.get_key_status = key2_get_status;
    key2_st.key_auto       = KEY_WAIT_DOWN;
    key2_st.get_key_status = key2_get_status;

    key3_st.get_key_status = key3_get_status;
    key3_st.key_auto       = KEY_WAIT_DOWN;
    key3_st.get_key_status = key3_get_status;

    key4_st.get_key_status = key4_get_status;
    key4_st.key_auto       = KEY_WAIT_DOWN;
    key4_st.get_key_status = key4_get_status;

WHILE_BEGIN

    key_status( &key1_st );
    key_status( &key2_st );
    key_status( &key3_st );
    key_status( &key4_st );
    
    //-------------------------------------------------------------------------
    // �� ��������� ��������������� ������� ������
    c = 0;
    if (key1_st.key != KEY_POS_NONE) c++;
    if (key2_st.key != KEY_POS_NONE) c++;
    if (key3_st.key != KEY_POS_NONE) c++;
    if (key4_st.key != KEY_POS_NONE) c++;
    
    if (c > 1) {
        vTaskDelay(1000);
        continue;
    }
    //-------------------------------------------------------------------------

    if (key1_st.key == KEY_POS_CLICK){
        k = KEY_UP;
        if (DEBUG_KEYBOARD) printf_dos("KEY 1 - event CLICK !\n");
        res = xQueueSendToBack( xKey, &k, 0);
        if (res != pdPASS){
            if (DEBUG_KEYBOARD) printf_dos("WARNING: Keyboard buffer full !\n");
        }
    }
    
    if (key2_st.key == KEY_POS_CLICK){
        k = KEY_DOWN;
        if (DEBUG_KEYBOARD) printf_dos("KEY 2 - event CLICK !\n");
        res = xQueueSendToBack( xKey, &k, 0);
        if (res != pdPASS){
            if (DEBUG_KEYBOARD) printf_dos("WARNING: Keyboard buffer full !\n");
        }
    }
    
    if (key3_st.key == KEY_POS_CLICK){
        k = KEY_EXIT;
        if (DEBUG_KEYBOARD) printf_dos("KEY 3 - event CLICK !\n");
        res = xQueueSendToBack( xKey, &k, 0);
        if (res != pdPASS){
            if (DEBUG_KEYBOARD) printf_dos("WARNING: Keyboard buffer full !\n");
        }
    }
    
    if (key4_st.key == KEY_POS_CLICK){
        k = KEY_OK;
        if (DEBUG_KEYBOARD) printf_dos("KEY 4 - event CLICK !\n");
        res = xQueueSendToBack( xKey, &k, 0);
        if (res != pdPASS){
            if (DEBUG_KEYBOARD) printf_dos("WARNING: Keyboard buffer full !\n");
        }
    }
    
    vTaskDelay(10);

WHILE_END

}

