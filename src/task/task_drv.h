#ifndef TASK_DRV_H_
#define TASK_DRV_H_

// ��������� ������� - ����/������
typedef enum
{
    DRV_ERROR = 0,
    DRV_FIRE,
    DRV_STOP,
    DRV_EXIT,
    DRV_WINTER,
    DRV_MANUAL,
    DRV_AUTO  
} drv_control_st_e;

typedef struct
{
    drv_control_st_e current;
    drv_control_st_e prev;
} drv_control_st_t;

typedef struct {
    uint8_t   elpart_config;       // ���� ������������ ������������� ����� ����� DRV
    int32_t   motor_i_max;         // ������ �� ���� ���������
    int32_t   motor_u_max;         // ������ �� ���������� ���������     
    float     speed_coef;          // ����������� ���������   
    float     speed_coef_winter;   // ����������� ��������� � ������ ������ 
    float     open_part_winter;    // ������� ��������� � ������ ������     
    int8_t    reverse;             // ������ ������     
    
    uint8_t   calibration;
    
    uint8_t   mechpart_config;     // ���� ������������ ������������ ����� ����� DRV
    uint16_t  nom_vel;             // ����������� �������� (�.�.)     
    int32_t   position_close;      // ��������� ��������  (�.�.)
    int32_t   position_open;       // ��������� ��������  (�.�)

    drv_control_st_t state;        // ����� ���������� (������� � ����������) - ����/������

  // �� ����������� - ������������ ��������� ���������������
    float c1;
    float c2;
    float c3;
    float c4;
    
} drv_config_t;


void vTaskDrv(void const * argument);

#endif