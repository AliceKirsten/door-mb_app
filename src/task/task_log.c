// ������:
// �����������
// ��������� ���������
//
//
//
#include <string.h>
#include <time.h>
#include "os.h"
#include "printf_hal.h"
#include "lcd.h"
#include "msg_types.h"
#include "debug.h"
#include "keyboard.h"
#include "task_log.h"
#include "lcd_menu.h"
#include "log_array.h"
#include "sys_timer.h"
#include "rtc.h"
#include "hardware.h"
#include "fw_types.h"
#include "fw.h"
#include "version.h"

#ifndef DEBUG_LOG
#define DEBUG_LOG 0
#endif

#define UPDATE_TIME_TO_LCD_SEC  (30)      // ����� ����� ������� ��������� ���������� ����� �� LCD
#define UPDATE_HRTC_TO_SRTC_SEC (60*60)   // ����� ����� ������� ��������� ���������� Hard RTC � soft RTC

extern uint8_t flag_fb_refresh;           // ���� ������ ����� �� LCD
extern xQueueHandle xPakRouterToTLog;     // ������� ������ Router -> TASK Log
extern xQueueHandle xPakTLogToRouter;     // ������� ������ TASK Log -> Router
extern xQueueHandle xLog;                 // ������� ������ ��� ������ Task Log (��� ������ ����������� � ������ �������)
extern status_sensors_t cur_send_st;      // ���������� ��������� ������� ��������
extern door_curent_state_t door_current;  // ������� ��������� ������

extern xQueueHandle xKey;    // ������� �� ������ Task Keyboard - ���������� ������� ������
extern key_status_t key1_st; // ��������� ������ ����������� �� ������ task_keyboard
extern key_status_t key2_st;
extern key_status_t key3_st;
extern key_status_t key4_st;

extern menu_position_t menu_pos;
extern lcd_state_e lcd_st;
extern cursor_position_t cursor_pos;

extern struct tm *rtc_tm; // ����� � ���� � ������� struct tm, ������ - ������ ��/� RTC

// function --------------------------------------------------------------------
static void door_leaf_string( char *buf, uint8_t pr);
static void lcd_print_status(void);
static packet_for_send_t packet_toss( packet_struct_t *in, packet_struct_t *out);
// function --------------------------------------------------------------------

static char str_sprintf[32]; // ����� ��� ����� sprint
static packet_struct_t packet_rx;
static packet_struct_t packet_tx;
portTickType xTimeMenuTimeout;    // ����� ����� ���������� ��������� �������� ��������� � ���� (������������ � lcd_menu.c)
portTickType xTimeLCDTimeout;     // ����� ����� ���������� ��������� �������� � ����� ������� �����

log_allocation_table_t log_lat;
static log_record_t log_r;
static log_return_st ret;
static packet_for_send_t ps;      // ������, ���� ����� ��� ������� ��-���?

static uint32_t time_count = 0;   // ������� ����� ������������ ������� ������ � ���. ����� (���� ������ ��� �� �������� �������)
static uint32_t time_sync = 0;    // ������� ������������ ���������� ������� � ������� �������� 

static packet_get_log_rec_quantity_t *pglrq = 0;
static packet_get_log_rec_t * pglgr = 0;
static packet_fw_param_t *pfwp = 0;
static packet_fw_data_t *pfwd = 0;
static packet_fw_data_write_t *pfwdw = 0;
static packet_fw_upgrade_t *pfwu = 0;
static packet_soft_version_t *psv = 0;

static uint32_t packet_fw_data_count = 0;  // ������� ������� � ������� ��������
static uint32_t n_packet_to_block = 0;     // ���������� ������� ����������� ��� ������ �����
static uint8_t flash_buf[ FW_BLOCK_SIZE ]; // ����� � ������� �������� ��� ������ �� flash
static size_t flash_buf_index = 0;         // ������ ��� flash_buf[]
static uint32_t flash_wr_adr = 0;          // ����� �� FLASH ���� ������������ ������� ������
static uint8_t result = 0;                 // ���������
static uint8_t flag_block_ready = 0;       // ���� ������ � ����� � ������
static uint16_t error_counter = 0;         // ���������� ������ ��� ������ ������� (����� ��������� �������)

//-----------------------------------------------------------------------------
// ������ �����������
//-----------------------------------------------------------------------------
void vTaskLog(void const * argument)
{
    uint32_t c = 0; // ���������� ���������� �������� ������ �� lcd_print_status() ������ ����� ������ ������ �������� �� ������� !
    portBASE_TYPE res;
    msg_t msg;
    key_position_t key;
    
    packet_tx.preamble = PREAMBLE_32_BIT;
    packet_tx.adr_dst  = ADR_PC;
    packet_tx.adr_src  = ADR_TLOG;

	lcd_st = LCD_ST_DAT_DOOR;
	menu_pos.pos    = 0;
	menu_pos.select = 0;
	cursor_pos.cursor = CURSOR_DISABLE;

    if (DEBUG_LOG) printf_dos("Task Log - START.\n");
    
    lcd_cls();
    xTimeMenuTimeout = xTaskGetTickCount() + MENU_TIMEOUT_SEC;

WHILE_BEGIN

    if (xTaskGetTickCount() >= xTimeMenuTimeout){ 
        lcd_cls();
        flag_fb_refresh = 0; // ������������� ���������� ������
    }

    switch(lcd_st){
	    case LCD_ST_DAT_DOOR:
            if (uxQueueMessagesWaiting(xKey)){// ���� ������ ������ ��������� � ����
                while( xQueueReceive( xKey, &key, 0) != pdFALSE); // ���������� ��� �������, ����� ��������� � ����
                time_count = 0; // ���������� ������� ���� ���������� ���� � ����� � lcd_print_status()
                if (flag_fb_refresh == 0){// �������� ����� �� LCD � �������, �����(���� ����� �� ����� �������) ��������� � ����
                    flag_fb_refresh = 1;
                    xTimeMenuTimeout = xTaskGetTickCount() + MENU_TIMEOUT_SEC;
                    break;
                }
                lcd_cls();
				lcd_st = LCD_ST_MENU;
                xTimeMenuTimeout = xTaskGetTickCount() + MENU_TIMEOUT_SEC;
                cursor_pos.cursor = CURSOR_DISABLE; // ��������� ���������� ��� ������ � ���� � ��������� �� ���������
                menu_pos.pos    = 0;
	            menu_pos.select = 0;
                break;
            }
            if (c == 0){ lcd_print_status(); c = 100;} // ����� ���������� ������ ������ 1/100 ���  
            c--;
			break;

		case LCD_ST_MENU:
            if (xTaskGetTickCount() >= xTimeMenuTimeout){ // ������� �� ���� �� ��������� �������
                cursor_pos.cursor = CURSOR_DISABLE;
                menu_pos.pos    = 0;
	            menu_pos.select = 0;
                lcd_cls();
                lcd_st = LCD_ST_DAT_DOOR;
                break;
            }
		    lcd_print_menu();
			break;

		case LCD_ST_INPUT:
            if (xTaskGetTickCount() >= xTimeMenuTimeout){ // ������� �� ���� �� ��������� �������
                cursor_pos.cursor = CURSOR_DISABLE;
                menu_pos.pos    = 0;
	            menu_pos.select = 0;
                lcd_cls();
                lcd_st = LCD_ST_DAT_DOOR;
                break;
            }
		    lcd_input_val();
			break;
	} // switch ----------------------------------------------------------------

//------------------------------------------------------------------------------
// ��������� ������� ��������� � ������

    // ��������� �� ������� ������ xLog ----------------------------------------
    res = xQueueReceive(xLog, &msg, 0);
    if (res == pdPASS){
        if (DEBUG_LOG) printf_dos("TASK LOG: get MSG = %d\n", msg);
        log_r.time = time_get_sec_counter(); // ����+�����
        log_r.msg = msg;                     // ������
        ret = log_set_rec( &log_lat, log_r); // ��������� ������
        if (ret == LOG_RET_ERR) printf_dos("ERROR: log_set_rec.\n");
        if (msg != M_NONE || msg != M_START) error_counter++;
    }

    // ��������� �� xPakRouterToTLog -------------------------------------------
    res = xQueueReceive(xPakRouterToTLog, &packet_rx, 0);
    if (res == pdPASS){
        if (DEBUG_LOG) printf_dos("TASK LOG: get Packet\n");
        // ����� ��������� ���������� ������
        ps = packet_toss(&packet_rx, &packet_tx);
        //hex_out((const char*)&packet_rx, PACKET_SIZE);
    }
    
    if (ps == SPACK_YES){ // ���� ����� �� �������� - ����������
        ps = SPACK_NO;
        res = xQueueSendToBack( xPakTLogToRouter, &packet_tx, portMAX_DELAY);
        if (res != pdPASS)
          printf_dos("ERROR: TLOG: xQueueSendToBack( xPakTLogToRouter, &packet_tx, 0)\n");
        else
          if (DEBUG_LOG) printf_dos("TASK LOG: packet_tx to PC OK\n");
    }

    
    vTaskDelay(1);

WHILE_END

}

//-----------------------------------------------------------------------------
// ����� ��������� ������� (������� ���.�� � �.�.)
//-----------------------------------------------------------------------------
static void lcd_print_status(void)
{
    uint8_t pr;
    uint32_t time;

    //���� ����� --------------------------------------------------------------
    time = time_get_sec_counter();
    if (time - time_count > UPDATE_TIME_TO_LCD_SEC){
        time_count = time;
        rtc_tm = localtime( (time_t const*)&time );
        sprintf(str_sprintf, "%02d-%02d-%02d %02d:%02d", 
                rtc_tm->tm_mday, rtc_tm->tm_mon, rtc_tm->tm_year-100,
                rtc_tm->tm_hour, rtc_tm->tm_min );
        print_str( 27, 0, 0, 0, str_sprintf);
    }
    
    // ������������� ���������� ������� � �����������
    if (time - time_sync > UPDATE_HRTC_TO_SRTC_SEC){
        // time_sync - ���������� ��� ���������� �������� ������� �� HARD RTC
        if (rtc_get_time32( &time_sync )) printf_d("ERROR[%s %d]: rtc_get_time32\n", __FUNCTION__, __LINE__);
        else {
            time_set_sec_counter( time_sync );
            time_sync = time;
        }
    }
    

    //-------------������              ������� ���������               ��������� ��������� - ��������
    if (cur_send_st.rdr1 == I_OFF) print_str( 0,  1, 0, 0, "P1"); else print_str( 0,  1, 1, 0, "P1");
    if (cur_send_st.rdr2 == I_OFF) print_str( 16, 1, 0, 0, "P2"); else print_str( 16, 1, 1, 0, "P2");
    if (cur_send_st.bar1 == I_OFF) print_str( 34, 1, 0, 0, "�1"); else print_str( 34, 1, 1, 0, "�1");
    if (cur_send_st.bar2 == I_OFF) print_str( 50, 1, 0, 0, "�2"); else print_str( 50, 1, 1, 0, "�2");
    if (cur_send_st.stop == I_OFF) print_str( 66, 1, 0, 0, "����"); else print_str( 66, 1, 1, 1, "����");
    if (cur_send_st.fire == I_OFF) print_str( 95, 1, 0, 0, "�����"); else print_str( 95, 1, 0, 1, "�����");
    
  //    print_str( 0, 2, 0, 0, "123456789012345678901");
    //print_str( 0, 2, 0, 0, "������.���:�� �������");
    switch(cur_send_st.pdu.current){
    case I_NONE:// ����� �� ���������
        print_str( 0, 2, 0, 0, "������.���:�� �������");
        break;
    case I_CLOSE:// �������
        print_str( 0, 2, 0, 0, "������.���: �������  ");
        break;
    case I_AUTO:// �������
        print_str( 0, 2, 0, 0, "������.���: �������  ");
        break;
    case I_EXIT:// ������ �����
        print_str( 0, 2, 0, 0, "������.���: �����    ");
        break;
    case I_WINTER:// ������
        print_str( 0, 2, 0, 0, "������.���: ������   ");
        break;
    case I_OPEN:// �������
        print_str( 0, 2, 0, 0, "������.���: �������  ");
        break;
    }
    
    
    print_str( 0, 3, 0, 0, "������� ��: ����     ");
    //print_str( 0, 4, 0, 0, "���: 100%            ");
    
    if (error_counter){
        sprintf(str_sprintf,   "%5d !   ", error_counter);
        print_str( 0, 5, 0, 0, "������: ");
        print_str( 48, 5, 0, 0, str_sprintf);
    } else {
        print_str( 0, 5, 0, 0, "������: ��� ������   ");
    }


    //print_str( 0, 6, 0, 0, "�����: ������� 100%  ");
    pr = door_current.pos; // �������� ��������� 0-100%
    if (pr > 100) pr = 100;

    switch(door_current.state){
        case DOOR_OPEN: 
            sprintf(str_sprintf, "�����: �������  %3d%% ", pr);
            break;

        case DOOR_CLOSE:
            sprintf(str_sprintf, "�����: �������  %3d%% ", pr);
            break;

        case DOOR_OPEN_MOVE:
            sprintf(str_sprintf, "�����: �������� %3d%% ", pr);
            break;

        case DOOR_CLOSE_MOVE:
            sprintf(str_sprintf, "�����: �������� %3d%% ", pr);
            break;

    }
    print_str( 0, 6, 0, 0, str_sprintf);

    //door_current.state = DOOR_CLOSE;    // �������
    //door_current.pos   = 100;           // 100 % - �.�. ���������
    
    //door_current.lock  = LOCK_POS_OPEN; // ����� ������


//  print_str( 0, 2, 0, 0, "123456789012345678901");
//  print_str( 0, 7, 0, 0, "|---------||--------|");
// ����� �������
    switch(door_current.state){
        case DOOR_OPEN: 
        case DOOR_OPEN_MOVE:
            door_leaf_string(str_sprintf, 100 - pr);
            break;

        case DOOR_CLOSE:
        case DOOR_CLOSE_MOVE:
            door_leaf_string(str_sprintf, pr);
            break;

    }
    print_str( 0, 7, 0, 0, str_sprintf);
}

//-----------------------------------------------------------------------------
// ����� ������� ����� � �������� �����
//-----------------------------------------------------------------------------
static void door_leaf_string( char *buf, uint8_t pr)
{
	size_t i = 0;
	size_t j = 0;
	uint8_t l,p;

	l = (pr * 10) / 125; // ������ �������, ����������� ������ '='
	p = 8 - l;           // 8 - ������������ ����� ��� 100%
	p = p * 2;           // * 2 �.�. ��� ��������� ��������
    
	//printf("%% = %d\n", pr);
	//printf("l = %d\n", l);
    //printf("p = %d\n", p);

	buf[ i ] = '|'; // ����� ������� ����� �����
	i++;

	// ����� - ������� �����
	for ( j = 0; j<l; j++){
		buf[ i + j ] = '=';
	}
	i = i + l;

	buf[ i ] = '|';
	i++;

	// ������� - ������ ����� ����� ���������
	for ( j = 0; j < p; j++){
		buf[ i + j ] = ' ';
	}
	i = i + p;

	// ����� - ������� ������
	buf[ i ] = '|';
	i++;

	for ( j = 0; j < l; j++){
		buf[ i + j ] = '=';
	}
	i = i + l;

	buf[ i ] = '|';// ������ ������� ����� �����
}

//-----------------------------------------------------------------------------
// ��������� �������
//-----------------------------------------------------------------------------
packet_for_send_t packet_toss( packet_struct_t *in, packet_struct_t *out)
{
    command_t type;
    uint32_t dlen = 0;
    result_t res;
    
    pfwd = (packet_fw_data_t*)in->data;

    // copy PACKET DST = header from PACKET SRC
    out->preamble = in->preamble;
    out->adr_dst  = in->adr_src;
    out->adr_src  = in->adr_dst;
    out->type     = in->type;
    
    type = (command_t)in->type;
    
    if (DEBUG_LOG) printf_dos("LOG: PACKET TYPE = %d.\n", type);
    
    switch( type ){ //----------------------------------------------------------
        
    case CMD_LOG_QUA:{
        printf_dos("GET_LOG_QUA:\n");
        pglrq = (packet_get_log_rec_quantity_t *)out->data;
        pglrq->count = log_rec_quantity( &log_lat );
        return SPACK_YES;
    }
        
    case CMD_LOG_REC:{
        printf_dos("GET_LOG_REC:\n");
        pglgr = (packet_get_log_rec_t *)out->data;
      
        if (log_rec_quantity( &log_lat )){ // �������� ������ ���� ������
            ret = log_get_rec( &log_lat, &log_r);
            if (ret == LOG_RET_ERR){printf_dos("ERROR:log_ger_rec\n");pglgr->count = 0;return SPACK_YES;} // ������ IO �������� ������� = 0
            memcpy(pglgr->rec, (void const*)&log_r, sizeof(log_record_t));
            pglgr->count = 1;
        }else{
            pglgr->count = 0; // ��� �������, �������� ���������� = 0
        }
        return SPACK_YES;
    }

    case CMD_VERSION:{
        if (DEBUG_LOG) printf_d("LOG: CMD_VERSION = %d.%d.%d\n", SOFT_VER_H, SOFT_VER_L, SOFT_VER_Y);
        psv = (packet_soft_version_t *)out->data;
        psv->fw_types = FW_TYPE;
        psv->soft_ver_h = SOFT_VER_H;
        psv->soft_ver_l = SOFT_VER_L;
        psv->soft_ver_y = SOFT_VER_Y;
        return SPACK_YES;
    }
      
    case CMD_REBOOT:{// ������������
        run_reboot();
        break;
    }
    
    case CMD_FW_PARAM:{// ��������� ������������ ����� ��
        packet_fw_data_count = 0; // ������� ������� � ������� ��������
        flag_block_ready = 0;
        memcpy(out->data, in->data, sizeof(packet_fw_param_t));
        pfwp = (packet_fw_param_t *)out->data;
        flash_wr_adr = FW_COPY_ADDR; // ������������� ��������� ����� ������� ���� ����� ������������ ������
        
        if (DEBUG_LOG) printf_d("CMD_FW_PARAM: file size = %d, FW_APP_SIZE = %d\n", pfwp->file_size, FW_APP_SIZE);
        if (DEBUG_LOG) printf_d("CMD_FW_PARAM: block size = %d, FW_BLOCK_SIZE = %d\n", pfwp->block_size, FW_BLOCK_SIZE);
        if (pfwp->file_size > FW_APP_SIZE || pfwp->block_size != FW_BLOCK_SIZE) {
            printf_dos("ERROR: File FirmWare or Block size.\n");
            pfwp->result = RET_S_ERR; 
        }else{
            pfwp->result = RET_S_OK; 
            fw_erase_fwcopy(); // �������� ������ ����� �������, fw_copy - �������.

           	//if (size % FW_BLOCK_SIZE) // ������� ���������� ������
        	//	fw_block_all = size / FW_BLOCK_SIZE + 1;
	        //else
            //  fw_block_all = size / FW_BLOCK_SIZE;

            if (DEBUG_LOG) printf_dos("sizeof(packet_fw_data_t) = %d\n", sizeof(packet_fw_data_t));
                       
           	if (FW_BLOCK_SIZE % sizeof(packet_fw_data_t)) // ������� ���������� ������� ����������� ��� ������ �����
        		n_packet_to_block = FW_BLOCK_SIZE / sizeof(packet_fw_data_t) + 1;
	        else
                n_packet_to_block = FW_BLOCK_SIZE / sizeof(packet_fw_data_t);
            
            if (DEBUG_LOG) printf_d("CMD_FW_PARAM: n_packet_to_block = %d\n", n_packet_to_block);
        }
        return SPACK_YES;
    }
    
    case CMD_FW_DATA:{// ������ �� ����� ��
        packet_fw_data_count++; // ������� ������� � ������� ��������
        if (DEBUG_LOG) printf_d("CMD_FW_DATA: packet_fw_data_count=%d\n", packet_fw_data_count);
        
        // �������� ������, � ����� � ������ ���������, ���������� ����� � ���������� ������
        if (packet_fw_data_count > n_packet_to_block){
            printf_d("CMD_FW_DATA: packet_fw_data_count(%d) > n_packet_to_block(%d)\n", packet_fw_data_count, n_packet_to_block);
            flag_block_ready = 0;
            result = RET_S_ERR_BLOCK;
            return SPACK_NO;
        }
        
        // ������� ���������� ������ � ������ ��� �����������
		if (packet_fw_data_count < n_packet_to_block)
			dlen = sizeof(pfwd->data);
		else
            dlen = FW_BLOCK_SIZE - (n_packet_to_block-1) * sizeof(pfwd->data);

        flash_buf_index = (packet_fw_data_count-1) * sizeof(packet_fw_data_t);
        memcpy(&flash_buf[ flash_buf_index ], pfwd, dlen); // �������� ������ �� ������

        if (DEBUG_LOG) printf_d("CMD_FW_DATA: flash_buf_index = %d\n", flash_buf_index);
        if (DEBUG_LOG) printf_d("CMD_FW_DATA: dlen = %d\n", dlen);

        if (packet_fw_data_count == n_packet_to_block) flag_block_ready = 1; // ���� ������
        return SPACK_NO;
    }
    
    case CMD_FW_DATA_WRITE:{ // ������ ����� � FLASH ������
        if (DEBUG_LOG) printf_d("CMD_FW_DATA_WRITE:\n");
        packet_fw_data_count = 0;
        
        pfwdw = (packet_fw_data_write_t *)out->data;
        
        if (flash_wr_adr >= FW_COPY_ADDR + FW_COPY_SIZE){
            if (DEBUG_LOG) printf_d("ERROR: flash_wr_adr(0x%08X) >= FW_COPY_ADDR + FW_COPY_SIZE (0x%08X)\n", flash_wr_adr, FW_COPY_ADDR + FW_COPY_SIZE);
            pfwd->data[0] = RET_S_ERR;
            return SPACK_YES;
        }
        
        if (result != RET_S_OK){
            pfwdw->result = result;
            return SPACK_YES;
        }

        if (flag_block_ready == 0){ // ���� �� ������ - ������
            pfwdw->result = RET_S_ERR_BLOCK;
            return SPACK_YES;
        }

        res = fw_write_data(flash_wr_adr, flash_buf, FW_BLOCK_SIZE);    // ���������� ������ � FLASH, � ���������� ��������� ������ � �������� ������
        if (res == RET_OK){
            pfwdw->result = RET_S_OK;
            flash_wr_adr += FW_BLOCK_SIZE; // ������� ������ �������� �� ��������� �����
            packet_fw_data_count = 0;
        } else {
            pfwdw->result = RET_S_ERR_BLOCKW;
        }
        return SPACK_YES;
    }
    
    case CMD_FW_UPGRADE:{// ������ ����� ���������� ��
        if (DEBUG_LOG) printf_d("CMD_FW_UPGRADE:\n");
        pfwu = (packet_fw_upgrade_t *)out->data;
        pfwu->result = fw_upgrade( (uint8_t*)FW_COPY_ADDR ); // ��������� ����� ������ ������ �.�. ������ ������ �� ��������� ������������
        return SPACK_YES;
    }
    
    default:
        break;
    }// switch -----------------------------------------------------------------
    
    return SPACK_NO;
}

