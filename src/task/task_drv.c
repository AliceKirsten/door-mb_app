// ������:
// ���������� ������ ��������
//
//
//
//
#include "os.h"
#include "printf_hal.h"
#include "msg_types.h"
#include "debug.h"
#include "string.h"
#include "task_drv.h"

#ifndef DEBUG_DRV
#define DEBUG_DRV 1
#endif

#define false (0)
#define true  (1)

#define HALL_SENSITIVITY 55

#define abs(x)  (x >= 0 ? x : -x)

typedef enum
{
    T_INITIALIZE = 0,
    T_CALIBRATE,
    T_WAIT_RESPONSE,
    T_GETDATA,
    T_PROCESS,
    T_SENDLOG,
} task_drv_state_t;

typedef struct
{
    int32_t current;
    int32_t upos;
    int32_t pos;
    int32_t vel;
    int32_t acc;
    float   epos;
    uint8_t ndata;
} device_state_t;

extern xQueueHandle xPakRouterToTDrv;             // ������� ������ Router -> TASK Drv
extern xQueueHandle xPakTDrvToRouter;             // ������� ������ TASK Drv -> Router

extern status_sensors_t cur_send_st;   // ���������� ��������� ������� ��������
// ��������� ������ �������-������� (��� �����������)
// ������� ��������� ������ 0-100%
// ����� ������-������ (�������� ������ - ��� ����� (���� ���))
// ��������: ��������-�������� ������

// ������� ������ ��� ������ �����������
extern xQueueHandle xLog;                         // ������� ������ ��� ������ Task Log (��� ������ ����������� � ������ �������)

extern xQueueHandle xInSensors;

// ��������� ������� ��������� ������� - ���������� �� I2C ������
// ����� ����������� � ��� ���������� �������� (���������� ��������� ������ ����������� �� ���������� ������ � I2C flash)
extern device_config_t device_config;

static packet_struct_t packet_rx; // �������� �����
static packet_struct_t packet_tx; // ����������� �����
static drv_config_t drv_config;

door_curent_state_t door_current; // ������� ��������� ������

//------------------------------------------------------------------------------
static void vCalcDrvParams(void)
{
    drv_config.motor_i_max           = device_config.motor_i_max*HALL_SENSITIVITY;
    drv_config.motor_u_max           = device_config.motor_v_max*1000;
    drv_config.speed_coef            = 1.0f/(float)device_config.speed_open;
    drv_config.speed_coef_winter     = 1.0f/(float)device_config.speed_open_winter;
    drv_config.open_part_winter      = 100.0f/(float)device_config.open_width_in_winter;
    drv_config.reverse               = device_config.end_position_inversion ? -1 : 1;
    drv_config.c4                    = device_config.max_position_error;
}

static void vSendCommandToDrv(command_t cmd, packet_struct_t* ptx)
{
    ptx->adr_dst  = ADR_DRV;
    ptx->type = cmd;
    xQueueSendToBack(xPakTDrvToRouter, ptx, portMAX_DELAY);
}

void vTaskDrv(void const * argument)
{ 
    device_state_t device_state;
    task_drv_state_t task_state = 0, task_state_prev;
    
    pdu_state_e pdu_curr, pdu_prev;
    input_dat_state_e stop_curr, stop_prev;
    input_dat_state_e fire_curr, fire_prev;
    input_dat_state_e test_curr, test_prev;
    
    command_t last_sent_cmd;
    
    uint8_t clb_tries = 0;
    uint8_t rsp_tries = 0;
    uint8_t mov_tries = 0;
    
    float err_sum = 0;
    int   err_div = 1;
    
    msg_t msg; // ������� ������
    portBASE_TYPE res;
    
    uint32_t dpos;      //������ ����� �������� ������
    
    portTickType xTimeTx, xTimeMv;

    packet_tx.preamble = PREAMBLE_32_BIT;
    packet_tx.adr_src  = ADR_TDRV;

    xTimeTx = xTaskGetTickCount() + 100;
    
    if (DEBUG_DRV) printf_dos("Task DRV - START.\n");

//------------------------------------------------------------------------------------------------------------
    
//------------------------------------------------------------------------------------------------------------

//xTimeTx = xTaskGetTickCount() + 30000;
//do
//{
//    test_curr = cur_send_st.test;
//    if (test_curr != test_prev)     //����������
//    {
//        if (test_curr == I_ON)
//        {
//            if (DEBUG_DRV) printf_dos("Calibration on startup\n");
//            
//            drv_config.calibration = false;
//            drv_config.mechpart_config = false;
//            clb_tries = 0;
//            task_state = T_CALIBRATE;
//            break;
//        }
//        test_prev = test_curr;
//    }
//    vTaskDelay(10);
//} while (xTimeTx > xTaskGetTickCount());
      

WHILE_BEGIN

  vCalcDrvParams();
  
  switch (task_state)
  {
      case T_WAIT_RESPONSE:     //�������� ������������� ������ �� ��������
        if (DEBUG_DRV) printf_dos("TDRV: task state T_WAIT_RESPONSE\n");
        res = xQueueReceive(xPakRouterToTDrv, &packet_rx, 0); // ���������� ����� ���� ����, �� ������� !
        if (res == pdPASS && packet_rx.adr_src == ADR_DRV)
        {
          if (DEBUG_DRV) printf_dos("T_WAIT_RESPONSE: get Packet %d\n", packet_rx.type);
          if (packet_rx.type == packet_tx.type) //������� ���������� �����
          {
              switch (packet_rx.type)
              {
                  case GetElectData:                              //����� �� ������ ������������� - 1   
                    int32_t   i_max = 0, u_max = 0;             
                    float     sp_coef = 0, sp_coef_w = 0, open_w = 0;    
                    int8_t    rvs = 0;
                    
                    memcpy(&i_max, &packet_rx.data[0], 4);
                    memcpy(&u_max, &packet_rx.data[4], 4);
                    memcpy(&sp_coef, &packet_rx.data[8], 4);
                    memcpy(&sp_coef_w, &packet_rx.data[12], 4);
                    memcpy(&open_w, &packet_rx.data[16], 4);
                    memcpy(&rvs, &packet_rx.data[20], 1);
                    if (i_max     != drv_config.motor_i_max       ||
                        u_max     != drv_config.motor_u_max       ||
                        sp_coef   != drv_config.speed_coef        ||
                        sp_coef_w != drv_config.speed_coef_winter ||
                        open_w    != drv_config.open_part_winter  ||
                        rvs       != drv_config.reverse)
                    {
                        memcpy(&packet_tx.data, &drv_config.motor_i_max, 21);
                        vSendCommandToDrv(SetElectData, &packet_tx);
                    }
                    drv_config.elpart_config = true;
                    break;
                  case GetMechData:                               //����� �� ������ ������������� - 2
                    memcpy(&drv_config.nom_vel, &packet_rx.data, 12);
                    dpos = abs(drv_config.position_close - drv_config.position_open);
                    drv_config.mechpart_config = true;
                    drv_config.state.current = drv_config.state.prev = DRV_AUTO;
                    break;
                  case GetCalibrate:                              //����� �� ������ ����������
                    drv_config.calibration = true;
                    drv_config.mechpart_config = false;
                    break; 
                  case GetSystemData:                             //����� �� ������ ��������� ������
                    memcpy(&device_state, packet_rx.data, 24);
                    device_state.ndata = true;
                    break;
              }
          }
          else  //������� ������������ �����
          {
              switch (packet_rx.type)
              {
                  case GetErrorData: 
                    memcpy(&msg, &packet_rx.data[0], 1);
                    if (DEBUG_DRV) printf_dos("T_WAIT_RESPONSE: get Error %d\n", msg);
                    switch (msg)
                    {
                        case M_CALIBRATE_ERR:   
                            if (task_state_prev == T_CALIBRATE)
                            {
                                int32_t fv, bv;
                                float ft, bt;
                                memcpy(&fv, &packet_rx.data[1], sizeof(int32_t));
                                memcpy(&bv, &packet_rx.data[5], sizeof(int32_t));
                                memcpy(&ft, &packet_rx.data[9], sizeof(float));
                                memcpy(&bt, &packet_rx.data[13], sizeof(float));
                                if (DEBUG_DRV) printf_dos("T_WAIT_RESPONSE: get fv = %d, \
                                                bv = %d, ft = %d, bt = %d\n", 
                                                fv, bv, (int)(ft*100), (int)(bt*100));
                                if (fv == 0 || abs(fv) > 200 || bv == 0 || abs(bv) > 200)
                                {
                                    msg = M_ENCODER_ERR;
                                    xQueueSendToBack(xLog, &msg, portMAX_DELAY); 
                                }
                                if (ft == 0 || ft > 0.1 || bt == 0 || bt > 0.1)
                                {
                                    msg = M_HIGH_WEIGHT;
                                    xQueueSendToBack(xLog, &msg, portMAX_DELAY); 
                                }
                            }
                            break;
                        case M_DRV_NO_DATA:
                            memcpy(&packet_tx.data, &drv_config.motor_i_max, 21);
                            vSendCommandToDrv(SetElectData, &packet_tx);
                        default:
                            xQueueSendToBack(xPakRouterToTDrv, &packet_rx, portMAX_DELAY); //���������� �����
                            break;
                            
                    }
                    xQueueSendToBack(xLog, &msg, portMAX_DELAY); 
                    break; 
                  default:
                    if (DEBUG_DRV) printf_dos("T_WAIT_RESPONSE: wrong packet received\n");
                    xQueueSendToBack(xPakRouterToTDrv, &packet_rx, portMAX_DELAY); //���������� �����
                    break;
              }
          }
          task_state = task_state_prev;       //������� � ���������� ���������
          break;
        }
        else if (res == pdPASS) xQueueSendToBack(xPakRouterToTDrv, &packet_rx, portMAX_DELAY); //���������� �����
        if (rsp_tries < 10)     //�������� �������� ������ �������� ������
        {
            if (DEBUG_DRV) printf_dos("T_WAIT_RESPONSE: resp_tries: %d\n", rsp_tries);
            vTaskDelay(100);
            rsp_tries++;
        }
        else                    //������� �� �������� ��� �������� �� �� -> ������
        {
            if (DEBUG_DRV) printf_dos("T_WAIT_RESPONSE: go to the next state\n");
            msg = M_DRV_SILENT;
            xQueueSendToBack(xLog, &msg, portMAX_DELAY);
            //drv_config.state.current = DRV_ERROR;
            task_state = T_GETDATA;     //������� � �������� ����
        }
        break;
      case T_INITIALIZE:        //������������� ��������
        if (DEBUG_DRV) printf_dos("TDRV: task state T_INITIALIZE\n");
        if (!drv_config.elpart_config)       //������ ������������� 1
        {
            if (DEBUG_DRV) printf_dos("T_INITIALIZE: request electrical data\n");
            vSendCommandToDrv(GetElectData, &packet_tx);
            task_state_prev = task_state;
            task_state = T_WAIT_RESPONSE;
            rsp_tries = 0;
            break;
        }
        
        if (!drv_config.mechpart_config)     //������ ������������ 2
        {
            if (DEBUG_DRV) printf_dos("T_INITIALIZE: request mechanical data\n");
            vSendCommandToDrv(GetMechData, &packet_tx);
            task_state_prev = task_state;
            task_state = T_WAIT_RESPONSE;
            rsp_tries = 0;
            break;
        }
        //��� ���������� - ����������
        if (!drv_config.calibration) task_state = T_CALIBRATE;       
        else 
        {
            if (DEBUG_DRV) printf_dos("T_INITIALIZE: calibration needed\n");
            xTimeMv = xTaskGetTickCount();
            //���� ���������� - �������������� �����
            drv_config.state.current = DRV_AUTO;
            task_state = T_GETDATA;
        }
        break;
      case T_CALIBRATE: //��������� ���������� - ��� �������
        if (DEBUG_DRV) printf_dos("TDRV: task state T_CALIBRATE\n");
        if (clb_tries < 3 && !drv_config.calibration)
        {
            if (DEBUG_DRV) printf_dos("T_CALIBRATE: request calibration\n");
            clb_tries++;
            vSendCommandToDrv(GetCalibrate, &packet_tx);
            vTaskDelay(40000);
            task_state_prev = task_state;
            task_state = T_WAIT_RESPONSE;
            rsp_tries = 0;
            break;
        }  //���� ���������� �������� - ������ ����� ������������� ������
        else if (drv_config.calibration && !drv_config.mechpart_config)
        {
            if (DEBUG_DRV) printf_dos("T_CALIBRATE: request mechanical data\n");
            vSendCommandToDrv(GetMechData, &packet_tx);
            task_state_prev = task_state;
            task_state = T_WAIT_RESPONSE;
            break;
        } //���� ���������� ����������
        if (clb_tries == 3 &&
           !drv_config.calibration &&
           !drv_config.mechpart_config) 
        {
            if (DEBUG_DRV) printf_dos("T_CALIBRATE: calibration filed\n");
            drv_config.state.current = DRV_MANUAL;   //��������� ���������� ������� - ������ �����
            vSendCommandToDrv(SetManCtrl, &packet_tx);
            msg = M_CALIBRATE_ERR;
            xQueueSendToBack(xLog, &msg, portMAX_DELAY);
        }
        else 
        {
            if (DEBUG_DRV) printf_dos("T_CALIBRATE: calibration done\n");
            drv_config.state.current = DRV_AUTO;
        }
        task_state = T_GETDATA;
        break;
      case T_GETDATA: //������ �������� ������ � �������
        if (DEBUG_DRV) printf_dos("TDRV: task state T_GETDATA\n");
        if (!device_state.ndata)        //���� ��� ������ �� ��������, ������������ ������
        {
            if (DEBUG_DRV) printf_dos("T_GETDATA: request system data\n");
            vSendCommandToDrv(GetSystemData, &packet_tx);
            task_state_prev = task_state;
            task_state = T_WAIT_RESPONSE;
            rsp_tries = 0;
        }
        else task_state = T_PROCESS;    //���� ������ -> ������� � �������� ����
        break;
      case T_PROCESS:   //�������� ����
        if (DEBUG_DRV) printf_dos("TDRV: task state T_PROCESS\n");
        stop_curr = cur_send_st.stop;   //����� "����"
        if (stop_curr != stop_prev)
        {
            if (stop_curr == I_ON)           //"����" ���������
            {
                if (DEBUG_DRV) printf_dos("T_PROCESS: STOP state ON\n");
                drv_config.state.prev = drv_config.state.current;
                drv_config.state.current = DRV_MANUAL;
                
                vSendCommandToDrv(SetManCtrl, &packet_tx);
            }
            else                                            //"����" ����������
            {
                if (DEBUG_DRV) printf_dos("T_PROCESS: STOP state OFF\n");
                drv_config.state.current = drv_config.state.prev;
                
                vSendCommandToDrv(SetAutoCtrl, &packet_tx);
                mov_tries = 0;
            }
            stop_prev = stop_curr;
        }
        else if (drv_config.state.current != DRV_MANUAL)       //"����" - ��������� ���������
        {
            fire_curr = cur_send_st.fire;                           //����� "�������"
            if (fire_curr != fire_prev)
            {
                if (fire_curr == I_ON)                              //"�������" ���������
                {
                    if (DEBUG_DRV) printf_dos("T_PROCESS: FIRE state ON\n");
                    drv_config.state.prev = drv_config.state.current;
                    drv_config.state.current = DRV_FIRE;
                    
                    
                    switch (device_config.reaction_fire_input)
                    {
                        case 1:
                          //������� �����
                          vSendCommandToDrv(SetDoorOpen, &packet_tx);
                          last_sent_cmd = SetDoorOpen;
                          break;
                       case 2:
                          //������� �����
                          vSendCommandToDrv(SetDoorClose, &packet_tx);
                          last_sent_cmd = SetDoorClose;
                          break;
                      case 3:
                          //������� �����
                          vSendCommandToDrv(SetDoorClose, &packet_tx);
                          last_sent_cmd = SetDoorClose;
                          break;
                    }
                }
                else 
                {
                    if (DEBUG_DRV) printf_dos("T_PROCESS: FIRE state OFF\n");
                    switch (device_config.reaction_fire_input)      //"�������" ����������
                    {
                        case 1:
                            vTaskDelay(device_config.delay_open_fire*1000);
                        case 2:
                        case 3:
                            drv_config.state.current = drv_config.state.prev;
                            break;
                    }
                }
                fire_prev = fire_curr;
            }
            else if (drv_config.state.current != DRV_FIRE)
            {
                xQueueReceive(xInSensors, &pdu_curr, 0);
                if (pdu_curr != pdu_prev)                               //����� "�����"
                {
                    drv_config.state.prev = drv_config.state.current;
                    switch (pdu_curr)
                    {
                        case I_CLOSE:                                   //������� �����
                          if (DEBUG_DRV) printf_dos("T_PROCESS: PDU state CLOSE\n");
                          drv_config.state.current = DRV_STOP;
                          
                          vSendCommandToDrv(SetDoorClose, &packet_tx);
                          last_sent_cmd = SetDoorClose;
                          break;
                        case I_OPEN:                                    //������� �����
                          if (DEBUG_DRV) printf_dos("T_PROCESS: PDU state OPEN\n");
                          drv_config.state.current = DRV_STOP;
                          
                          if (pdu_prev == I_WINTER)                           //����� �� ������� ������
                          {
                              if (DEBUG_DRV) printf_dos("T_PROCESS: PDU previous state WINTER\n");
                              
                              if (drv_config.position_close < drv_config.position_open) 
                                  drv_config.position_open = drv_config.position_open + ((1-drv_config.open_part_winter)*dpos);
                              else drv_config.position_open = drv_config.position_open - ((1-drv_config.open_part_winter)*dpos);
                              
                              vSendCommandToDrv(ResetWintCtrl, &packet_tx);
                          }
                          vSendCommandToDrv(SetDoorOpen, &packet_tx);
                          last_sent_cmd = SetDoorOpen;
                          break;
                        case I_EXIT:                                    //������ �� �����
                         if (DEBUG_DRV)  printf_dos("T_PROCESS: PDU state EXIT\n");
                          drv_config.state.current = DRV_EXIT;
                          
                          break;
                        case I_WINTER:                                  //������ ����� - �������� ����������
                          if (DEBUG_DRV) printf_dos("T_PROCESS: PDU state WINTER\n");
                          drv_config.state.current = DRV_WINTER;
                          
                          if (drv_config.position_close < drv_config.position_open) 
                            drv_config.position_open = drv_config.position_open + ((1-drv_config.open_part_winter)*dpos);
                          else drv_config.position_open = drv_config.position_open - ((1-drv_config.open_part_winter)*dpos);
                          
                          vSendCommandToDrv(SetWintCtrl, &packet_tx);
                          break;
                        case I_AUTO:                                    //�������������� �����
                          if (DEBUG_DRV) printf_dos("T_PROCESS: PDU state AUTO\n");
                          drv_config.state.current = DRV_AUTO;
                          
                          vSendCommandToDrv(SetAutoCtrl, &packet_tx);
                          break;
                        default:
                          break;
                    }
                    if (pdu_prev == I_WINTER)                           //����� �� ������� ������
                    {
                        if (DEBUG_DRV) printf_dos("T_PROCESS: PDU previous state WINTER\n");
                        
                        if (drv_config.position_close < drv_config.position_open) 
                            drv_config.position_open = drv_config.position_open + ((1-drv_config.open_part_winter)*dpos);
                        else drv_config.position_open = drv_config.position_open - ((1-drv_config.open_part_winter)*dpos);
                        
                        vSendCommandToDrv(ResetWintCtrl, &packet_tx);
                    }
                    pdu_prev = pdu_curr;
                }
                test_curr = cur_send_st.test;
                if (test_curr != test_prev)     //����������
                {
                    if (test_curr == I_ON)
                    {
                        if (DEBUG_DRV) printf_dos("T_PROCESS: TEST state ON\n");
                        
                        drv_config.calibration = false;
                        drv_config.mechpart_config = false;
                        clb_tries = 0;
                        task_state = T_CALIBRATE;
                        break;
                    }
                    test_prev = test_curr;
                }
            }
        }
        //����� ��������/������� - ������ � �������������� �������
        if (drv_config.state.current == DRV_EXIT)
        {
             if ((cur_send_st.bar1 == I_ON      ||           //������ �������� � ����� �������/�����������
                  cur_send_st.bar2 == I_ON      ||
                  cur_send_st.rdr1 == I_ON))    
              {
                  if (DEBUG_DRV) printf_dos("T_PROCESS: open the door in EXIT mode\n");
                  vSendCommandToDrv(SetDoorOpen, &packet_tx);
                  if (device_config.delay_open > 0) xTimeMv = xTaskGetTickCount() + (dpos/drv_config.nom_vel) + device_config.delay_open*1000;
                  mov_tries = 0;
              }
              else if (cur_send_st.bar1 == I_OFF &&           //������ �������� � ����� �������
                       cur_send_st.bar2 == I_OFF &&
                       cur_send_st.rdr1 == I_OFF &&
                       door_current.state == DOOR_OPEN   &&
                       xTaskGetTickCount() > xTimeMv)
              {
                  if (DEBUG_DRV) printf_dos("T_PROCESS: close the door in EXIT mode\n");
                  vSendCommandToDrv(SetDoorClose, &packet_tx);
              }
        }
        else if (drv_config.state.current == DRV_WINTER)
        {
            if ((cur_send_st.bar1 == I_ON     ||            //����� ������ - ����� ���� ��������
                cur_send_st.rdr1 == I_ON      ||
                cur_send_st.bar2 == I_ON      ||
                cur_send_st.rdr2 == I_ON))
            {
                if (DEBUG_DRV) printf_dos("T_PROCESS: open the door in WINTER mode\n");
                vSendCommandToDrv(SetDoorOpen, &packet_tx);
                if (device_config.delay_open_winter > 0) xTimeMv = xTaskGetTickCount() + (dpos/drv_config.nom_vel) + device_config.delay_open_winter*1000;
                mov_tries = 0;
            }
            else if (cur_send_st.bar1 == I_OFF &&
                     cur_send_st.rdr1 == I_OFF &&
                     cur_send_st.bar2 == I_OFF &&
                     cur_send_st.rdr2 == I_OFF &&
                     door_current.state == DOOR_OPEN &&
                     xTaskGetTickCount() > xTimeMv)
            {
                if (DEBUG_DRV) printf_dos("T_PROCESS: close the door in WINTER mode\n");
                vSendCommandToDrv(SetDoorClose, &packet_tx);
            }
        }  
        else if (drv_config.state.current == DRV_AUTO)
        {
            if ((cur_send_st.bar1 == I_ON     ||            //����� ������/���� - ����� ���� ��������
                cur_send_st.rdr1 == I_ON      ||
                cur_send_st.bar2 == I_ON      ||
                cur_send_st.rdr2 == I_ON))
            {
                if (DEBUG_DRV) printf_dos("T_PROCESS: open the door in AUTO mode\n");
                vSendCommandToDrv(SetDoorOpen, &packet_tx);
                if (device_config.delay_open > 0) xTimeMv = xTaskGetTickCount() + (dpos/drv_config.nom_vel) + device_config.delay_open*1000;
                mov_tries = 0;
            }
            else if (cur_send_st.bar1 == I_OFF &&
                     cur_send_st.rdr1 == I_OFF &&
                     cur_send_st.bar2 == I_OFF &&
                     cur_send_st.rdr2 == I_OFF &&
                     door_current.state == DOOR_OPEN &&
                     xTaskGetTickCount() > xTimeMv)
            {
                if (DEBUG_DRV) printf_dos("T_PROCESS: close the door in AUTO mode\n");
                vSendCommandToDrv(SetDoorClose, &packet_tx);
            }
        }
        
        //����� ��������� �����
        if (abs(device_state.epos) > device_config.max_position_error)
        {
            if (DEBUG_DRV) printf_dos("T_PROCESS: door is mooving\n");
            //����� � ��������
            if (device_state.upos == drv_config.position_close) door_current.state = DOOR_CLOSE_MOVE;
            else door_current.state = DOOR_OPEN_MOVE;
            
            //��������� ������ ����������� ������ 
            err_sum += device_state.epos;
            err_div += 1;
            if (abs(err_sum/err_div) > dpos) 
            {
                drv_config.state.prev = drv_config.state.current;
                drv_config.state.current = DRV_MANUAL;
                
                vSendCommandToDrv(SetManCtrl, &packet_tx);
                msg = M_DOOR_POS;
                xQueueSendToBack(xLog, &msg, portMAX_DELAY);
            }
            
            //�������� �� ��������� ���������������
            if (drv_config.c1*device_state.pos + 
                drv_config.c2*device_state.vel + 
                drv_config.c3*device_state.acc > drv_config.c4)
            {
                drv_config.calibration = false;
                drv_config.mechpart_config = false;
                clb_tries = 0;
                task_state = T_CALIBRATE;
                break;
            }
        }
        else
        {
            if (DEBUG_DRV) printf_dos("T_PROCESS: door is stopped\n");
            //����� � �������� ��������� - �������/������
            if (device_state.upos == drv_config.position_close) door_current.state = DOOR_CLOSE;
            else door_current.state = DOOR_OPEN;
            
            err_sum = 0;
            err_div = 1;
        }
        device_state.ndata = false;
        
        // ���������� ��� ������ 
        while (uxQueueMessagesWaiting(xPakRouterToTDrv))
        {
            res = xQueueReceive(xPakRouterToTDrv, &packet_rx, portMAX_DELAY); 
            if (res == pdPASS)
            {
                if (packet_rx.adr_src == ADR_DRV)
                {
                    switch (packet_rx.type)
                    {
                        case GetErrorData:     //��������� ������ �� ��������
                            memcpy(&msg, packet_rx.data, 1);
                            switch (msg)
                            {
                                case M_DRV_NO_DATA:     // � ������ �������� ��� ���������� ������
                                    memcpy(&packet_tx.data, &drv_config.motor_i_max, 21);
                                    vSendCommandToDrv(SetElectData, &packet_tx);
                                    break;
                                case M_CALIBRATE_NO:    // ���������� �� ���������
                                    drv_config.calibration = false;
                                    drv_config.mechpart_config = false;
                                    clb_tries = 0;
                                    task_state = T_CALIBRATE;
                                    break;
                                case M_MOT_OVERCUR:     // ���������� �� ���� - �����������
                                    if (mov_tries < 10)
                                    {
                                        mov_tries++;
                                        vSendCommandToDrv(SetAutoCtrl, &packet_tx);
                                        vTaskDelay(5);
                                        vSendCommandToDrv(SetDoorOpen, &packet_tx);
                                        vTaskDelay(10 + (dpos/drv_config.nom_vel));
                                        vSendCommandToDrv(last_sent_cmd, &packet_tx);
                                    }
                                    break;
                                case M_DRV_PERIPH:
                                case M_IN_VOL_LOW:
                                case M_IN_VOL_NEG:
                                case M_ACC_VOL_NEG:
                                case M_CALIBRATE_ERR:
                                    drv_config.state.current = DRV_ERROR;    //FATAL!!!
                                    vSendCommandToDrv(SetManCtrl, &packet_tx);
                                    break;
                                default:    
                                    if (DEBUG_DRV) printf_dos("T_PROCESS: get Error %d\n", msg);
                                    break;
                            }
                            xQueueSendToBack(xLog, &msg, portMAX_DELAY);
                            break;
                        default:
                            break;
                    }
                }
                else if (packet_rx.adr_src == ADR_PC)
                {
                    switch (packet_rx.type)
                    {
                        case SetElectData:  // ������� ��������� ������������� ����� ������� �� ��
                            uint16_t   u1 = 0, u2 = 0, u3 = 0, u4 = 0, u5 = 0, u6 = 0, u7 = 0, u8 = 0;
    
                            memcpy(&u1, &packet_rx.data[0], 2);
                            memcpy(&u2, &packet_rx.data[2], 2);
                            memcpy(&u3, &packet_rx.data[4], 2);
                            memcpy(&u4, &packet_rx.data[6], 2);
                            memcpy(&u5, &packet_rx.data[8], 2);
                            memcpy(&u6, &packet_rx.data[10], 2);
                            memcpy(&u7, &packet_rx.data[12], 2);
                            if (u1 != device_config.motor_i_max            ||
                                u2 != device_config.motor_v_max            ||
                                u3 != device_config.speed_open             ||
                                u4 != device_config.speed_open_winter      ||
                                u5 != device_config.open_width_in_winter   ||
                                u6 != device_config.end_position_inversion ||
                                u7 != device_config.max_position_error)
                            {
                                device_config.motor_i_max = u1;
                                device_config.motor_v_max = u2;
                                device_config.speed_open = u3;
                                device_config.speed_open_winter = u4;
                                device_config.open_width_in_winter = u5;
                                device_config.end_position_inversion = u6;
                                device_config.max_position_error = u7;
                                vCalcDrvParams();
                                memcpy(&packet_tx.data, &drv_config.motor_i_max, 21);
                                vSendCommandToDrv(SetElectData, &packet_tx);
                            }
                            break;
                        case GetElectData:
                            packet_tx.adr_dst = ADR_PC;
                            packet_tx.type = GetElectData;
                            memcpy(&device_config.motor_i_max, &packet_tx.data[0], 2);
                            memcpy(&device_config.motor_v_max, &packet_tx.data[2], 2);
                            memcpy(&device_config.speed_open, &packet_tx.data[4], 2);
                            memcpy(&device_config.speed_open_winter, &packet_tx.data[6], 2);
                            memcpy(&device_config.open_width_in_winter, &packet_tx.data[8], 2);
                            memcpy(&device_config.end_position_inversion, &packet_tx.data[10], 2);
                            memcpy(&device_config.max_position_error, &packet_tx.data[12], 2);
                            memcpy(&drv_config.nom_vel, &packet_tx.data[14], 2);
                            memcpy(&drv_config.position_close, &packet_tx.data[16], 4);
                            memcpy(&drv_config.position_open, &packet_tx.data[20], 4);
                            xQueueSendToBack(xPakTDrvToRouter, &packet_tx, portMAX_DELAY);
                            break;
                        case SetMechData:
                            memcpy(&u1, &packet_rx.data[0], 2);
                            memcpy(&u2, &packet_rx.data[2], 2);
                            memcpy(&u3, &packet_rx.data[4], 2);
                            memcpy(&u4, &packet_rx.data[6], 2);
                            memcpy(&u5, &packet_rx.data[8], 2);
                            memcpy(&u6, &packet_rx.data[10], 2);
                            memcpy(&u7, &packet_rx.data[12], 2);
                            memcpy(&u7, &packet_rx.data[14], 2);
                            if (u1 != device_config.delay_open             ||
                                u2 != device_config.delay_open_winter      ||
                                u3 != device_config.delay_open_fire        ||
                                u4 != device_config.lock_pos               ||
                                u5 != device_config.reaction_fire_input    ||
                                u6 != device_config.batt_job_n             ||
                                u7 != device_config.lock_type_num          ||
                                u8 != device_config.motor_stop)
                            {
                                device_config.delay_open = u1;
                                device_config.delay_open_winter = u2;
                                device_config.speed_open = u3;
                                device_config.lock_pos = u4;
                                device_config.reaction_fire_input = u5;
                                device_config.batt_job_n = u6;
                                device_config.lock_type_num = u7;
                                device_config.motor_stop = u8;
                            }
                            break;
                        case GetMechData:
                            packet_tx.adr_dst = ADR_PC;
                            packet_tx.type = GetMechData;
                            memcpy(&device_config.delay_open, &packet_tx.data[0], 2);
                            memcpy(&device_config.delay_open_winter, &packet_tx.data[2], 2);
                            memcpy(&device_config.delay_open_fire, &packet_tx.data[4], 2);
                            memcpy(&device_config.lock_pos, &packet_tx.data[6], 2);
                            memcpy(&device_config.reaction_fire_input, &packet_tx.data[8], 2);
                            memcpy(&device_config.batt_job_n, &packet_tx.data[10], 2);
                            memcpy(&device_config.lock_type_num, &packet_tx.data[12], 2);
                            memcpy(&device_config.motor_stop, &packet_tx.data[14], 2);
                            xQueueSendToBack(xPakTDrvToRouter, &packet_tx, portMAX_DELAY);
                            break;
                        case SetDiagData:  // ������� ������ ��������������� ������������� �� ��
                            memcpy(&drv_config.c1, &packet_rx.data[0], 16);
                            break;
                        case GetDiagData:  // ������� ������ ��������������� ������������� �� ��
                            packet_tx.adr_dst = ADR_PC;
                            memcpy(&packet_tx.data[0], &drv_config.c1, 16);
                            xQueueSendToBack(xPakTDrvToRouter, &packet_tx, portMAX_DELAY);
                            break;
                        case GetErrorData:     //��������� ������ �� ��������
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        //�������� ������ �� �� ��� � 100 ��
        if (xTimeTx <= xTaskGetTickCount()) task_state = T_SENDLOG;
        else task_state = T_GETDATA;
        break;
     case T_SENDLOG:   //�������� ������ �� ��
       if (DEBUG_DRV) printf_dos("TDRV: task state T_SENDLOG\n");
       packet_tx.adr_dst = ADR_PC;
       packet_tx.type = GetSystemData;
       memcpy(packet_tx.data, &device_state.current, 24);
       res = xQueueSendToBack( xPakTDrvToRouter, &packet_tx, portMAX_DELAY);
        if (res != pdPASS){
          if (DEBUG_DRV) printf_dos("ERROR: TDRV: xQueueSendToBack( xPakTDrvToRouter, &packet_tx, 0)\n");
        }else{
          if (DEBUG_DRV) printf_dos("TDRV: packet_tx to DRV OK\n");
          xTimeTx = xTaskGetTickCount() + 100;
        }
        task_state = T_GETDATA;
       break;
    default:    
       break;
  }
  vTaskDelay(10);
    

   
/*
    res = xQueueReceive(xPakRouterToTDrv, &packet_rx, 0);
    if (res == pdPASS){
        printf_dos("TASK DRV:������� �����:\n");
    }

    res = xQueueSendToBack(xPakTDrvToRouter, &packet_tx, 5);
    if (res != pdPASS) printf_dos("TASK DRV: error - xQueueSendToBack( packet_from_tdrv.q, packet_from_tdrv.buf\n");
*/

    // ������� ������, ���� ����
/*    er = ER_DRV;
    res = xQueueSendToBack( xLog, &er, 0);
    if (res != pdPASS) printf_dos("TASK DRV: error - xQueueSendToBack( xLog, er)\n");

// ������������ ������ ������
    door_current.state = DOOR_CLOSE;   // �������
    door_current.pos   = 100;          // 100 % - �.�. ���������
    vTaskDelay(3000);


    door_current.state = DOOR_OPEN;        // �������
    door_current.counter_open++;           // ������� �������� +1
    vTaskDelay(3000);

    door_current.state = DOOR_CLOSE;       // �������
    door_current.counter_close++;          // ������� �������� +1
    vTaskDelay(3000);
*/

WHILE_END

}

