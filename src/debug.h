#ifndef DEBUG_LIB_H_
#define DEBUG_LIB_H_

//#include <sys/time.h>

void dump_out(const char *buf_in, unsigned long len);
void hex_out(const char *buf_in, unsigned long len);
void print_hex(const char *in, unsigned short len);
void dump_queue(char * buf, unsigned int in, unsigned int out, const unsigned int size );

#endif /*DEBUG_LIB_H_*/
