#ifndef FLASH_745_H_
#define FLASH_745_H_

int flash_unlock(void);
int flash_lock(void);
int flash_erase_sector(uint8_t sector_num);
void flash_write_word(uint32_t adr, uint32_t data);

#endif