#ifndef TIM_6_7_H_
#define TIM_6_7_H_


typedef struct {
    uint16_t cen:1;
    uint16_t udis:1;
    uint16_t urs:1;
    uint16_t opm:1;
    uint16_t RES0:3;
    uint16_t arpe:1;
    uint16_t RES1:3;
    uint16_t uifremap:1;
    uint16_t RES2:4;
} tim_6_7_cr1_reg_t;

typedef union
{
    tim_6_7_cr1_reg_t reg;
    uint16_t data;
} tim_6_7_cr1_reg_ut;
//------------------------------------------------------------

#endif