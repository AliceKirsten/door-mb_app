#ifndef FW_SEND_TO_DRV_H_
#define FW_SEND_TO_DRV_H_

#include "os.h"

result_t fw_send_to_drv(packet_adr_t adr_dst, packet_adr_t adr_src, 
                   xQueueHandle *qtx, xQueueHandle *qrx, 
                   uint8_t *fw_array, uint32_t fw_size);

#endif
