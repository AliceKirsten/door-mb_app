/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "queue.h"
#include "semphr.h"
#include "event_groups.h"

#include "main.h"
#include "printf_hal.h"
#include "msg_types.h"
#include "task_hardpost.h"
#include "task_lcdref.h"
#include "get_packet_from_osq.h"
#include "keyboard.h"

#define IN_SENDSOR_FIFO_SIZE    (8)

#define PAK_ROUTER_TO_TPOST_SIZE (4)
#define PAK_TPOST_TO_ROUTER_SIZE (24)

#define PAK_ROUTER_TO_TDRV_SIZE (4)
#define PAK_TDRV_TO_ROUTER_SIZE (4)

#define PAK_ROUTER_TO_TLOG_SIZE (4)
#define PAK_TLOG_TO_ROUTER_SIZE (4)

#define LOG_QUEUE_SIZE          (32)

#define KEYBOARD_FIFO_BUF_SIZE  (8)  // �������� ������ ����������

xSemaphoreHandle xUartConsolOSsemHandle;  // ������� �������
xSemaphoreHandle xI2COSSemHandle;         // ������� ���� I2C
xQueueHandle xPakRouterToTDrv;             // ������� ������ Router -> TASK Drv
xQueueHandle xPakTDrvToRouter;             // ������� ������ TASK Drv -> Router

xQueueHandle xPakRouterToTLog;             // ������� ������ Router -> TASK Log
xQueueHandle xPakTLogToRouter;             // ������� ������ TASK Log -> Router

xQueueHandle xPakRouterToTPost;            // ������� ������ Router -> TASK Post
xQueueHandle xPakTPostToRouter;            // ������� ������ TASK Post -> Router

xQueueHandle xLog;                         // ������� ������ ��� ������ Task Log (��� ������ ����������� � ������ �������)
xQueueHandle xKey;                         // ������� �� ������ Task Keyboard - ���������� ������� ������
xQueueHandle xInSensors;                   // ������� ��������� ����������� �������������

extern packet_build_t packet_from_tdrv;
extern packet_build_t packet_from_tlog;
extern packet_build_t packet_from_tpost;

extern void vTaskHardPost(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) 
{
    portBASE_TYPE xReturn;

    vSemaphoreCreateBinary(xUartConsolOSsemHandle);
    vSemaphoreCreateBinary(xI2COSSemHandle);

    xInSensors = xQueueCreate(IN_SENDSOR_FIFO_SIZE, sizeof(pdu_state_e));
    if (xInSensors == 0) PRINT_FATAL_ERROR("HARD POST: xQueueCreate-xInSensors\n");
    
    xLog = xQueueCreate(LOG_QUEUE_SIZE, sizeof(msg_t));
    if (xLog == 0) PRINT_FATAL_ERROR("HARD POST: xQueueCreate-xLog\n");
    
    xPakRouterToTPost = xQueueCreate(PAK_ROUTER_TO_TPOST_SIZE, sizeof(packet_struct_t));
    if (xPakRouterToTPost == 0) PRINT_FATAL_ERROR("HARD POST: xQueueCreate-xPakRouterToTPost");

    xPakTPostToRouter = xQueueCreate(PAK_TPOST_TO_ROUTER_SIZE, sizeof(packet_struct_t));
    if (xPakTPostToRouter == 0) PRINT_FATAL_ERROR("HARD POST: xQueueCreate-xPakTPostToRouter");
    
    packet_from_tpost.q = xPakTPostToRouter;
    
    xPakRouterToTDrv = xQueueCreate(PAK_ROUTER_TO_TDRV_SIZE, sizeof(packet_struct_t));
    if (xPakRouterToTDrv == 0) PRINT_FATAL_ERROR("HARD POST: xQueueCreate-xPakRouterToTDrv");

    xPakTDrvToRouter = xQueueCreate(PAK_TDRV_TO_ROUTER_SIZE, sizeof(packet_struct_t));
    if (xPakTDrvToRouter == 0) PRINT_FATAL_ERROR("HARD POST: xQueueCreate-xPakTDrvToRouter");
    
    packet_from_tdrv.q = xPakTDrvToRouter;

    xPakRouterToTLog = xQueueCreate(PAK_ROUTER_TO_TLOG_SIZE, sizeof(packet_struct_t));
    if (xPakRouterToTLog == 0) PRINT_FATAL_ERROR("HARD POST: xQueueCreate-xPakRouterToTLog");

    xPakTLogToRouter = xQueueCreate(PAK_TLOG_TO_ROUTER_SIZE, sizeof(packet_struct_t));
    if (xPakTLogToRouter == 0) PRINT_FATAL_ERROR("HARD POST: xQueueCreate-xPakTLogToRouter");

    xKey = xQueueCreate(KEYBOARD_FIFO_BUF_SIZE, sizeof(uint8_t));
    if (xPakTLogToRouter == 0) PRINT_FATAL_ERROR("HARD POST: xQueueCreate-xKey");
    
    packet_from_tlog.q = xPakTLogToRouter;
    
    xReturn = xTaskCreate( (TaskFunction_t)vTaskLcdRef, 
                          "LCD_REF",
                          configMINIMAL_STACK_SIZE,
                          NULL,
                          TASK_LCDREF_PRIORITY,
                          NULL );
    
    if( xReturn != pdPASS ){
    	PRINT_FATAL_ERROR("xTaskCreate LCD REFRESH.");
    }

    xReturn = xTaskCreate( (TaskFunction_t)vTaskHardPost, 
                          "HardPost",
                          configMINIMAL_STACK_SIZE,
                          NULL,
                          TASK_HARDPOST_PRIORITY,
                          NULL );
    
    if( xReturn != pdPASS ){
    	PRINT_FATAL_ERROR("xTaskCreate HARD POST.");
    }
    
    
    
    


}

