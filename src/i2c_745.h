// I2C
#ifndef I2C_H_
#define I2C_H_
#include <stdint.h>

typedef struct {
    uint32_t pe:1;
    uint32_t txie:1;
    uint32_t rxie:1;
    uint32_t addrie:1;
    uint32_t nackie:1;
    uint32_t stopie:1;
    uint32_t tcie:1;
    uint32_t errie:1;
    uint32_t dnf:4;
    uint32_t anfoff:1;
    uint32_t RES0:1;
    uint32_t txdmaen:1;
    uint32_t rxdmaen:1;
    uint32_t sbc:1;
    uint32_t nostretch:1;
    uint32_t RES1:1;
    uint32_t gcen:1;
    uint32_t smbhen:1;
    uint32_t smbden:1;
    uint32_t alerten:1;
    uint32_t pecen:1;
    uint32_t RES2:8;
} i2c_cr1_reg_t;

typedef union {
    uint32_t data;
    i2c_cr1_reg_t reg;
} i2c_cr1_reg_ut;




typedef struct {
    uint32_t sadd:10;
    uint32_t rd_wrn:1;
    uint32_t add10:1;
    uint32_t head10r:1;
    uint32_t start:1;
    uint32_t stop:1;
    uint32_t nack:1;
    uint32_t nbytes:8;
    uint32_t reload:1;
    uint32_t autoend:1;
    uint32_t pecbyte:1;
    uint32_t RES0:5;
} i2c_cr2_reg_t;

typedef union {
    uint32_t data;
    i2c_cr2_reg_t reg;
} i2c_cr2_reg_ut;




typedef struct {
    uint32_t oa1:10;
    uint32_t oa1mode:1;
    uint32_t RES0:4;
    uint32_t oa1en:1;
    uint32_t RES1:16;
} i2c_oar1_reg_t;

typedef union {
    uint32_t data;
    i2c_oar1_reg_t reg;
} i2c_oar1_reg_ut;



typedef struct {
    uint32_t RES0:1;
    uint32_t oa2:7;
    uint32_t oa2msk:3;
    uint32_t RES1:4;
    uint32_t oa2en:1;
    uint32_t RES2:16;
} i2c_oar2_reg_t;

typedef union {
    uint32_t data;
    i2c_oar2_reg_t reg;
} i2c_oar2_reg_ut;


typedef struct {
    uint32_t scll:8;
    uint32_t sclh:8;
    uint32_t sdadel:4;
    uint32_t scldel:4;
    uint32_t RES0:4;
    uint32_t presc:4;
} i2c_timingr_reg_t;

typedef union {
    uint32_t data;
    i2c_timingr_reg_t reg;
} i2c_timingr_reg_ut;





typedef struct {
    uint32_t timeouta:12;
    uint32_t tidle:1;
    uint32_t RES0:2;
    uint32_t timouten:1;
    uint32_t timeoutb:12;
    uint32_t RES1:3;
    uint32_t texten:1;
} i2c_timeoutr_reg_t;

typedef union {
    uint32_t data;
    i2c_timeoutr_reg_t reg;
} i2c_timeoutr_reg_ut;

void i2c_init(void);
int i2c_write( uint8_t i2c_adr_8bit, uint8_t *buf, uint8_t buf_len);
int i2c_write_adr_16bit( uint8_t i2c_adr_8bit, uint16_t adr, uint8_t *buf, uint8_t buf_len);
int i2c_read_adr_16bit(uint8_t i2c_adr_8bit, uint16_t adr_start, uint8_t *buf, uint8_t buf_len);
int i2c_read_adr_8bit(uint8_t i2c_adr_8bit, uint8_t adr_start, uint8_t *buf, uint8_t buf_len);

#endif
