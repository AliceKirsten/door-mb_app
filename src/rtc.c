#include <time.h>
#include <stdint.h>
#include "m41t62.h"
#include "printf_hal.h"

//------------------------------------------------------------------------------
// �������� ������ ����� ���� ��������� �������� ����� �� ���. 1-1-2019 00:00
//------------------------------------------------------------------------------
int rtc_reset(void)
{
    int res;
    struct tm t;
    int er = 0;

    res = m41t62_get( &t );
    if (res){
        printf_d("ERROR[%s %d]:m41t62_get()\n", __FUNCTION__, __LINE__);
        return res;
    }
  
    if (t.tm_hour > 23) er |= 1;
    if (t.tm_min  > 59) er |= 1;
    if (t.tm_sec  > 59) er |= 1;
    if (t.tm_mday > 31) er |= 1;
    if (t.tm_mon  > 12) er |= 1;
    if (t.tm_year < 2019) er |= 1;
    
    if (er == 0) return 0;

    res = m41t62_reset();
    if (res){
        printf_d("ERROR[%s %d]:m41t62_reset()\n", __FUNCTION__, __LINE__);
        return res;
    }
    
    // ���������� ����� �� ���������
    t.tm_sec  = 0;
    t.tm_min  = 0;
    t.tm_hour = 0;
    t.tm_mday = 1;
    t.tm_wday = 2;
    t.tm_mon  = 1;
    t.tm_year = 2019;
    t.tm_yday = 0; // days since January 1 - [ 0 to 365 ]
    t.tm_isdst = -1;
    
    res = m41t62_set( &t );
    if (res){
        printf_d("ERROR[%s %d]:m41t62_set()\n", __FUNCTION__, __LINE__);
        return res;
    }
    return 0;
}

//------------------------------------------------------------------------------
// ������ �������� ������� �� RTC � �������������� ������� � 
//  32 ������ �������� �������� ������ � ������ �����
//------------------------------------------------------------------------------
int rtc_get_time32( uint32_t * t )
{
    int res;
    struct tm rtc_tm;
    
    res = m41t62_get( &rtc_tm );
    if (res){
        printf_d("ERROR[%s %d]: m41t62_get\n", __FUNCTION__, __LINE__);
        return res;
    }
    
    printf_d("\nGet current DATE: %4d-%02d-%02d (wday=%d)  TIME: %2d:%02d:%02d\n",
	      rtc_tm.tm_year, rtc_tm.tm_mon, rtc_tm.tm_mday, rtc_tm.tm_wday, rtc_tm.tm_hour, rtc_tm.tm_min, rtc_tm.tm_sec);
    
    rtc_tm.tm_year = rtc_tm.tm_year - 1900; 
    
    *t = mktime( &rtc_tm );

    return 0;
}

//------------------------------------------------------------------------------
// ������ ������� ������ � ������ ����� � RTC
//------------------------------------------------------------------------------
int rtc_set_time32( uint32_t t )
{
    int res;
    //struct tm rtc_tm;
    struct tm *tm;
    
    tm = localtime( (const time_t*)&t );
    tm->tm_year += 1900;

    res = m41t62_set( tm );
    if (res){
        printf_d("ERROR[%s %d]: m41t62_set\n", __FUNCTION__, __LINE__);
        return res;
    }

    return 0;
}
