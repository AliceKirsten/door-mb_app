/*
 */
#include "main.h"
#include "rcc_745.h"
#include "gpio_745.h"
#include "i2c_745.h"
#include "time_hal.h"

#ifndef DEBUG
#define DEBUG 0
#endif

//==============================================================================
// read byte MAC addres from MICROCHIP EEPROM 128 byte + MAC (IC=24AA02E48)
// ��������� ���� ��� ������� � �����
//==============================================================================
int mac_read(unsigned char *mac)
{
    int res;

    rcc_clk_en(&RCC->AHB1ENR, RCC_AHB1ENR_GPIOBEN);      // GPIOB CLK EN

    gpio_init(I2C_MAC_PWR_H_PORT, I2C_MAC_PWR_H, GPIO_MODE_OUT, GPIO_OTYPE_PUSHPULL, GPIO_SPEEDF_VERYHIGH, GPIO_PULL_UP, 0);
    
    gpio_set_1(I2C_MAC_PWR_H_PORT, I2C_MAC_PWR_H);      // ������ ������� �� ����������
    delay_ms(10);                                       // ������������ ������� �� ���������� U20

    res = i2c_read_adr_8bit(U20_I2C_ADR, 0xfa, mac, 6); // ������ MAC 6 ����

    gpio_set_0(I2C_MAC_PWR_H_PORT, I2C_MAC_PWR_H);      // ��������� ������� �� ���������� U20
    delay_ms(10);                                       // ������� �������� �������
    
    return res;
}
