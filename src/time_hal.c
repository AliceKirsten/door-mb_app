#include <stdint.h>
#include "time_hal.h"
#include "sys_timer.h"
#include "os.h"

#ifndef DEBUG_TIME
#define DEBUG_TIME 0
#endif


//******************************************************************************
// Delay ms
//******************************************************************************
void delay_ms( const uint32_t delay )
{
    uint32_t c;
  
    if (xTaskGetSchedulerState() != taskSCHEDULER_NOT_STARTED){
        vTaskDelay( delay );
    }else{
        c = time_get_ms_counter();
        c = c + delay;
        while(time_get_ms_counter() < c);
    }
}

//******************************************************************************
// Delay sec
//******************************************************************************
void delay_s( const uint32_t delay )
{
    uint32_t c;
    
    if (xTaskGetSchedulerState() != taskSCHEDULER_NOT_STARTED){
        vTaskDelay( delay * 1000 );
    }else{
        c = time_get_ms_counter();
        c = c + (delay * 1000);
        while(time_get_ms_counter() < c);
    }
}


//******************************************************************************
// Skolko vremeni prohlo meshdu dvuma intervalami
//
//******************************************************************************
uint32_t time_delta(uint32_t t1, uint32_t t2)
{
	if (t1 >= t2){
		return t1 - t2;
	}else{
		return (uint32_t)0xffffffffUL - t2 + t1;
	}
}
