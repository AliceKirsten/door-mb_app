#ifndef LOG_H_
#define LOG_H_
#include <stdint.h>
#include "main.h"     // ����� ������
#include "msg_types.h"

// ���������� ���������� ��������
typedef enum { LOG_RET_OK = 0, LOG_RET_ERR } log_return_st;

#define LOG_LAT1_LOCK_ADR    (LAT1_ADR_START + 0)  // ����� ���������� ���������
#define LOG_LAT1_IN_ADR      (LAT1_ADR_START + 1)  // ����� ���������� ���������
#define LOG_LAT1_OUT_ADR     (LAT1_ADR_START + 3)  // ����� ���������� ���������
#define LOG_LAT1_LOCK_SIZE   (1)  // ������ ���������
#define LOG_LAT1_IN_SIZE     (2)  // ������ ���������
#define LOG_LAT1_OUT_SIZE    (2)  // ������ ���������

#define LOG_LAT2_LOCK_ADR    (LAT1_ADR_START + 5)  // ����� ���������� ���������
#define LOG_LAT2_IN_ADR      (LAT1_ADR_START + 6)  // ����� ���������� ���������
#define LOG_LAT2_OUT_ADR     (LAT1_ADR_START + 8)  // ����� ���������� ���������
#define LOG_LAT2_LOCK_SIZE   (1)  // ������ ���������
#define LOG_LAT2_IN_SIZE     (2)  // ������ ���������
#define LOG_LAT2_OUT_SIZE    (2)  // ������ ���������


#define LOG_UNLOCK  (0) // ������� ������� ��� ���������
#define LOG_LOCK    (1) // ������� ������� ��� �� ���� ������� (��������� ����� ������� ...)

// ������� ��������� ���������� �� ���������� ������� � ��������� �������, ����������� � flash ������.
// ���� ��������� � ���, ������ � FLASH. �� ������ ���������� �������
typedef struct {
	uint8_t  lock1;   // ���������� �������
	uint16_t in1;     // ����
	uint16_t out1;    // �����

	uint8_t  lock2;   // ���������� �������
	uint16_t in2;     // ����
	uint16_t out2;    // �����
} log_allocation_table_t;

log_return_st log_write_lat1(log_allocation_table_t * lat);
log_return_st log_write_lat2(log_allocation_table_t * lat);
log_return_st log_get_lat(log_allocation_table_t * lat);
uint16_t log_rec_quantity(log_allocation_table_t * lat);
log_return_st log_get_rec( log_allocation_table_t * lat, log_record_t * log_r);
log_return_st log_set_rec( log_allocation_table_t * lat, log_record_t log_r);

#endif