// FLASH 745 -------------------------------------------------------------------
//
// unlock
// lock
// erase sector
// write uin32_t
//
//
//------------------------------------------------------------------------------
#include <stdint.h>
#include "stm32f745xx.h"


#define FLASH_PSIZE_WORD (2 << FLASH_CR_PSIZE_Pos) // 10b - 32 bita

//------------------------------------------------------------------------------
// ������������� ������� � FLASH.
//------------------------------------------------------------------------------
int flash_unlock(void)
{
    
    if (FLASH->CR & FLASH_CR_LOCK){
        FLASH->KEYR = 0x45670123;
        FLASH->KEYR = 0xCDEF89AB;
        
        if (FLASH->CR & FLASH_CR_LOCK) return 1; // error
        else return 0; // ok
    }
    
    return 0;
}

//------------------------------------------------------------------------------
// ���������� ������� � FLASH.
//------------------------------------------------------------------------------
int flash_lock(void)
{
    FLASH->CR |= FLASH_CR_LOCK;
    return 0;
}

//------------------------------------------------------------------------------
// �������� - 1 �������.
//------------------------------------------------------------------------------
int flash_erase_sector(uint8_t sector_num)
{
    
//To erase a sector, follow the procedure below:
//1. Check that no Flash memory operation is ongoing by checking the BSY bit in the
//FLASH_SR register
    if (FLASH->SR & FLASH_SR_BSY)
        return 1;
    
//2. Set the SER bit and select the sector out of the 8 in the main memory block) wished to
//erase (SNB) in the FLASH_CR register
    FLASH->CR |= FLASH_CR_SER;

    FLASH->CR &= ~(FLASH_CR_SNB);
    FLASH->CR |= (sector_num & 0x0f) << FLASH_CR_SNB_Pos;

//3. Set the STRT bit in the FLASH_CR register
    FLASH->CR |= FLASH_CR_STRT;
    
//4. Wait for the BSY bit to be cleared
    while(FLASH->SR & FLASH_SR_BSY);
    
    return 0;
}


//------------------------------------------------------------------------------
// ������ ����� 32 bit � FLASH.
//------------------------------------------------------------------------------
void flash_write_word(uint32_t adr, uint32_t data)
{
    
    while(FLASH->SR & FLASH_SR_BSY);
    
    /* If the previous operation is completed, proceed to program the new data */
    FLASH->CR &= ~(FLASH_CR_PSIZE_Msk);
    FLASH->CR |= FLASH_PSIZE_WORD;
    FLASH->CR |= FLASH_CR_PG;

    *(__IO uint32_t*)adr = data;
  
    /* Data synchronous Barrier (DSB) Just after the write operation
     This will force the CPU to respect the sequence of instruction (no optimization).*/
    __DSB();
    
    while(FLASH->SR & FLASH_SR_BSY);
}

