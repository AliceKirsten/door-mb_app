#include <stdint.h>
#include "main.h"
#include "stm32f745xx.h"
#include "rcc_745.h"
#include "i2c_745.h"
#include "gpio_745.h"

//==============================================================================
// init I2C
//==============================================================================
void i2c_init(void)
{
    // i2c_1 apb1 54 mhz
    volatile uint32_t reg;
    i2c_timingr_reg_ut reg_timing;
    
 
    rcc_clk_en(&RCC->AHB1ENR, RCC_AHB1ENR_GPIOBEN);  // GPIOB CLK EN
    rcc_clk_en(&RCC->APB1ENR, RCC_APB1ENR_I2C1EN);   // I2C_1 CLK EN

    gpio_init(I2C_SDA_PORT, I2C_SDA,  GPIO_MODE_AF, GPIO_OTYPE_OPENDRAIN, GPIO_SPEEDF_VERYHIGH, GPIO_PULL_UP, GPIO_AF4_I2C1);
    gpio_init(I2C_SCL_PORT, I2C_SCL,  GPIO_MODE_AF, GPIO_OTYPE_OPENDRAIN, GPIO_SPEEDF_VERYHIGH, GPIO_PULL_UP, GPIO_AF4_I2C1);
  
    // clear PE bit in I2C_CR1
    //  PE must be kept low during at least 3 APB clock cycles in order to perform the software 
    //  reset. This is ensured by writing the following software sequence: - Write PE=0 - Check 
    //  PE=0 - Write PE=1.
    I2C1->CR1 &= ~(1); // PE=0
    reg = I2C1->CR1;
    
    // configure ANFOFF and DNF[3:0] in I2C_CR1

    // configure PRESC[3:0]   APB1(54Mhz) / (presc + 1)
    // SDADEL[3:0] SCLDEL[3:0] SCLH[7:0] SCLL[7:0] in I2C_TIMINGR
    reg_timing.reg.presc  = 12; // 4MHz(250ns) = 54MHz / (12+1)
    reg_timing.reg.sdadel = 2;  // 2*250ns
    reg_timing.reg.scldel = 4;  // 5*250ns
    reg_timing.reg.scll   = 19; // (19+1) * 250 ns = 5 us
    reg_timing.reg.sclh   = 15; // (15+1) * 250 ns = 4 us

    I2C1->TIMINGR = reg_timing.data;
    
    // configure NOSTRETCH in I2C_CR1
    //I2C1->OAR1 |= I2C_OAR1_OA1EN | 0xa0;

    // set PE bit in I2C_CR1
    I2C1->CR1 |= 1; // PE=1
}

//==============================================================================
// Write to I2C
//
// i2c_adr_8bit - DEVICE adr 8 bit (7bit << 1)
// *buf         - buffer = memadr + data
// buf_len      - ������ buf
//
// return 0   - OK
// return !=0 - ERROR
//==============================================================================
int i2c_write( uint8_t i2c_adr_8bit, uint8_t *buf, uint8_t buf_len)
{
    
    uint32_t reg;
    
    if  (I2C1->ISR & I2C_ISR_BUSY) return 1; // error bus busy !
    
    I2C1->CR2 = 0;

    I2C1->CR2 &= ~I2C_CR2_RD_WRN; // write = set 0
    I2C1->CR2 |= I2C_CR2_AUTOEND; // �� ���������� ���������� ��������� STOP
    I2C1->CR2 |= i2c_adr_8bit;
    I2C1->CR2 &= ~I2C_CR2_NBYTES_Msk;
    I2C1->CR2 |= (buf_len << I2C_CR2_NBYTES_Pos); // set NBYTES
 
    
    // start + dev adr ---------------------------------------------------------
    I2C1->CR2 |= I2C_CR2_START;
    
    while(1){
        reg = I2C1->ISR;
        
        if (reg & I2C_ISR_ARLO){
            return 2; // This bit is cleared by hardware when PE=0
        }
        if (reg & I2C_ISR_BERR){
            I2C1->ICR = I2C_ICR_BERRCF;
            return 3; // error BERR
        }
        
        if (reg & I2C_ISR_NACKF) {
            I2C1->ICR = I2C_ICR_NACKCF;
            return 4; // error ADR NASK
        }
        
        if (reg & I2C_ISR_TXIS) break;
    }// while

    // send data ---------------------------------------------------------------
_tx_new:
    I2C1->TXDR = *buf;
    buf++;
    
    while(1){
        reg = I2C1->ISR;
        if (reg & I2C_ISR_ARLO){
            return 2; // This bit is cleared by hardware when PE=0
        }
        if (reg & I2C_ISR_BERR){
            I2C1->ICR = I2C_ICR_BERRCF;
            return 3; // error BERR
        }

        if (reg & I2C_ISR_TXIS) goto _tx_new;
        
        if (reg & I2C_ISR_NACKF) {
            I2C1->ICR = I2C_ICR_NACKCF;
            return 5;
        }
        
        if (reg & I2C_ISR_TC) break;
        
        if (reg & I2C_ISR_STOPF){
            I2C1->ICR = I2C_ICR_STOPCF;
            break;
        }
        
    } // while
    

    return 0;
}

//==============================================================================
// Write to I2C - ADD 16 bit ADR
//
// i2c_adr_8bit - DEVICE adr 8 bit (7bit << 1)
// uint16_t adr - adr offset for dev memory (0 + offset)
// *buf         - buffer = memadr + data
// buf_len      - ������ buf
//
// return 0   - OK
// return !=0 - ERROR
//==============================================================================
int i2c_write_adr_16bit( uint8_t i2c_adr_8bit, uint16_t adr, uint8_t *buf, uint8_t buf_len)
{
    
    uint32_t reg;
    uint16_t c = 0; // ������� 
    
    if  (I2C1->ISR & I2C_ISR_BUSY) return 1; // error bus busy !
    
    I2C1->CR2 = 0;

    I2C1->CR2 &= ~I2C_CR2_RD_WRN; // write = set 0
    I2C1->CR2 |= I2C_CR2_AUTOEND; // �� ���������� ���������� ��������� STOP
    I2C1->CR2 |= i2c_adr_8bit;
    I2C1->CR2 &= ~I2C_CR2_NBYTES_Msk;
    I2C1->CR2 |= ((buf_len+2) << I2C_CR2_NBYTES_Pos); // set NBYTES
 
    
    // start + dev adr ---------------------------------------------------------
    I2C1->CR2 |= I2C_CR2_START;
    
    while(1){
        reg = I2C1->ISR;
        
        if (reg & I2C_ISR_ARLO){
            return 2; // This bit is cleared by hardware when PE=0
        }
        if (reg & I2C_ISR_BERR){
            I2C1->ICR = I2C_ICR_BERRCF;
            return 3; // error BERR
        }
        
        if (reg & I2C_ISR_NACKF) {
            I2C1->ICR = I2C_ICR_NACKCF;
            return 4; // error ADR NASK
        }
        
        if (reg & I2C_ISR_TXIS) break;
    }// while

    // send data ---------------------------------------------------------------
_tx_new:
    switch(c){
    case 0:{ // Hi adr offset
        I2C1->TXDR = (adr & 0xff00) >> 8;
        c++;
        break;
    }

    case 1:{ // Lo adr offset
        I2C1->TXDR = (adr & 0x00ff) >> 0;
        c++;
        break;
    }

    default:{ // data buf
        I2C1->TXDR = *buf;
        buf++;
    }
    }//switch

    while(1){
        reg = I2C1->ISR;
        if (reg & I2C_ISR_ARLO){
            return 2; // This bit is cleared by hardware when PE=0
        }
        if (reg & I2C_ISR_BERR){
            I2C1->ICR = I2C_ICR_BERRCF;
            return 3; // error BERR
        }

        if (reg & I2C_ISR_TXIS) goto _tx_new;
        
        if (reg & I2C_ISR_NACKF) {
            I2C1->ICR = I2C_ICR_NACKCF;
            return 5;
        }
        
        if (reg & I2C_ISR_TC) break;
        
        if (reg & I2C_ISR_STOPF){
            I2C1->ICR = I2C_ICR_STOPCF;
            break;
        }
        
    } // while
    

    return 0;
}

//==============================================================================
// Read from I2C
//
// i2c_adr_8bit - DEVICE adr 8 bit (7bit << 1)
// adr_start    - I2C offset adr (8bit)
// *buf         - buffer = memadr + data
// buf_len      - ������ buf
//
// return 0   - OK
// return !=0 - ERROR
//==============================================================================
int i2c_read_adr_8bit(uint8_t i2c_adr_8bit, uint8_t adr_start, uint8_t *buf, uint8_t buf_len)
{
    uint32_t reg;
    
    if  (I2C1->ISR & I2C_ISR_BUSY) return 1; // error bus busy !
    
    I2C1->CR2 = 0;
      
    I2C1->CR2 &= ~I2C_CR2_RD_WRN; // write = set 0
    I2C1->CR2 |= I2C_CR2_AUTOEND; // �� ���������� ���������� ��������� STOP
    I2C1->CR2 |= i2c_adr_8bit;
    I2C1->CR2 &= ~I2C_CR2_NBYTES_Msk;
    I2C1->CR2 |= (1 << I2C_CR2_NBYTES_Pos); // set NBYTES
    
    // start + dev adr ---------------------------------------------------------
    I2C1->CR2 |= I2C_CR2_START;
    
    while(1){
        reg = I2C1->ISR;
        
        if (reg & I2C_ISR_ARLO){
            return 2; // This bit is cleared by hardware when PE=0
        }
        if (reg & I2C_ISR_BERR){
            I2C1->ICR = I2C_ICR_BERRCF;
            return 3; // error BERR
        }
        
        if (reg & I2C_ISR_NACKF) {
            I2C1->ICR = I2C_ICR_NACKCF;
            return 4; // error ADR NASK
        }
        
        if (reg & I2C_ISR_TXIS) break;
    }// while

    // send adr ---------------------------------------------------------------
    I2C1->TXDR = adr_start;
    
    while(1){
        reg = I2C1->ISR;
        
        if (reg & I2C_ISR_ARLO){
            return 2; // This bit is cleared by hardware when PE=0
        }
        if (reg & I2C_ISR_BERR){
            I2C1->ICR = I2C_ICR_BERRCF;
            return 3; // error BERR
        }

        if (reg & I2C_ISR_TXIS){
          break;
        }
        
        if (reg & I2C_ISR_NACKF) {
            I2C1->ICR = I2C_ICR_NACKCF;
            return 5;
        }
        
        //if (reg & I2C_ISR_TC) break;
        
        if (reg & I2C_ISR_STOPF){
            I2C1->ICR = I2C_ICR_STOPCF;
            break;
        }
        
        
    } // while

    // read data
//_tx_new:
    I2C1->CR2 |= I2C_CR2_RD_WRN; // read = set 1
    I2C1->CR2 |= I2C_CR2_AUTOEND; // �� ���������� ���������� ��������� STOP
    I2C1->CR2 |= i2c_adr_8bit;
    I2C1->CR2 &= ~I2C_CR2_NBYTES_Msk;
    I2C1->CR2 |= (buf_len << I2C_CR2_NBYTES_Pos); // set NBYTES

    // start + dev adr ---------------------------------------------------------
    I2C1->CR2 |= I2C_CR2_START;
    
    while(1){
        reg = I2C1->ISR;
        
        if (reg & I2C_ISR_ARLO){
            return 2; // This bit is cleared by hardware when PE=0
        }
        if (reg & I2C_ISR_BERR){
            I2C1->ICR = I2C_ICR_BERRCF;
            return 3; // error BERR
        }
        
        if (reg & I2C_ISR_NACKF) {
            I2C1->ICR = I2C_ICR_NACKCF;
            return 4; // error ADR NASK
        }
        
//        if (reg & I2C_ISR_TXIS) break;

        if (reg & I2C_ISR_RXNE){
            *buf = I2C1->RXDR;
            buf++;
        }
        
        if (reg & I2C_ISR_STOPF){
            I2C1->ICR = I2C_ICR_STOPCF;
            break;
        }
    }// while
    
    return 0;
}

//==============================================================================
// Read from I2C
//
// i2c_adr_8bit - DEVICE adr 8 bit (7bit << 1)
// adr_start    - I2C offset adr (16 bit)
// *buf         - buffer = memadr + data
// buf_len      - ������ buf
//
// return 0   - OK
// return !=0 - ERROR
//==============================================================================
int i2c_read_adr_16bit(uint8_t i2c_adr_8bit, uint16_t adr_start, uint8_t *buf, uint8_t buf_len)
{
    uint32_t reg;
    
    if  (I2C1->ISR & I2C_ISR_BUSY) return 1; // error bus busy !

    I2C1->CR2 = 0;
    
    I2C1->CR2 &= ~I2C_CR2_RD_WRN; // write = set 0
    I2C1->CR2 |= I2C_CR2_AUTOEND; // �� ���������� ���������� ��������� STOP
    I2C1->CR2 |= i2c_adr_8bit;
    I2C1->CR2 &= ~I2C_CR2_NBYTES_Msk;
    I2C1->CR2 |= (2 << I2C_CR2_NBYTES_Pos); // set NBYTES
    
    // start + dev adr ---------------------------------------------------------
    I2C1->CR2 |= I2C_CR2_START;
    
    while(1){
        reg = I2C1->ISR;
        
        if (reg & I2C_ISR_ARLO){
            return 2; // This bit is cleared by hardware when PE=0
        }
        if (reg & I2C_ISR_BERR){
            I2C1->ICR = I2C_ICR_BERRCF;
            return 3; // error BERR
        }
        
        if (reg & I2C_ISR_NACKF) {
            I2C1->ICR = I2C_ICR_NACKCF;
            return 4; // error ADR NASK
        }
        
        if (reg & I2C_ISR_TXIS) break;
    }// while

    // send adr ---------------------------------------------------------------
    I2C1->TXDR = (adr_start & 0xff00) >> 8;
    
    while(1){
        reg = I2C1->ISR;
        
        if (reg & I2C_ISR_ARLO){
            return 2; // This bit is cleared by hardware when PE=0
        }
        if (reg & I2C_ISR_BERR){
            I2C1->ICR = I2C_ICR_BERRCF;
            return 3; // error BERR
        }

        if (reg & I2C_ISR_TXIS){
          I2C1->TXDR = (adr_start & 0xff) >> 0;
          //break;
        }
        
        if (reg & I2C_ISR_NACKF) {
            I2C1->ICR = I2C_ICR_NACKCF;
            return 5;
        }
        
        //if (reg & I2C_ISR_TC) break;
        
        if (reg & I2C_ISR_STOPF){
            I2C1->ICR = I2C_ICR_STOPCF;
            break;
        }
        
        
    } // while

    // read data
//_tx_new:
    I2C1->CR2 |= I2C_CR2_RD_WRN; // read = set 1
    I2C1->CR2 |= I2C_CR2_AUTOEND; // �� ���������� ���������� ��������� STOP
    I2C1->CR2 |= i2c_adr_8bit;
    I2C1->CR2 &= ~I2C_CR2_NBYTES_Msk;
    I2C1->CR2 |= (buf_len << I2C_CR2_NBYTES_Pos); // set NBYTES

    // start + dev adr ---------------------------------------------------------
    I2C1->CR2 |= I2C_CR2_START;
    
    while(1){
        reg = I2C1->ISR;
        
        if (reg & I2C_ISR_ARLO){
            return 2; // This bit is cleared by hardware when PE=0
        }
        if (reg & I2C_ISR_BERR){
            I2C1->ICR = I2C_ICR_BERRCF;
            return 3; // error BERR
        }
        
        if (reg & I2C_ISR_NACKF) {
            I2C1->ICR = I2C_ICR_NACKCF;
            return 4; // error ADR NASK
        }
        
//        if (reg & I2C_ISR_TXIS) break;

        if (reg & I2C_ISR_RXNE){
            *buf = I2C1->RXDR;
            buf++;
        }
        
        if (reg & I2C_ISR_STOPF){
            I2C1->ICR = I2C_ICR_STOPCF;
            break;
        }
    }// while
    
    return 0;
}

