#ifndef RTC_H_
#define RTC_H_

int rtc_reset(void);
int rtc_get_time32( uint32_t * t );
int rtc_set_time32( uint32_t t );


#endif
