#include <stdint.h>
#include "os.h"
#include "hardware.h"
#include "printf_hal.h"
#include "main.h"
#include "keyboard.h"
#include "time_hal.h"

//******************************************************************************
// ��������� ������
//******************************************************************************
void key_status( key_status_t *key_st )
{
    uint32_t dt;
    if (key_st->get_key_status == NULL){
        printf_dos("ERROR: key_status - key_st->get_key_status() == NULL\n");
        return;
    }
    
    switch(key_st->key_auto){
    case KEY_WAIT_DOWN:{ // �������� ������� �� ������
        key_st->key = KEY_POS_NONE;
        if (key_st->get_key_status() == KEY_GPIO_DOWN){
            key_st->key_auto = KEY_DELAY_DOWN;
            key_st->t_start  = xTaskGetTickCount(); // ���������� ������� ����� ������� ������
        }
        break;
    }

    case KEY_DELAY_DOWN:{ // �������� ��� �������� �� ����������� (������� ������)
        dt = time_delta(key_st->t_start, xTaskGetTickCount() );
        if (dt >= KEY_TIMEOUT_MS){
            key_st->key_auto = KEY_TEST_DOWN;
        }
        break;
    }

    case KEY_TEST_DOWN:{  // �������� ������ ��� ��� ������
        if (key_st->get_key_status() == KEY_GPIO_DOWN){
            key_st->key      = KEY_POS_DOWN;
            key_st->t_start  = xTaskGetTickCount(); // �������� ����� ������� ������
            key_st->key_auto = KEY_WAIT_UP;
        } else {
            key_st->key_auto = KEY_WAIT_DOWN;
            key_st->key      = KEY_POS_NONE;
        }
        break;
    }
    
    case KEY_WAIT_UP:{   // �������� ������� ������
        key_st->t_down = time_delta(key_st->t_start, xTaskGetTickCount() ); // ��������� ����� ������� ������ ������
        if (key_st->get_key_status() == KEY_GPIO_UP){
            key_st->key_auto = KEY_DELAY_UP;
            key_st->t_stop   = xTaskGetTickCount(); // ���������� ������� ����� ������� ������
        }
        break;
    }

    case KEY_DELAY_UP:{  // �������� ��� �������� �� ����������� (������� ������)
        dt = time_delta(key_st->t_stop, xTaskGetTickCount() );
        if (dt >= KEY_TIMEOUT_MS){
            key_st->key_auto = KEY_TEST_UP;
        }
        break;
    }

    case KEY_TEST_UP:{   // �������� ������ ������
        if (key_st->get_key_status() == KEY_GPIO_UP){
            key_st->key_auto = KEY_WAIT_DOWN;
            key_st->t_stop   = xTaskGetTickCount(); // ���������� ������� ����� ������� ������
            key_st->t_down   = time_delta(key_st->t_start, key_st->t_stop); // ��������� ����� ������� ���� ������ ������
            key_st->key      = KEY_POS_CLICK;
        }
        break;
    }

    default:
        break;
    }
}
