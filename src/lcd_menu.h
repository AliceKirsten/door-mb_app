#ifndef LCD_MENU_H_
#define LCD_MENU_H_
#include <stdint.h>

#define MENU_TIMEOUT_SEC  (60 * 1000)   // ���, ����� ��������� � ���� � ������� ���������� ������� �� ������

typedef enum {
    LCD_ST_DAT_DOOR = 0, // �������� ������� ��������� ����� + �������
	LCD_ST_MENU,         // ����� ����
	LCD_ST_INPUT         // ���� ������ ��������
} lcd_state_e;


typedef struct {
	uint8_t pos;           // ���� ���� � �������� ���������� ����� �� �����
	uint8_t select;        // ��������� ���� ���� - ���������
} menu_position_t;

#define MENU_LINES_MAX (LCD_RES_Y / 8 - 1) // ���������� ����� ���� �� ������


// ��� ������� ������, ��� ����� ������� ����������
typedef enum {
    LCD_IN_D_T_INT = 0,
	LCD_IN_D_T_ENUM,
	LCD_IN_D_T_TXT,

} lcd_in_data_types;

// ��������� ��� ����� �������� ���������� � ����
typedef struct {
    const char txt[ 21+1 ];   // �������� ������ ����
	void (*call)();           // ������������ ���������
	lcd_in_data_types d_type; // ��� ������� ������

	// for int -------------------------------------------------------------
	uint16_t int_size_char; // ���������� ������
	const char *sprintf_txt;// ���� ��� ���������� � sprintf
	const char *table_char; // ������� �������� ������� ����� ����������
	uint16_t int_min;       // ����������� ��������
	uint16_t int_max;       // ������������ ��������
	uint16_t *int_val;      // ���������� � ������� ����� ��������� ��������

	// for enum ------------------------------------------------------------
	const char *str1_txt;   // ����� ��������� ��� ������ ������� ��������
	uint16_t str1_int;      // �������� �������� ���� ��� ������� ������
	const char *str2_txt;   // ����� ��������� ��� ������ ������� ��������
	uint16_t str2_int;      // �������� �������� ���� ��� ������� ������
	uint16_t *enum_val;     // ���������� � ������� ����� ��������� ��������

	// for str -------------------------------------------------------------
	void (*call_print_val)(char * str); // ������������ ������ ������ �� �����
	int (*call_verify_val)(char * str);// ������������ �������� � ��������� ��������� ��������
	const char *table_char_str; // ������� �������� ������� ����� ����������
	const char *table_char_skip; // ������� �������� ������� ������������
    
} menu_t;


// ���� ��������
typedef struct {
	int x; // ���������� � ������
	int y; // ���������� � ������� �������� (� enum ���������� ��������)

	int cx;// ��������� ��������� ������� (��������� ��� ������������ ��������)
	int cy;
} input_val_t;


void lcd_print_menu(void);
void lcd_input_val(void);
void lcd_print_status(void);

void lcd_call_input_int(void);
void lcd_call_input_enum(void);
void lcd_call_input_txt(void);

void call_date_print(char * str);
int call_date_verify(char * str);

void call_time_print(char * str);
int call_time_verify(char * str);

#endif
