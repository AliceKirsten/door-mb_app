#include "stm32f0xx_hal.h"
#include "port.h"
#include "main.h"
#include "string.h"

/* Public variables ---------------------------------------------------------*/
__IO SysTickCounter_t clock_count = {&SysTick->CTRL, &SysTick->VAL, 0};
__IO SysTickCounter_t pos_count = {&TIM3->CR1, &TIM3->CNT, 0};

trx_struct_t rx_data[MAX_EVENT];
event_t event_queue[MAX_EVENT];
event_t *read_event = NULL, *write_event = NULL;

uint8_t conv_ind;
int ADC_DATA[ADC_MAX_CHAN];

uint32_t sword = 0;
//--------------------------------------------------------------------------------
// Periphery IRQ Callbacks
//USART Master rx transmission complete
void HAL_UART_RxCpltCallback(UART_HandleTypeDef* huart)
{       
    int i = 0;
    uint16_t db = 0;
    uint8_t buf[PACKET_SIZE];
    
    if (huart->RxXferSync)
    {
        i = huart->RxXferCount > huart->RxXferLast ? 
            huart->RxXferCount - huart->RxXferLast :
            (256 - huart->RxXferLast) + huart->RxXferCount;
        if (i == PACKET_SIZE)
        {
            if (huart->RxXferCount > huart->RxXferLast) memcpy(buf, &huart->pRxBuffPtr[huart->RxXferLast], PACKET_SIZE);
            else 
            {
                db = 256 - huart->RxXferLast;
                memcpy(buf, &huart->pRxBuffPtr[huart->RxXferLast], db);
                memcpy(&buf[db], &huart->pRxBuffPtr[0], PACKET_SIZE - db);
            }
            putevent(COMMUNICATION, buf);
            huart->RxXferLast += PACKET_SIZE;
            if (huart->RxXferLast >= 256) huart->RxXferLast -= 256;
            huart->RxXferSync = 0;
        }
    }
    else
    {
        i = huart->RxXferLast;
        do
        {
            sword >>= 8;
            sword |= (uint32_t)(huart->pRxBuffPtr[i++] << 24);
        } while (sword ^ MAGIC_32 != 0 && i < huart->RxXferCount);
        if (sword ^ MAGIC_32 == 0) huart->RxXferSync = 1;
    }
}

// ADC end of conversion callback
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
    uint32_t res = GET_VOLTAGE(hadc->Instance->DR);
    
    if (conv_ind != ADC_MCURR) ADC_DATA[conv_ind] = res;
    else ADC_DATA[conv_ind] = res - ADC_DATA[ADC_CALIBC];
}

// Timer period elapsed callback
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
    get_encoder_values(&motor_instance);
    putevent(REG_CYCLE, NULL);
    
    __HAL_TIM_CLEAR_IT(htim, TIM_IT_UPDATE);
}

//overcurrent error
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
    //putevent(OVERCURRENT); --??? testing needed
}
//--------------------------------------------------------------------------------
// Time management functions - HAL
void HAL_Delay(__IO uint32_t Delay)
{
    uint32_t dead_time = HAL_GetTick() + Delay;
    
    while(HAL_GetTick() < dead_time);
}

uint32_t HAL_GetTick(void)
{
    return clock_count.units;
}

void HAL_SuspendTick(void)
{
    *clock_count.enable &= ~((uint32_t)1);
}

void HAL_ResumeTick(void)
{
    *clock_count.enable |= 1;
}

//--------------------------------------------------------------------------------
// Time and position functions 
bool DelayCnt(uint32_t Delay, __IO uint32_t *reg, uint32_t flag)
{
    bool res = false;
    uint32_t dead_time = GetTickCnt() + (Delay << 10);
    
    do
    {
        res = *reg & flag;
    } while(GetTickCnt() < dead_time && !res);
    
    return res;
}

uint64_t GetTickCnt(void)
{
    uint64_t time = (clock_count.units << 10) + *clock_count.ticks;
    return time;
}

void GetPosCnt(motor_t* motor)
{
    int32_t pos = *pos_count.ticks;
    int32_t vel = pos - motor->prev_pos;
    if ((uint32_t)abs(vel) > (ENC_PERIOD >> 1)) 
    {
        if (vel > 0) vel = vel - ENC_PERIOD;
        else vel = vel + ENC_PERIOD;
    }
    
    motor->vel = vel;
    motor->pos += vel;
}

//--------------------------------------------------------------------------------
// functions of event queue routine
void putevent(EventType new_event, uint8_t * data)
{
    uint8_t i = 0;
    while(write_event->type != IDLE) 
    {
        write_event = write_event->next;
        i++;
        if (i == MAX_EVENT)
        {
            clearevents();
            return;
        }
    }
    write_event->type = new_event;
    if (new_event == COMMUNICATION) memcpy(write_event->data, data, PACKET_SIZE);
    write_event = write_event->next;
}

void getevent(event_t* new_event)
{
    uint8_t i = 0;
    while (read_event->type == IDLE && i < MAX_EVENT) 
    {
        read_event = read_event->next;
        i++;
    }
    memcpy(new_event, read_event, sizeof(event_t));
    read_event->type = IDLE;
}

void clearevents(void)
{
    for (int i = 0; i < MAX_EVENT; i++)
    {
        event_queue[i].type = IDLE;
        event_queue[i].data = &rx_data[i];
        if (i < MAX_EVENT-1) event_queue[i].next = &event_queue[i+1];
        else event_queue[i].next = &event_queue[0];
    }
    read_event = &event_queue[0];
    write_event = &event_queue[0];
}

//--------------------------------------------------------------------------------
// LED control functions

void led_on(led_t led)
{
    switch (led)
	{
	case LED_1:
		HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_SET);
		break;
	case LED_ALL:
		HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_SET);
		break;
	default:
		// do nothing for undefined led number
		break;
	}
}

void led_off(led_t led)
{
    switch (led)
	{
	case LED_1:
		HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_RESET);
		break;
	case LED_ALL:
		HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_RESET);
		break;
	default:
		// do nothing for undefined led number
		break;
	}
}

void led_toggle(led_t led)
{
    switch (led)
	{
	case LED_1:
		HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
		break;
	case LED_ALL:
		HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
		break;
	default:
		// do nothing for undefined led number
		break;
	}
}

//--------------------------------------------------------------------------------
// PID regulator processing
void process_pid(pid_t* pid, int32_t u_val, int32_t s_val)
{
    float e, p, i, d, sum;
    float ai, si;
    int32_t asum, ssum;
    
    e = (float)(u_val - s_val);
    p = (pid->Kp * e);
    i = (pid->Ki * e) + pid->i;
    d = pid->Kd * (e - pid->error);
    
    ai = (float)abs(i);
    si = (float)sign(i);
    pid->i = (ai >= (float)pid->limit ? (si*(float)pid->limit) : i);
    pid->error = e;
    
    sum = p + i + d;  
    asum = (int32_t)abs(sum);
    ssum = (int32_t)sign(sum);
    pid->out = (asum >= (int32_t)pid->limit ? (ssum*(int32_t)pid->limit) : sum);
}

void drive(int32_t val, TIM_HandleTypeDef* htim)
{
    uint32_t uval = abs(val);
    uint32_t pwm = (uval >= htim->Instance->ARR-50 ? 
                    htim->Instance->ARR-50 :
                    (uval == 0 ? 
                    10 :
                    uval));
    int s = (int)sign(val);
    
    if (s < 0)              //backward
    {
        HAL_GPIO_WritePin(AHI_GPIO_Port, AHI_Pin, GPIO_PIN_RESET);
        HAL_GPIO_WritePin(BHI_GPIO_Port, BHI_Pin, GPIO_PIN_SET); 
        
        htim->Instance->CCR1 = 0;
        htim->Instance->CCR4 = pwm;  
    }
    else                //forward
    {
        HAL_GPIO_WritePin(AHI_GPIO_Port, AHI_Pin, GPIO_PIN_SET);
        HAL_GPIO_WritePin(BHI_GPIO_Port, BHI_Pin, GPIO_PIN_RESET); 
        
        htim->Instance->CCR1 = pwm;
        htim->Instance->CCR4 = 0;
    }
}

//--------------------------------------------------------------------------------
// encoder processing
void get_encoder_values(motor_t* motor)
{
    int i = 0;
    
    motor->prev_pos = motor->pos;
    motor->prev_vel = motor->vel;
    GetPosCnt(motor);
    
    motor->acc = 0;
    for (i = 4; i > 0; i--)
    {
        motor->acc_int[i] = motor->acc_int[i - 1];
        motor->acc += motor->acc_int[i];
    }
    motor->acc_int[i] = motor->vel - motor->prev_vel;
    motor->acc += motor->acc_int[i];
}