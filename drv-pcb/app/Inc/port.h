/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __PORT_H__
#define __PORT_H__

/* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */
#include <stdint.h>

#define TICKS_IN_UNIT                   1024
#define TICKS_IN_SECOND                 1024000
#define MAX_EVENT                       100

#define ADC_MAX_CHAN                    7
#define ADC_VTRES                       1000
#define HALL_SENSITIVITY                55
#define TEMP30_CAL_ADDR                 ((uint16_t*) ((uint32_t) 0x1FFFF7B8))
#define VDD_APPLI                       3000
#define AVG_SLOPE                       5336
#define MAX_VIN                         40000

#define V_DES                           10
#define T_DES                           2.0f
#define PWM_PERIOD                      1440
#define CALIB_PWM                       240
#define ENC_PERIOD                      65535
#define CYCLE_LENGTH                    10000
#define CYCLE_DIV                       48
#define MAGIC_32                        0xf3c3f00c

#define GET_VOLTAGE(x)                  ((uint32_t)x * (VDD_VALUE << 4)) >> 12                
#define CONSOLE_ENABLE                  USART1->CR1 |= USART_CR1_RXNEIE;
#define CONSOLE_DISABLE                 USART1->CR1 &= ~USART_CR1_RXNEIE;

/* Boolean type definition */
#ifndef bool
typedef enum
{
    false = 0,
    true
} bool;
#endif

#ifndef abs
#define abs(x)  (x >= 0 ? x : 0 - x)
#endif

#ifndef sign
#define sign(x) (x >= 0 ? 1 : -1)
#endif

/* This structure allows to count timer ticks.
*/
typedef struct 
{
    __IO uint32_t* enable;
    __IO uint32_t* ticks;
    uint64_t units;
} SysTickCounter_t;
extern __IO SysTickCounter_t clock_count, pos_count;

bool DelayCnt(uint32_t Delay, __IO uint32_t *reg, uint32_t flag);
uint64_t GetTickCnt(void);

/* CONSOLE Packet structure */
#define START_FIELD_SIZE        4
#define ADDRESS_FIELD_SIZE      1
#define TYPE_FIELD_SIZE         1
#define DATA_FIELD_SIZE         24
#define CRC_FIELD_SIZE          2

//��� �������
typedef enum 
{
    Unused = 0,                     //�� ����������������
    OK,                             //��� ������
    SystemError,                    //��������� ������
    FatalError                      //����������� ������
} StatusType;

//��� ������
typedef enum 
{
    PeriphInitFailed = 8,           //������ ������������� ���������
    NoDataInFlash,                  //������ �� �����
    InVoltageLow,                   //������ ������� ����������
    InVoltageNegative,              //������������ ������� ���������� 
    AccVoltageNegative,             //������������� ���������� ������������
    Overcurrent,                    //���������� �� ����
    CalibrationUndone,              //��������� ����������
    CalibrationFailed               //������ ��������� ����������
    
} ErrorType;

//��������� ��������� �������
typedef struct
{
    ErrorType  sys_error;           //������
} sys_state_t;

//��� �������
typedef enum 
{
    SetVelPid = 1,                  //1- ��������� ���������� ���-���������� �� ��������
    GetVelPid,                      //2- ������ ���������� ���-���������� �� ��������
    SetPosPid,                      //3- ��������� ���������� ���-���������� �� ���������
    GetPosPid,                      //4- ������ ���������� ���-���������� �� ���������
    SetElectData = 5,               //5- ��������� ���������� ������������� �������
    GetElectData,                   //6- ������ ���������� ������������� �������
    SetMechData,                    //7- ��������� ���������� ������������ �������
    GetMechData,                    //8- ������ ���������� ������������ �������
    SetManCtrl,                     //9- ��������� ������� ������ ���������� ������
    SetAutoCtrl,                    //10- ��������� ��������������� ������ ���������� ������
    SetWintCtrl,                    //11- ��������� ��������������� ������ ���������� ������
    ResetWintCtrl,                  //12- ��������� ��������������� ������ ���������� ������
    GetCalibrate,                   //13- ���������� ����������
    SetDoorOpen,                    //14- ������� �����
    SetDoorClose,                   //15- ������� �����
    GetAccVolt,                     //16- ������ ���������� �� ������������
    SetAccOpen,                     //17- ������� ���������� ������������
    SetAccClose,                    //18- ������� ���������� ������������
    SetTime,                        //19- ��������� �������
    GetTime,                        //20- ������ �������
    GetErrorCount,                  //21-- ������ ���������� ������
    GetVoltageData,                 //22- ������ ���� ����������
    GetSystemData,                  //23- ������ ���������� ��������� �������
    GetErrorData,                   //24- ������ ���� ������

    CMD_VERSION = 200,              // ������ ������ �� � ��� �������� �� (��������� ��� ���������)(������ packet_soft_version_t)
} CommandType;

// �������-�������� �����, FirmWare - ��� ��������� (fw_types.h) + ������ ��
#pragma pack(1)
typedef struct {
    uint8_t fw_types;   // ��� ��������� (��������� ��� ���������� ��).
    uint8_t soft_ver_h; // ������� ����� ������ �� - ���������� ��� ������ ���������� !!!
    uint8_t soft_ver_l; // ������� ����� ������ ��.
    uint8_t soft_ver_y; // ���, ��������� ��� ����� ����.
    char get_commit_str[]; // ������, ������� ����� GIT � ������� "(2019-09-06 07acc48)"
} packet_soft_version_t;
#pragma pack()



typedef __packed struct
{
    uint32_t    preamble;                     // ��������� ������������ ����� 32 ����
    uint8_t     adr_src:4;                    // ����� ���������
    uint8_t     adr_dst:4;                    // ����� ����������
    uint8_t     type;                         // ��� ������
    uint8_t     data[DATA_FIELD_SIZE];        // ������
    uint16_t    crc;                          // crc-16
} packet_t;
#define PACKET_SIZE            (sizeof(packet_t))

typedef enum {
    ADR_PC = 0,                         // ��
    ADR_DRV,                            // ��������� �������� ������
    ADR_TDRV,                           // ������ ���������� ��������� ������
    ADR_TLOG,                           // ������ �����, ��������� ���������� � �.�.
        
//    ADR_MB,                             // �������� �����
//    ADR_DRV,                            // ����� ��������
//    ADR_SK                              // ������ ��������� - ���� �� �����������
} packet_adr_t;
    
typedef union
{
    packet_t    trx_packet;         //���������
    uint8_t     trx_message[PACKET_SIZE]; //������ ����
} trx_struct_t;

/* Event enumeration */
typedef enum
{
    IDLE = 0,
    INITIALIZATION,
    CALIBRATION,
    REG_CYCLE,
    COMMUNICATION,
    OVERCURRENT,
    FATAL_ERROR
} EventType;

typedef struct event
{
    EventType           type;
    trx_struct_t        *data;
    struct event        *next;
} event_t;

extern event_t* read_event, *write_event;
void putevent(EventType newevent, uint8_t * data);
void getevent(event_t* newevent);
void clearevents();

/* ADC Control */
typedef enum
{
    ADC_CALIBC = 0,     //motor calibr voltage (zero current)
    ADC_MCURR,          //motor current (absolute)
    ADC_CALIBV,         //input calibr voltage (ADC offset)
    ADC_VREF,           //input positive voltage
    ADC_VIN,            //input negative voltage
    ADC_ACC,            //accum negative voltage
    ADC_TEMP            //MCU temperature
} adc_chan_t;
extern uint8_t conv_ind;
extern int ADC_DATA[ADC_MAX_CHAN];

/* LED control */
typedef enum
{
    LED_1 = 0,
    LED_ALL
} led_t;
void led_on(led_t led);
void led_off(led_t led);
void led_toggle(led_t led);

/* PID regulator type */
typedef __packed struct
{
    float       Kp;                //���������������� �����
    float       Ki;                //������������ �����
    float       Kd;                //���������������� �����
    uint32_t    limit;             //�����������
    float       error;             //������ �������������
    float       i;                 //�������� ������
    int32_t     out;               //����� ����������
} pid_t;

void process_pid(pid_t* pid, int32_t u_val, int32_t s_val);
void drive(int32_t val, TIM_HandleTypeDef* htim);

/* motor parameters main structure */
typedef __packed struct 
{
    uint16_t err_cnt;           //error counter                 //MOTOR_INSTANCE_FLASH_ADDRESS + 0   
    float k_mu;                 //vmax voltage|in voltage coef  //MOTOR_INSTANCE_FLASH_ADDRESS + 2
    
    uint32_t max_A;             //max current                   //MOTOR_INSTANCE_FLASH_ADDRESS + 6
    uint32_t max_U;             //max voltage                   //MOTOR_INSTANCE_FLASH_ADDRESS + 10
    float scf_vel;              //velocity scaling factor       //MOTOR_INSTANCE_FLASH_ADDRESS + 14 
    float scf_vel_w;            //velocity scaling factor       //MOTOR_INSTANCE_FLASH_ADDRESS + 18 
    float open_part;            //opening path part             //MOTOR_INSTANCE_FLASH_ADDRESS + 22 
    int8_t reverse;             //motor polarity reversed       //MOTOR_INSTANCE_FLASH_ADDRESS + 26
    
    uint8_t calibration;        //calibration status            //MOTOR_INSTANCE_FLASH_ADDRESS + 27   
    uint16_t reserved;                                          //MOTOR_INSTANCE_FLASH_ADDRESS + 28
    
    float t_mech;               //mechanical time constant      //MOTOR_INSTANCE_FLASH_ADDRESS + 30
    uint16_t nom_vel;           //nominal velocity              //MOTOR_INSTANCE_FLASH_ADDRESS + 34
    int32_t close_pos;          //opened doors position         //MOTOR_INSTANCE_FLASH_ADDRESS + 36
    int32_t open_pos;           //closed doors position         //MOTOR_INSTANCE_FLASH_ADDRESS + 40

    int32_t pos;                //current pos                   //MOTOR_INSTANCE_FLASH_ADDRESS + 44
    int32_t vel;                //current velocity              //MOTOR_INSTANCE_FLASH_ADDRESS + 48
    int32_t acc;                //current acceleration          //MOTOR_INSTANCE_FLASH_ADDRESS + 52
    int32_t acc_int[5];         //previous accelerations        //MOTOR_INSTANCE_FLASH_ADDRESS + 56
    int32_t prev_pos;           //previous pos                  //MOTOR_INSTANCE_FLASH_ADDRESS + 76
    int32_t prev_vel;           //previous velocity             //MOTOR_INSTANCE_FLASH_ADDRESS + 80
} motor_t;                                                      //TOTAL SIZE: 84 BYTES
extern motor_t motor_instance;
void GetPosCnt(motor_t* motor);
void get_encoder_values(motor_t* motor);
#define MOTOR_INSTANCE_FLASH_ADDRESS    0x0800F800              //FLASH last page address
#define MOTOR_POSITION_FLASH_ADDRESS    0x0800F82C              //MOTOR_INSTANCE_FLASH_ADDRESS + 44
#endif /* __PORT_H__ */
