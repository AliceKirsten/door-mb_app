#ifndef FW_TYPE_H_
#define FW_TYPE_H_
#include "stm32f030xc.h"

// ������ ���� ���������� ��� ���������, ��������� ��� ��������� ��.
// ����� ������������� FLASH ������ �� �����.

// FW_BOOT_STR - ������� 16 �������� ������� \0

#define FW_BOOT (0) // ��������� - ���������.
#define FW_BOOT_STR "BOOT_STM32" // ������ ������������ � ����� �� �� FLASH

#define FW_APP  (1) // ��������� - ����������.
#define FW_APP_STR "APP_STM32" // ������ ������������ � ����� �� �� FLASH

#define FW_TYPE (FW_APP)

#if FW_TYPE == FW_APP
 #define SOFT_TYPE_STR FW_APP_STR
#endif

#if FW_TYPE == FW_BOOT
 #define SOFT_TYPE_STR FW_BOOT_STR
#endif

#define PAGE_SIZE (2048UL) // ������ �������� ���� ������ � ������ STM32F030CC

//----------------------------------------------------------------
#define FW_BOOT_ADDR (FLASH_BASE)                  // ����� ������ ����������
#define FW_BOOT_SIZE (32UL * 1024UL)               // ������������ ������ ����������

#define FW_APP_ADDR  (FW_BOOT_ADDR + FW_BOOT_SIZE) // ����� ������ ����������� �� (�������� ���������)
#define FW_APP_SIZE  (112UL * 1024UL)              // ������������ ������ ����������� ��

#define FW_COPY_ADDR (FW_APP_ADDR + FW_APP_SIZE)   // ����� ������ ����� ����������� �� (�� � ����������, ��� ����������� � ������� FW_APP_ADDR)
#define FW_COPY_SIZE (FW_APP_SIZE)                 // ������������ ������ ����������� �� (������������ ���� ������ ���� �������� ������ ��� ����� ����������)

#endif