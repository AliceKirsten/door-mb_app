#ifndef OS_H_
#define OS_H_

#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "queue.h"
#include "semphr.h"
#include "event_groups.h"

#define WHILE_BEGIN while(1){
#define WHILE_END }

#endif
