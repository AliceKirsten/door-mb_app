@echo off
set stlinkcmd="C:\Program Files (x86)\STMicroelectronics\STM32 ST-LINK Utility\ST-LINK Utility\ST-LINK_CLI.exe"

echo "================================================================================"
echo "   Script: STM32 CHIP PROGRAMMING."
echo "================================================================================"


echo "================================================================================"
echo "   ST-LINK probe."
echo "================================================================================"
%stlinkcmd% -List
if %ErrorLevel% equ 0 goto erase_all
goto error_nostlink

:erase_all
echo "================================================================================"
echo "---- ERASE ALL CHIP ----"
echo "================================================================================"
%stlinkcmd% -ME
if %ErrorLevel% equ 0 goto prog
goto error_prog

:prog
echo "================================================================================"
echo "---- PROG ----"
echo "================================================================================"
%stlinkcmd% -P mb_boot.hex 0x08000000 -V "after_programming"
if %ErrorLevel% neq 0 goto error_prog

echo "================================================================================"
echo "---- RUN ----"
echo "================================================================================"
%stlinkcmd% -Rst -Run 0x08000000 
rem %stlinkcmd% -Run 0x08000000 
rem -Q -NoPrompt
exit 0

rem -----------------------------------------------------------------------------
:error_prog
echo "ERROR ERROR ERROR ERROR ERROR ERROR"
echo "    PROG"
echo "ERROR ERROR ERROR ERROR ERROR ERROR"
exit 1

:error_nostlink
echo "ERROR ERROR ERROR ERROR ERROR ERROR"
echo "    NO ST-LINK"
echo "ERROR ERROR ERROR ERROR ERROR ERROR"
exit 1
